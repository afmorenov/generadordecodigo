image:
	docker build -t docker-srv.siesa.com/siesa/framework/wizard/netcore$(v) .

push:
	docker push docker-srv.siesa.com/siesa/framework/wizard/netcore$(v)
	
run:
	docker run -t -i --rm -p 8000:80 docker-srv.siesa.com/siesa/framework/wizard/netcore$(v)

portainer:
	docker volume create portainer_data
	docker run -t -i --rm -d -p 9000:9000 --name portainer -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer
	
publish: image push