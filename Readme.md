-- Instalar make
choco install make

-- Crea imagen docker
make image

-- Prueba imagen local
docker volume create portainer_data;
docker run -t -i --rm -d -p 9000:9000 -v /var/run/docker.sock:/var/run/docker.sock -v portainer_data:/data portainer/portainer;

docker run -t -i --rm -p 8000:80 docker-srv.siesa.com/siesa/framework/wizard/netcore

docker login docker-srv.siesa.com

-- Crea y publica
make image
make push