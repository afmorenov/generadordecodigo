﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeGenerator.Models
{
    public class ConstraintModel
    {
        public string TableName { get; set; }
        public string Value { get; set; }
        public string Resource { get; set; }
    }

}
