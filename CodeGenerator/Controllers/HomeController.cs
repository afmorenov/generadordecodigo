﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using CodeGenerator.Templates;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text.RegularExpressions;

namespace CodeGenerator.Controllers
{
    public class HomeController : Controller
    {
        public CodeGeneratorModel CodeGeneratorModel = new CodeGeneratorModel();

        public IActionResult Index()
        {
            var context = HttpContext.Connection.Id;
            GCUtil.Reset();
            CodeGeneratorModel.ConnectionId = HttpContext.Connection.Id;
            CodeGeneratorModel.Conexiones = GCUtil.ListDBSettings;
            //CodeGeneratorModel.Tables = GetTables(1).Tables;
            var TemplatesResult = GetTemplates(1, 1);
            //CodeGeneratorModel.Templates = TemplatesResult.Templates;
            CodeGeneratorModel.ProyectoArea = TemplatesResult.ProyectoArea;
            return View(CodeGeneratorModel);
        }

        [HttpGet]
        public CodeGeneratorModel GetTables(int NumberConnection)
        {
            if (NumberConnection != 0)
            {
                DBSettings DBSettingsActual = GCUtil.ListDBSettings.Find(x => x.NumberConnection == NumberConnection);
                try
                {
                    CodeGeneratorModel.Tables = GCUtil.DataBaseInfo.FirstOrDefault(x => x.NumberConnection == NumberConnection).Tables.OrderBy(x=>x.Name).ToList();
                    SetTableModule(CodeGeneratorModel.Tables);
                }
                catch (Exception Ex)
                {
                    CodeGeneratorModel.Errores.Add("Error en la conexión con la base de datos " + DBSettingsActual.Name);
                    CodeGeneratorModel.Errores.Add(Ex.Message);
                }
            }
            return CodeGeneratorModel;
        }

        private void SetTableModule(List<TableModel> tables)
        {
            foreach (TableModel table in tables)
            {
                Match WUNOEE = Regex.Match(table.Name.ToUpper(), @"^W000[1-6]", RegexOptions.IgnoreCase); // tablas Web que se alojan en UNOEE
                Match UNOEE = Regex.Match(table.Name.ToUpper(), @"^T", RegexOptions.IgnoreCase);
                Match UNOWEB = Regex.Match(table.Name.ToUpper(), @"^W", RegexOptions.IgnoreCase);

                if (ModuleSeguridad().Contains(table.Name.ToUpper()))
                    table.Comment = "UNOSEC";
                else if (UNOEE.Success || WUNOEE.Success)
                    table.Comment = "UNOEE";
                else if (UNOWEB.Success)
                {

                    //Para Web se pasa entero el numero de la tabla ya que se sigue un estandar WXXXX donde las X son un digito entero positivo
                    //a diferencia de UNOEE donde por ejemplo existe la tabla t0171_mm_tipos_cambio y la tabla  t171_mc_lista_fletes_tarifa y
                    // su entero serian el mismo numero. (No pasa nada ya que solo existe una area de UNOEE para todas las tablas que comienzan por T)
                    int NumberTable = int.Parse(table.Name.Substring(1, 4));

                    //Se separa las areas dependiendo de la separacion de cada una (Se separa de 100 en 100 para cada Area en vez haber 
                    //asigando 1000 como minimo sabiendo que disponen de 9999, por eso la condicion tan larga y absurda en UNOGP)
                    if (NumberTable >= 800 && NumberTable <= 899)
                    {
                        table.Comment = "UNOMNTO";
                    }
                    else if (NumberTable >= 1200 && NumberTable <= 1399)
                    {
                        table.Comment = "UNOSST";
                    }
                    else if ((NumberTable >= 700 && NumberTable <= 799) || (NumberTable >= 900 && NumberTable <= 999) || (NumberTable >= 1400 && NumberTable <= 1499) || NumberTable == 49 || (NumberTable >= 541 && NumberTable <= 545))
                    {
                        table.Comment = "UNOGP";
                    }
                    else
                    {
                        table.Comment = "UNONOM";
                    }
                }
            }

        }

        private List<string> ModuleSeguridad()
        {
            List<string> securityModule = new List<string>();
            if (securityModule.Count == 0)
            {
                securityModule.Add("T001_PP_PARAMETROS");
                securityModule.Add("T002_PP_VERSIONES");
                securityModule.Add("T003_PP_MODULOS");
                securityModule.Add("T004_PP_PREF_USUARIO");
                securityModule.Add("T010_MM_COMPANIAS");
                securityModule.Add("T051_MM_PREF_USUARIO_CIA");
                securityModule.Add("T052_MM_PREF_USUARIO_DOCTOS");
                securityModule.Add("T058_MM_USUARIO_ENTIDAD");
                securityModule.Add("T181_MC_PARAMETROS_APROBACION");
                securityModule.Add("T211_MM_FUNCIONARIOS");
                securityModule.Add("T542_SS_SESION_W");
                securityModule.Add("T545_MN_GRUPOS_MENU");
                securityModule.Add("T546_MN_MENU_METODOS");
                securityModule.Add("T547_MN_MENU_OTROS");
                securityModule.Add("T548_MN_MENUS");
                securityModule.Add("T549_MN_MENUS_GRUPOS_MENU");
                securityModule.Add("T5491_MN_MENUS_GRUPOS_MENU_W");
                securityModule.Add("T550_SS_PERFILES");
                securityModule.Add("T551_SS_METODOS_GENERALES");
                securityModule.Add("T552_SS_USUARIOS");
                securityModule.Add("T5521_SS_USUARIOS_CLAVES");
                securityModule.Add("T5522_SS_USUARIOS_PROGRAMA");
                securityModule.Add("T553_SS_PARAMETROS_GENERALES");
                securityModule.Add("T554_SS_POLITICAS");
                securityModule.Add("T561_SS_METODOS");
                securityModule.Add("T5611_SS_METODOS_CU");
                securityModule.Add("T562_SS_PARAMETROS");
                securityModule.Add("T563_SS_METODOS_MODULOS");
                securityModule.Add("T564_SS_METODOS_SP");
                securityModule.Add("T570_SS_METODOS_PERFIL");
                securityModule.Add("T571_SS_USUARIOS_PERFIL");
                securityModule.Add("T572_SS_CONSULTAS_PERFIL");
                securityModule.Add("T5798_SM_UPDATE_SERVICE_PACK");
                securityModule.Add("T6001_IN_INFORMES_PERFILES");
                securityModule.Add("T691_WS_USUARIO_CONSULTA");
                securityModule.Add("T9734_PDV_CAJEROS");
                securityModule.Add("T9760_PDV_BD_POS");
                securityModule.Add("T9766_PDV_ENROLAMIENTO");
                securityModule.Add("W0003_LENGUAJES");
                securityModule.Add("W0004_BARRAS_HERRAMS_CLASES");
                securityModule.Add("W0015_RECURSOS");
                securityModule.Add("W0009_VERSIONES");
                securityModule.Add("W0040_ARCHIVOS");
            }
            return securityModule;
        }



        [HttpGet]
        public CodeGeneratorModel GetTemplates(int Framework , int TipoOrigen)
        {
            try
            {
                if (Framework == 0)
                    CodeGeneratorModel.Errores.Add("Error en el framework seleccionado. ");
                else if (Framework == 1)
                {
                    CodeGeneratorModel.ProyectoArea = new List<string> { "UNONOM", "UNOGP", "UNOSST", "UNOEE", "UNOMNTO", "UNOCCO" };
                    if (TipoOrigen == 1)
                        CodeGeneratorModel.NameSpace = "SiesaNetCore";
                    else 
                        CodeGeneratorModel.NameSpace = "SiesaNetCoreRoe";
                }
                else if (Framework == 2)
                {
                    CodeGeneratorModel.ProyectoArea = new List<string> { "nom", "gpe", "soc", "gen", "map", "cco" };
                    CodeGeneratorModel.NameSpace = "SiesaJavaHibernate";
                }
                else if (Framework == 3)
                {
                    CodeGeneratorModel.ProyectoArea = new List<string> { "Seguridad", "Licencias" };
                    CodeGeneratorModel.NameSpace = "SiesaBlazor";
                }
                CodeGeneratorModel.Templates = new BuildTemplate().GetTemplates(CodeGeneratorModel.NameSpace).OrderBy(x => x.Order).ToList();
            }
            catch (Exception Ex)
            {
                CodeGeneratorModel.Errores.Add("Error en el framework " + CodeGeneratorModel.NameSpace);
                CodeGeneratorModel.Errores.Add(Ex.Message);
            }

            return CodeGeneratorModel;
        }

        [HttpGet]
        public ActionResult DownloadFile(string path,string name)
        {
            return File(System.IO.File.ReadAllBytes(path), "application/octet-stream", name);
        }

        //[HttpPost]
        //public JsonResult GenerateCode(string JsonTables, string JsonTemplates, int NumberConnection, int FrameworkActual, string UserId)
        //{
        //    GCUtil.Reset();

        //    CodeGeneratorModel.ConexionActual = 1;
        //    CodeGeneratorModel.FrameworkActual = 1;

        //    Dictionary<string, object> result = new Dictionary<string, object>();
        //    if (NumberConnection != 0 && FrameworkActual != 0)
        //    {
        //        GCUtil.Framework = FrameworkActual;
        //        if (FrameworkActual == 1)
        //        {
        //            CodeGeneratorModel.Domain = "Siesa.Enterprise";
        //            CodeGeneratorModel.NameSpace = "SiesaNetCore";
        //        }
        //        else
        //        {
        //            CodeGeneratorModel.Domain = "JavaHibernate";
        //            CodeGeneratorModel.NameSpace = "SiesaJavaHibernate";
        //        }
        //        CodeGeneratorModel.PathGenerate += CodeGeneratorModel.Domain;
        //        DBSettings DBSettingsActual = GCUtil.ListDBSettings.Find(x => x.NumberConnection == NumberConnection);
        //        List<TableModel> tables = JsonConvert.DeserializeObject<List<TableModel>>(JsonTables);
        //        List<TemplateModel> templates = JsonConvert.DeserializeObject<List<TemplateModel>>(JsonTemplates);

        //        if (tables.Count != 0 && templates.Count != 0)
        //        {
        //            try
        //            {
        //                if (Directory.Exists(CodeGeneratorModel.PathGenerate))
        //                {
        //                    Directory.Delete(CodeGeneratorModel.PathGenerate, true);
        //                }

        //                if (tables.Count != 0)
        //                {
        //                    tables.ForEach(x => x = new Scheme(DBSettingsActual).GetDataTable(x));
        //                }
        //                //var hubContext = GlobalHost.ConnectionManager.GetHubContext("ChatHub");
        //                foreach (var template in templates)
        //                    foreach (var table in tables)
        //                    {
        //                        CodeGeneratorModel.TableGenerated = table;
        //                        CodeGeneratorModel.TemplateGenerated = template;
        //                        new BuildTemplate().Generate(CodeGeneratorModel);
        //                    }

        //                string folderToZip = CodeGeneratorModel.PathGenerate;
        //                string nameFile = CodeGeneratorModel.Domain + "_CodeGenerate_" + DateTime.Now.ToFileTime().ToString() + ".zip";
        //                string zipFile = GCUtil.PathTemp + nameFile;
        //                if (!Directory.Exists(folderToZip))
        //                    GCUtil.Errors.Add("El directorio no fue creado o no existe.");
        //                else
        //                {
        //                    ZipFile.CreateFromDirectory(folderToZip, zipFile);
        //                    byte[] file = System.IO.File.ReadAllBytes(zipFile);
        //                    result.Add("Error", GCUtil.Errors);
        //                    result.Add("nameFile", nameFile);
        //                    result.Add("file", file);
        //                    result.Add("success", true);
        //                    return Json(result);
        //                }
        //            }
        //            catch (Exception e)
        //            {
        //                GCUtil.Errors.Add(e.Message);
        //            }
        //        }
        //        else
        //        {
        //            GCUtil.Errors.Add("Debe seleccionar al menos una tabla y una plantilla.");
        //        }
        //    }
        //    else
        //    {
        //        GCUtil.Errors.Add("Debe seleccionar una conexión y un framework.");
        //    }
        //    result.Add("Error", GCUtil.Errors);
        //    result.Add("success", false);
        //    return Json(result);
        //}

        [HttpGet]
        public JsonResult RefreshTable(string TableName, int NumberConnection)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            if (NumberConnection != 0)
            {
                DBSettings DBSettingsActual = GCUtil.ListDBSettings.Find(x => x.NumberConnection == NumberConnection);
                try
                {
                    Scheme scheme = new Scheme(DBSettingsActual);
                    scheme.tableModels.Add(new TableModel { Name = TableName });

                    int ubicacion = GCUtil.DataBaseInfo.FindIndex(x => x.NumberConnection == DBSettingsActual.NumberConnection);

                    List<ColumnModel> columns = scheme.GetColumns();
                    if (columns.Count > 0)
                    {
                        var asd = GCUtil.DataBaseInfo[ubicacion].Columns.RemoveAll(x=>x.TableName == TableName);
                        GCUtil.DataBaseInfo[ubicacion].Columns.AddRange(columns);
                    }

                    List<InReferencesModel> inReferencesModel = scheme.GetInFKs();
                    if (inReferencesModel.Count > 0)
                    {
                        GCUtil.DataBaseInfo[ubicacion].InReferences.RemoveAll(x => x.TableName == TableName);
                        GCUtil.DataBaseInfo[ubicacion].InReferences.AddRange(inReferencesModel);
                    }

                    List<OutReferencesModel> outReferencesModel = scheme.GetOutFKs();
                    if (outReferencesModel.Count > 0)
                    {
                        GCUtil.DataBaseInfo[ubicacion].OutReferences.RemoveAll(x => x.TableName == TableName);
                        GCUtil.DataBaseInfo[ubicacion].OutReferences.AddRange(outReferencesModel);
                    }

                    List<IndexModel> indexModel = scheme.GetIndexes();
                    if (outReferencesModel.Count > 0)
                    {
                        GCUtil.DataBaseInfo[ubicacion].Indexes.RemoveAll(x => x.TableName == TableName);
                        GCUtil.DataBaseInfo[ubicacion].Indexes.AddRange(indexModel);
                    }

                    result.Add("Columns", columns.Select(x => x.Name)); ;
                    result.Add("InReferences", inReferencesModel.Select(x => x.InReferencesName).Distinct());
                    result.Add("OutReferences", outReferencesModel.Select(x => x.OutReferencesName).Distinct());
                    result.Add("Indexes", indexModel.Select(x => x.Name).Distinct());
                }
                catch (Exception Ex)
                {
                    result.Add("Errores", new List<string> {
                        "Error  actualizando datos de la tabla " + TableName + "en la base de datos" + DBSettingsActual.Name,
                        Ex.Message
                    });
                }
            }
            return Json(result);
        }

        [HttpGet]
        public JsonResult SearchNewTables(int NumberConnection)
        {
            Dictionary<string, object> result = new Dictionary<string, object>();
            if (NumberConnection != 0)
            {
                DBSettings DBSettingsActual = GCUtil.ListDBSettings.Find(x => x.NumberConnection == NumberConnection);
                try
                {
                    int ubicacion = GCUtil.DataBaseInfo.FindIndex(x => x.NumberConnection == NumberConnection);
                    Scheme scheme = new Scheme(DBSettingsActual);
                    List<TableModel> tablesBD = scheme.GetTables();
                    List<TableModel> tablesNow = GCUtil.DataBaseInfo[ubicacion].Tables;
                    scheme.tableModels = tablesBD.Where(x => !tablesNow.Exists(j => j.Name == x.Name)).ToList();
                    if (scheme.tableModels.Count > 0) {
                        GCUtil.DataBaseInfo[ubicacion].Tables.AddRange(scheme.tableModels);
                        GCUtil.DataBaseInfo[ubicacion].Columns.AddRange(scheme.GetColumns());
                        GCUtil.DataBaseInfo[ubicacion].InReferences.AddRange(scheme.GetInFKs());
                        GCUtil.DataBaseInfo[ubicacion].OutReferences.AddRange(scheme.GetOutFKs());
                        GCUtil.DataBaseInfo[ubicacion].Indexes.AddRange(scheme.GetIndexes());

                        result.Add("NewTables", scheme.tableModels.Select(x => x.Name));
                        result.Add("DatasourceTables", GCUtil.DataBaseInfo[ubicacion].Tables.OrderBy(x => x.Name).ToList());
                    }
                }
                catch (Exception Ex)
                {
                    result.Add("Errores", new List<string> {
                        "Error consultando tablas en la base de datos " + DBSettingsActual.Name,
                        Ex.Message
                    });
                }
            }
            return Json(result);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
