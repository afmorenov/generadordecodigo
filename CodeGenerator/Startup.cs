﻿using CodeGenerator.Data;
using DevExpress.AspNetCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Connections;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using SignalRChat.Hubs;
using System;
using WebEssentials.AspNetCore.Pwa;

namespace CodeGenerator
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().AddNewtonsoftJson(options =>

            {
                options.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                options.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;
                options.SerializerSettings.TypeNameHandling = TypeNameHandling.Objects;
                options.SerializerSettings.PreserveReferencesHandling = PreserveReferencesHandling.Objects;
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
                options.SerializerSettings.ReferenceLoopHandling = ReferenceLoopHandling.Serialize;
                options.SerializerSettings.DateTimeZoneHandling = DateTimeZoneHandling.Utc;
                options.SerializerSettings.DateFormatString = "yyyy-MM-ddTHH:mm:ss.fff";
            });

            services.Configure<CookiePolicyOptions>(options =>
            {
                    // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                    options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });


            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_3_0);
            services.AddSignalR(hubOptions =>
            {
                hubOptions.MaximumReceiveMessageSize = long.MaxValue;
                hubOptions.EnableDetailedErrors = true;
                hubOptions.ClientTimeoutInterval = TimeSpan.FromMinutes(2);
                hubOptions.KeepAliveInterval = TimeSpan.FromMinutes(2);
                });
            services.AddProgressiveWebApp(new PwaOptions
            {
                AllowHttp = true
            });

            services.AddDevExpressControls();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            string prefix = (Environment.GetEnvironmentVariable("NETWIZARD_PREFIX") ?? "/Wizard").Trim();
            if (!string.IsNullOrEmpty(prefix))
            {
                if (!prefix.StartsWith("/"))
                {
                    prefix = "/" + prefix;
                }
                if (prefix.EndsWith("/"))
                {
                    prefix = prefix.Substring(0, prefix.Length);
                }
                Console.WriteLine("Using Prefix: " + prefix);
                app.UsePathBase(new PathString(prefix));
            }

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseDevExpressControls();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseRouting();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
                endpoints.MapRazorPages();
                //endpoints.MapBlazorHub();

                endpoints.MapHub<GenerateHub>("/GenerateHub", (config) =>
                {
                    config.ApplicationMaxBufferSize = long.MaxValue;
                    config.TransportMaxBufferSize = long.MaxValue;
                    config.Transports = HttpTransportType.WebSockets;
                });

            });

            //app.UseSignalR(routes =>
            //{
            //    routes.MapHub<GenerateHub>("/GenerateHub", (config) =>
            //         {
            //        config.ApplicationMaxBufferSize = long.MaxValue;
            //        config.TransportMaxBufferSize = long.MaxValue;
            //        config.Transports = HttpTransportType.WebSockets;
            //    });

            //});

            GCUtil.LoadDBData();
            foreach (var dbSettings in GCUtil.ListDBSettings)
            {
                Scheme scheme = new Scheme(dbSettings);

                //scheme.tableModels.Add(new Models.TableModel { Name = "w0003_lenguajes" });
                //scheme.tableModels.Add(new Models.TableModel { Name = "t997_fw_jobs" });
                //scheme.tableModels.Add(new Models.TableModel { Name = "t9971_fw_jobs_program" });
                //scheme.tableModels.Add(new Models.TableModel { Name = "w0015_recursos" });
                //scheme.tableModels.Add(new Models.TableModel { Name = "w0511_detalle_grupo_empleados" });
                //scheme.tableModels.Add(new Models.TableModel { Name = "t1602_mc_cli_contado_disp_nov" });
                //scheme.tableModels.Add(new Models.TableModel { Name = "w0556_cambio_entidades" });
                //scheme.tableModels.Add(new Models.TableModel { Name = "t0051_pp_programa_lealtad" });
                //scheme.tableModels.Add(new Models.TableModel { Name = "W0580_PERIODOS_NOMINA" });
                //scheme.tableModels.Add(new Models.TableModel { Name = "w0515_entidades_eps" });
                //scheme.tableModels.Add(new Models.TableModel { Name = "w0800_equipos" });

                GCUtil.DataBaseInfo.Add(scheme.GetDataBaseInfo());
            }
        }
    }


}
