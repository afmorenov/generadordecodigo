﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using CodeGenerator.Templates.SiesaNetCore;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace CodeGenerator.Templates.SiesaBlazor
{
    public class DinamicBusinessRulesBlazor : DinamicBusinessRules
    {
        public new string Name { get; set; } = "Reglas dinamicas";
        public new int Order { get; set; } = 7;

        public DinamicBusinessRulesBlazor() : base() { }

        public DinamicBusinessRulesBlazor(CodeGeneratorModel CodeGeneratorModel) : base(CodeGeneratorModel)
        {

        }
    }
}
