﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaBlazor
{
    public class ViewDetailBlazor
    {

        public string Name { get; set; } = "DetailView";
        public int Order { get; set; } = 4;
        public string Description { get; set; } = "Generates a Base Class Mappings for .NET";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private string Project { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public ViewDetailBlazor() { } // Para usarse con reflection

        #endregion

        public ViewDetailBlazor(CodeGeneratorModel CodeGeneratorModel)
        {
            OutputFolder = CodeGeneratorModel.PathGenerate + "/DetailsViews/";
            FullNameFile = CodeGeneratorModel.TableGenerated.Code + "DetailView.razor";

            #region asignacion
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            Project = CodeGeneratorModel.Domain;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate() 
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                if (Table.Columns.Exists(x => x.IsPrimaryKey))
                {
                    List<ColumnModel> pks = Table.Columns.Where(x => x.IsPrimaryKey).ToList();
                    List<ColumnModel> normalColumns = Table.Columns.Where(x => !x.IsPrimaryKey && !x.IsFKIn).ToList();
                    List<ColumnModel> inReferences = Table.Columns.Where(x => x.IsFKIn && !x.IsPrimaryKey).ToList();

                    sw.WriteLine(@"@inherits {0}Model", Table.Code);
                    sw.WriteLine();

                    sw.WriteLine(@"<HBusinessMessages HWindowMessage=""@WindowMessage"" />");
                    sw.WriteLine(@"<HDataGrid T=""@{0}"" CustomData=""@GetDetailDataAsync"" ", Table.Code);
                    sw.WriteLine(@"         RowInsertingAsync=""@GridOnRowInserting"" ");
                    sw.WriteLine(@"         RowUpdatingAsync=""@GridOnRowUpdating"" ");
                    sw.WriteLine(@"         RowRemovingAsync=""@GridOnRowRemoving"" ");
                    sw.WriteLine(@"         ShowFilterRow=""true"" ");
                    sw.WriteLine(@"         EditMode=""DataGridEditMode.PopupEditForm"" > ");
                    sw.WriteLine(@"     <HDataGridCommandColumn Width=""150px""></HDataGridCommandColumn> ", Table.Code);
                    foreach(var column in normalColumns)
                    {
                        if (!(column.Code.Equals("FechaCreacion") || column.Code.Equals("UsuarioCreacion") || column.Code.Equals("FechaActualizacion") || column.Code.Equals("UsuarioActualizacion")))
                        {
                    sw.WriteLine(@"     <HDataGridColumn Field=""@nameof({0}.{1})"" Caption=""@HCore.Recursos.GetRecurso(""{2}.{3}"")"" />", Table.Code, column.Code, Table.Name, column.Name);
                        }
                    }
                    sw.WriteLine(@"</HDataGrid>");
                }

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
            

        }

        private string GetPkStringCols(List<ColumnModel> pks)
        {
            string cols = "";
            for (int i = 0; i < pks.Count; i++)
            {
                if (i == 0)
                    cols += "t." + pks[i].Code;
                else
                    cols += " ,t." + pks[i].Code;
            }
            return cols;
        }


        private string firstIsNumber(string codeName)
        {
            int i = 0;
            bool firstNumber = int.TryParse(codeName[0].ToString(), out i);
            if (firstNumber)
                codeName += "F" + codeName;
            return codeName;
        }

    }
}
