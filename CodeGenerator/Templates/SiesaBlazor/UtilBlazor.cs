﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaBlazor
{
    public class UtilBlazor
    {

        public string Name { get; set; } = "UtilBlazor";
        public int Order { get; set; } = 5;
        public string Description { get; set; } = "Generates a Base Class Mappings for .NET";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private string Project { get; set; }
        private List<TableModel> Tables { get; set; }
        private TemplateModel Template { get; set; }
        public UtilBlazor() { } // Para usarse con reflection

        #endregion

        public UtilBlazor(CodeGeneratorModel CodeGeneratorModel)
        {
            OutputFolder = CodeGeneratorModel.PathGenerate + "/Util/";
            FullNameFile = $"{Name}.cs";

            #region asignacion
            Tables = CodeGeneratorModel.Tables;
            Project = CodeGeneratorModel.Domain;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate()
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                var tablesSeguridad = Tables.Where(x => GCUtil.ModuleSeguridadLicences().Contains(x.Name.ToUpper()));
                if (tablesSeguridad.Count() > 0)
                {
                    sw.WriteLine(@"// Module Seguridad");
                    foreach (var table in tablesSeguridad)
                    {
                        sw.WriteLine(@"menu.Hijos.Add(new HMenu {0} Nombre = HCore.Recursos.GetRecurso(""{2}""), Url = ""/{3}"", ViewKey = typeof({4}View).Name {1});", "{", "}", table.Name, GCUtil.GetTableOnlyName(table.Name).ToLower(), table.Code);
                    }
                }

                sw.WriteLine();

                var tablesLicences = Tables.Where(x => !GCUtil.ModuleSeguridadLicences().Contains(x.Name.ToUpper()));
                if (tablesLicences.Count() > 0)
                {
                    sw.WriteLine(@"// Module Licences");
                    foreach (var table in tablesLicences)
                    {
                        sw.WriteLine(@"menu.Hijos.Add(new HMenu {0} Nombre = HCore.Recursos.GetRecurso(""{2}""), Url = ""/{3}"", ViewKey = typeof({4}View).Name {1});", "{", "}", table.Name, GCUtil.GetTableOnlyName(table.Name).ToLower(), table.Code);
                    }
                }

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }


        }

        private string GetPkStringCols(List<ColumnModel> pks)
        {
            string cols = "";
            for (int i = 0; i < pks.Count; i++)
            {
                if (i == 0)
                    cols += "t." + pks[i].Code;
                else
                    cols += " ,t." + pks[i].Code;
            }
            return cols;
        }


        private string firstIsNumber(string codeName)
        {
            int i = 0;
            bool firstNumber = int.TryParse(codeName[0].ToString(), out i);
            if (firstNumber)
                codeName += "F" + codeName;
            return codeName;
        }

    }
}
