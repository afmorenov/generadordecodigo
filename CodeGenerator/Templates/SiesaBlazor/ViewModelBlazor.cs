﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaBlazor
{
    public class ViewModelBlazor
    {

        public string Name { get; set; } = "ViewModel";
        public int Order { get; set; } = 3;
        public string Description { get; set; } = "Generates a Base Class Mappings for .NET";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private string Project { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public ViewModelBlazor() { } // Para usarse con reflection

        #endregion

        public ViewModelBlazor(CodeGeneratorModel CodeGeneratorModel)
        {
            OutputFolder = CodeGeneratorModel.PathGenerate + "/ViewModels/";
            FullNameFile = CodeGeneratorModel.TableGenerated.Code + "ViewModel.cs";

            #region asignacion
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            Project = CodeGeneratorModel.Domain;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate() 
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                if (Table.Columns.Exists(x=>x.IsPrimaryKey))
                {
                    List<ColumnModel> pks = Table.Columns.Where(x => x.IsPrimaryKey).ToList();
                    List<ColumnModel> inReferences = Table.Columns.Where(x => x.IsFKIn && !x.IsPrimaryKey).ToList();

                    sw.WriteLine(@"using System;");
                    sw.WriteLine(@"using System.Collections.Generic;");
                    sw.WriteLine(@"using System.Threading;");
                    sw.WriteLine(@"using System.Threading.Tasks;");
                    sw.WriteLine(@"using DevExtreme.AspNet.Data;");
                    sw.WriteLine(@"using DevExtreme.AspNet.Data.ResponseModel;");
                    sw.WriteLine(@"using Hefestos.Frontend.Core.Blazor;");
                    sw.WriteLine(@"using Siesa.Seguridad.Application;");
                    sw.WriteLine(@"using Siesa.{0}.Entidades;", Table.Comment);
                    sw.WriteLine();
                    sw.WriteLine(@"namespace {0}.{1}.ViewModels", Project, Table.Comment);
                    sw.WriteLine(@"{");
                    sw.WriteLine(@"   public class {0}Model : ViewModelBaseAsync<{0}>", Table.Code);
                    sw.WriteLine(@"   {");
                    sw.WriteLine();

                    sw.WriteLine(@"    public {0}Model() : base()",Table.Code);
                    sw.WriteLine(@"    {");
                    sw.WriteLine();
                    sw.WriteLine(@"    }");
                    sw.WriteLine();

                    sw.WriteLine(@"    public override Task<bool> PostOnEdit()");
                    sw.WriteLine(@"    {");
                    foreach (var column in inReferences)
                    {
                        var referecia = Table.InReferences.FirstOrDefault(x => x.ColumnName == column.Name);
                        sw.WriteLine(@"        Select_{0} = SGFState.Manager.GetLogicaNegocio<{1}>().BuscarPorId(x => x.{2} == Model.{0});", column.Code, GCUtil.CleanName(referecia.ParentTableName), GCUtil.GetColumnCodeParentTable(referecia.ParentColumnName));
                    }
                    sw.WriteLine(@"         return base.PostOnEdit();");
                    sw.WriteLine(@"    }");
                    sw.WriteLine();

                    foreach (var column in inReferences)
                    {
                        var referecia = Table.InReferences.FirstOrDefault(x => x.ColumnName == column.Name);
                        sw.WriteLine(@"    private {0} Field_{1} {{ get; set; }}", GCUtil.CleanName(referecia.ParentTableName), column.Code);
                        var ss = GCUtil.CleanName(referecia.ParentTableName);
                        sw.WriteLine(@"    public {0} Select_{1}", GCUtil.CleanName(referecia.ParentTableName), column.Code);
                        sw.WriteLine(@"    {");
                        sw.WriteLine(@"        get => Field_{0};", column.Code);
                        sw.WriteLine(@"        set");
                        sw.WriteLine(@"        {");
                        sw.WriteLine(@"            Field_{0} = value;", column.Code);
                        sw.WriteLine(@"            Model.{0} = value.{1};", column.Code, GCUtil.GetColumnCodeParentTable(referecia.ParentColumnName));
                        sw.WriteLine(@"        }");
                        sw.WriteLine(@"    }");
                        sw.WriteLine();

                        sw.WriteLine(@"    public async Task<LoadResult> Get{0}DataAsync(DataSourceLoadOptionsBase options, CancellationToken cancellationToken)", GCUtil.CleanName(referecia.ParentTableName));
                        sw.WriteLine(@"    {");
                        sw.WriteLine(@"        return await this.GetDataAsync<{0}>(options, cancellationToken);", GCUtil.CleanName(referecia.ParentTableName));
                        sw.WriteLine(@"    }");
                        sw.WriteLine();
                    }

                    sw.WriteLine(@"    #region DETALLE");
                    sw.WriteLine();
                    sw.WriteLine(@"    public async Task<LoadResult> GetDetailDataAsync(DataSourceLoadOptionsBase options, CancellationToken cancellationToken)");
                    sw.WriteLine(@"    {");
                    sw.WriteLine(@"        //Reemplazar RowidPadre por el campo referencial del detalle");
                    sw.WriteLine(@"        return await this.GetDataAsync<{0}>(/*x => x.RowidPadre == GuidPadre,*/ options, cancellationToken);", Table.Code);
                    sw.WriteLine(@"    }");
                    sw.WriteLine();
                    sw.WriteLine(@"    public async Task GridOnRowInserting(Dictionary<string, object> newValue)");
                    sw.WriteLine(@"    {");
                    sw.WriteLine(@"        await SetProcessingGridDetail();");
                    sw.WriteLine(@"        try");
                    sw.WriteLine(@"        {");
                    sw.WriteLine(@"            {0} data = PopulateData(new {0}(), newValue);", Table.Code);
                    sw.WriteLine(@"            //Reemplazar RowidPadre por el campo referencial del detalle");
                    sw.WriteLine(@"            //data.RowidPadre = GuidPadre;");
                    sw.WriteLine(@"            //data.IndEstado = Select_IndEstado.Key;");
                    sw.WriteLine(@"            SGFState.Manager.GetLogicaNegocios<{0}>().Adicionar(data);", Table.Code);
                    sw.WriteLine(@"            WindowMessage.SetSuccessMessage(GetResource(""System.RecordSaved""));");
                    sw.WriteLine(@"        }");
                    sw.WriteLine(@"        catch (Exception e)");
                    sw.WriteLine(@"        {");
                    sw.WriteLine(@"            WindowMessage.AlertType = AlertType.danger;");
                    sw.WriteLine(@"            WindowMessage.Message = e.GetFullExceptionMessage();");
                    sw.WriteLine(@"        }");
                    sw.WriteLine(@"        await SetProcessingFinish();");
                    sw.WriteLine(@"    }");
                    sw.WriteLine();
                    sw.WriteLine(@"    public async Task GridOnRowUpdating({0} data, Dictionary<string, object> newValue)", Table.Code);
                    sw.WriteLine(@"    {");
                    sw.WriteLine(@"        await SetProcessingGridDetail();");
                    sw.WriteLine(@"        try");
                    sw.WriteLine(@"        {");
                    sw.WriteLine(@"            data = PopulateData(data, newValue);");
                    sw.WriteLine(@"            //data.IndEstado = Select_IndEstado.Key;");
                    sw.WriteLine(@"            SGFState.Manager.GetLogicaNegocios<{0}>().Modificar(data);", Table.Code);
                    sw.WriteLine(@"            WindowMessage.SetSuccessMessage(GetResource(""System.RecordSaved""));");
                    sw.WriteLine(@"        }");
                    sw.WriteLine(@"        catch (Exception e)");
                    sw.WriteLine(@"        {");
                    sw.WriteLine(@"            WindowMessage.AlertType = AlertType.danger;");
                    sw.WriteLine(@"            WindowMessage.Message = e.GetFullExceptionMessage();");
                    sw.WriteLine(@"        }");
                    sw.WriteLine(@"        await SetProcessingFinish();");
                    sw.WriteLine(@"    }");
                    sw.WriteLine();
                    sw.WriteLine(@"    public async Task GridOnRowRemoving({0} data)", Table.Code);
                    sw.WriteLine(@"    {");
                    sw.WriteLine(@"        await SetProcessingGridDetail();");
                    sw.WriteLine(@"        try");
                    sw.WriteLine(@"        {");
                    sw.WriteLine(@"            SGFState.Manager.GetLogicaNegocios<{0}>().Eliminar(data);", Table.Code);
                    sw.WriteLine(@"            WindowMessage.SetSuccessMessage(GetResource(""System.RecordRemoved""));");
                    sw.WriteLine(@"        }");
                    sw.WriteLine(@"        catch (Exception e)");
                    sw.WriteLine(@"        {");
                    sw.WriteLine(@"            WindowMessage.AlertType = AlertType.danger;");
                    sw.WriteLine(@"            WindowMessage.Message = e.GetFullExceptionMessage();");
                    sw.WriteLine(@"        }");
                    sw.WriteLine(@"        await SetProcessingFinish();");
                    sw.WriteLine(@"    }");
                    sw.WriteLine();
                    sw.WriteLine(@"    #endregion");

                    sw.WriteLine(@"   }");
                    sw.WriteLine(@"}");
                }

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
            

        }

        private string GetPkStringCols(List<ColumnModel> pks)
        {
            string cols = "";
            for (int i = 0; i < pks.Count; i++)
            {
                if (i == 0)
                    cols += "t." + pks[i].Code;
                else
                    cols += " ,t." + pks[i].Code;
            }
            return cols;
        }


        private string firstIsNumber(string codeName)
        {
            int i = 0;
            bool firstNumber = int.TryParse(codeName[0].ToString(), out i);
            if (firstNumber)
                codeName += "F" + codeName;
            return codeName;
        }

    }
}
