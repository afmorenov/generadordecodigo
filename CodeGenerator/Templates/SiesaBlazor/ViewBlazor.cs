﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaBlazor
{
    public class ViewBlazor
    {

        public string Name { get; set; } = "View";
        public int Order { get; set; } = 4;
        public string Description { get; set; } = "Generates a Base Class Mappings for .NET";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private string Project { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public ViewBlazor() { } // Para usarse con reflection

        #endregion

        public ViewBlazor(CodeGeneratorModel CodeGeneratorModel)
        {
            OutputFolder = CodeGeneratorModel.PathGenerate + "/Views/";
            FullNameFile = CodeGeneratorModel.TableGenerated.Code + "View.razor";

            #region asignacion
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            Project = CodeGeneratorModel.Domain;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate() 
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                if (Table.Columns.Exists(x=>x.IsPrimaryKey))
                {
                    List<ColumnModel> pks = Table.Columns.Where(x => x.IsPrimaryKey).ToList();
                    List<ColumnModel> normalColumns = Table.Columns.Where(x => !x.IsPrimaryKey && !x.IsFKIn).ToList();
                    List<ColumnModel> inReferences = Table.Columns.Where(x => x.IsFKIn && !x.IsPrimaryKey).ToList();

                    sw.WriteLine(@"@page ""/{0}""", GCUtil.GetTableOnlyName(Table.Name).ToLower());
                    sw.WriteLine(@"@inherits {0}Model",Table.Code);
                    sw.WriteLine();

                    sw.WriteLine(@"<HToolBar HTitleMaster=""@HCore.Recursos.GetRecurso(""{0}"")"" HVisible=""@FindWindowVisible"" HType=""@ToolBarType.Find"" IsLoading=""@IsLoading"" WindowMessage=""@WindowMessage"" >", Table.Name);
                    sw.WriteLine(@"     <HDataGrid @ref=""@QueryGrid"" T=""@{0}"" CustomData=""@GetDataAsync"" ShowFilterRow=""@ShowFilterRow"" @bind-SingleSelectedDataRow=""@SelectedModel"" ColumnResizeMode=""DataGridColumnResizeMode.Component"" >", Table.Code);
                    sw.WriteLine(@"         <HeaderTemplate>");
                    sw.WriteLine(@"             <HDxToolBar HType=""@ToolBarType.Find"" HOnNew=""@OnNew"" HOnEdit=""@OnEdit"" HOnShowFilterRow=""@OnShowFilterRow"" HOnRefresh=""@OnRefresh"" ></HDxToolBar>");
                    //sw.WriteLine(@"             <HBusinessMessages HWindowMessage=""@WindowMessage""></HBusinessMessages>");
                    sw.WriteLine(@"         </HeaderTemplate>");
                    sw.WriteLine(@"         <Columns>");
                    foreach(var column in normalColumns)
                    {
                        if (!(column.Code.Equals("FechaCreacion") || column.Code.Equals("UsuarioCreacion") || column.Code.Equals("FechaActualizacion") || column.Code.Equals("UsuarioActualizacion")))
                        {
                            if (column.Code.Equals("IndEstado"))
                            {
                                sw.WriteLine(@"             <DxDataGridColumn Field=""@nameof({0}.{1})"" Caption=""@HCore.Recursos.GetRecurso(""{2}.{3}"")"">", Table.Code, column.Code, Table.Name, column.Name);
                                sw.WriteLine(@"                 <DisplayTemplate>");
                                sw.WriteLine(@"                     @{");
                                sw.WriteLine(@"                             var indEstado = (context as {0}).{1};", Table.Code, column.Code);
                                sw.WriteLine(@"                             @if (indEstado == 1)");
                                sw.WriteLine(@"                             {");
                                sw.WriteLine(@"                                 <label>Activo</label>");
                                sw.WriteLine(@"                             } else if (indEstado == 0) ");
                                sw.WriteLine(@"                             {");
                                sw.WriteLine(@"                                 <label>Inactivo</label>");
                                sw.WriteLine(@"                             }");
                                sw.WriteLine(@"                     }");
                                sw.WriteLine(@"                 </DisplayTemplate>");
                                sw.WriteLine(@"             </DxDataGridColumn>");
                            }
                            sw.WriteLine(@"             <DxDataGridColumn Field=""@nameof({0}.{1})"" Caption=""@HCore.Recursos.GetRecurso(""{2}.{3}"")"" />", Table.Code, column.Code, Table.Name, column.Name);
                        }
                    }
                    foreach (var column in inReferences)
                    {
                        var referecia = Table.InReferences.FirstOrDefault(x => x.ColumnName == column.Name);
                        sw.WriteLine(@"             <DxDataGridColumn Field=""{0}_{1}.Descripcion"" Caption=""@HCore.Recursos.GetRecurso(""{2}.Descripcion"")"" /> ", GCUtil.GetTablePrefix(referecia.ParentTableName), column.Code, GCUtil.CleanName(referecia.ParentTableName));
                    }
                    sw.WriteLine(@"         </Columns>");
                    sw.WriteLine(@"     </HDataGrid>");
                    sw.WriteLine(@"</HToolBar>");

                    sw.WriteLine(@"<EditForm Model=""@Model"" Context=""EditContext"">");
                    sw.WriteLine(@"     <HToolBar HTitleMaster=""@HCore.Recursos.GetRecurso(""{0}"")"" HVisible=""@(!FindWindowVisible)"" HType=""@ToolBarType.Master"" WindowMessage=""@WindowMessage"" >", Table.Name);
                    sw.WriteLine(@"         <div style=""margin - top: 10px; border: 1px solid rgba(34, 34, 34, 0.125); border - radius: 0.25rem; padding - bottom: 10px; "">");
                    sw.WriteLine(@"             <HDxToolBar HType=""@ToolBarType.Master"" HOnCancel=""@OnCancel"" HOnNew=""@OnNew"" HOnSave=""@(() => OnSave(EditContext))"" HOnFind=""@OnFind"" HOnRemove=""@(() => OnRemove(EditContext))"" />");
                    //sw.WriteLine(@"         <HBusinessMessages HWindowMessage=""@WindowMessage""></HBusinessMessages>");
                    sw.WriteLine(@"             <DataAnnotationsValidator />");
                    sw.WriteLine(@"             <ValidationSummary />");
                    sw.WriteLine(@"             <HFormLayout>");
                    foreach (var column in normalColumns)
                    {
                        if (!(column.Code.Equals("FechaCreacion") || column.Code.Equals("UsuarioCreacion") || column.Code.Equals("FechaActualizacion") || column.Code.Equals("UsuarioActualizacion")))
                        {
                            sw.WriteLine(@"                 <HFormLayoutItem Caption=""@HCore.Recursos.GetRecurso(""{0}.{1}"")"" ColSpanMd=""12"">", Table.Name, column.Name);
                            sw.WriteLine(@"                     <Template>");

                            if (column.Type == "varchar" || column.Type == "char" || column.Type == "nvarchar")
                                sw.WriteLine(@"                         <HTextBox @bind-Text=""@Model.{0}"" />", column.Code);

                            if (column.Type == "datetime" || column.Type == "timestamp")
                                sw.WriteLine(@"                         <HDateEdit @bind-Date=""@Model.{0}"" />", column.Code);

                            if (column.Type == "int" || column.Type == "bigint" || column.Type == "smallint" || column.Type == "tinyint")
                                sw.WriteLine(@"                         <HSpinEdit @bind-Value=""@Model.{0}"" />", column.Code);

                            if (column.Type == "bit" || column.Type == "byte")
                                sw.WriteLine(@"                         <HCheckBox @bind-Checked=""@Model.{0}"" />", column.Code);

                            sw.WriteLine(@"                     </Template>");
                            sw.WriteLine(@"                 </HFormLayoutItem>");
                        }
                    }
                    foreach (var column in inReferences)
                    {
                        var referecia = Table.InReferences.FirstOrDefault(x => x.ColumnName == column.Name);

                        sw.WriteLine(@"                 <HFormLayoutItem Caption=""@HCore.Recursos.GetRecurso(""{0}.{1}"")"" ColSpanMd=""12"">", Table.Code, column.Code);
                        sw.WriteLine(@"                     <Template>");

                        sw.WriteLine(@"                         <HComboBox T=""{0}"" TValue=""{0}"" CustomData=""@Get{0}DataAsync"" @bind-Value=""@Select_{1}"" EditFormat=""({{0}} - {{1}})"">", GCUtil.CleanName(referecia.ParentTableName), column.Code);
                        sw.WriteLine(@"                             <DxListEditorColumn FieldName=""@nameof({0}.{1})"" Caption=""@HCore.Recursos.GetRecurso(""{0}.{1}"")"" />", GCUtil.CleanName(referecia.ParentTableName), GCUtil.GetColumnCodeParentTable(referecia.ParentColumnName));
                        sw.WriteLine(@"                         </HComboBox>");

                        sw.WriteLine(@"                     </Template>");
                        sw.WriteLine(@"                 </HFormLayoutItem>");
                    }
                    sw.WriteLine(@"             </HFormLayout>");
                    sw.WriteLine(@"         </div>");
                    sw.WriteLine();
                    sw.WriteLine(@"         <!-- DETALLES -->");
                    sw.WriteLine(@"         @*");
                    sw.WriteLine(@"         <div class=""card"" style=""margin: 15px 0px; "">");
                    sw.WriteLine(@"             <div class=""card-header"">");
                    sw.WriteLine(@"                 <DxTabs style=""width:100%;"" @bind-ActiveTabIndex=""@ActiveTabIndex"" CssClass=""tabs__detail"">");
                    sw.WriteLine(@"                     <DxTab Text=""@HCore.Recursos.GetRecurso(""DETALLE"")"" />");
                    sw.WriteLine(@"                 </DxTabs>");
                    sw.WriteLine(@"             </div>");
                    sw.WriteLine(@"             <div class=""card-body"">");
                    sw.WriteLine(@"                 @switch (ActiveTabIndex)");
                    sw.WriteLine(@"                 {");
                    sw.WriteLine(@"                     case 0:");
                    sw.WriteLine(@"                         <DetailView GuidPadre=""@Model.IdPadre""></DetailView>");
                    sw.WriteLine(@"                         break;");
                    sw.WriteLine(@"                 }");
                    sw.WriteLine(@"             </div>");
                    sw.WriteLine(@"         </div>");
                    sw.WriteLine(@"         *@");
                    sw.WriteLine(@"     </HToolBar>");
                    sw.WriteLine(@"</EditForm>");

                }

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
            

        }

        private string GetPkStringCols(List<ColumnModel> pks)
        {
            string cols = "";
            for (int i = 0; i < pks.Count; i++)
            {
                if (i == 0)
                    cols += "t." + pks[i].Code;
                else
                    cols += " ,t." + pks[i].Code;
            }
            return cols;
        }


        private string firstIsNumber(string codeName)
        {
            int i = 0;
            bool firstNumber = int.TryParse(codeName[0].ToString(), out i);
            if (firstNumber)
                codeName += "F" + codeName;
            return codeName;
        }

    }
}
