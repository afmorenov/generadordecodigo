﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaJavaHibernate
{
    public class EntityBase
    {

        public string Name { get; set; } = "Entity Base";
        public int Order { get; set; } = 2;
        public string NameFile { get; set; } = ".java";
        public string Description { get; set; } = "Generates a Java BLL Class for use with Hibernate.";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public EntityBase() { } // Para usarse con reflection

        #endregion

        public EntityBase(CodeGeneratorModel CodeGeneratorModel)
        {
            #region asignacion
            OutputFolder = CodeGeneratorModel.PathGenerate + "/SIESA/" + CodeGeneratorModel.TableGenerated.NameProject + "/hibernate/src/main/java/com/siesa/entities/base/";
            FullNameFile = "Base" + CodeGeneratorModel.TableGenerated.Code + this.NameFile;
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate() 
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                List<ColumnModel> ColumnsPrimaryKeys = Table.Columns.Where(x => x.IsPrimaryKey).ToList();

                sw.WriteLine(@"package com.siesa.entities.base;");
                if (Table.Columns.Exists(x => GCUtil.GetJavaDataType(x) == "Calendar"))
                    sw.WriteLine(@"import java.sql.Timestamp;");
                if (Table.Columns.Exists(x => GCUtil.GetJavaDataType(x) == "BigDecimal"))
                    sw.WriteLine(@"import java.math.BigDecimal;");
                sw.WriteLine(@"import javax.persistence.*;");
                sw.WriteLine();
                sw.WriteLine(@"import org.hibernate.annotations.GenericGenerator;");
                sw.WriteLine(@"import org.hibernate.annotations.Parameter;");
                sw.WriteLine();
                sw.WriteLine(@"import com.siesa.framework.data.BaseEntity;");
                sw.WriteLine(@"import com.siesa.framework.persistence.Options;");
                sw.WriteLine();
                sw.WriteLine(@"import java.util.Calendar;");
                sw.WriteLine();
                sw.WriteLine(@"@MappedSuperclass");
                sw.WriteLine(@"public class Base{0}  extends BaseEntity<Base{0}> {1}", Table.Code, "{");
                sw.WriteLine();
                sw.WriteLine(@"   private static final long serialVersionUID = 1L;");
                sw.WriteLine();
                sw.WriteLine(@"   public Base{0}() {1}", Table.Code, "{");
                sw.WriteLine(@"      super();");
                sw.WriteLine(@"   }");
                sw.WriteLine();
                sw.WriteLine(@"   public Base{0}(Base{0} data, Options option)", Table.Code);
                sw.WriteLine(@"   {");
                sw.WriteLine(@"      super();");
                sw.WriteLine(@"      if (option == Options.Light || option == Options.Me || option == Options.All)");
                sw.WriteLine(@"      {");
                sw.WriteLine(@"         //Options.Light");
                foreach (var column in ColumnsPrimaryKeys)
                {
                    sw.WriteLine(@"         this.set{0}(data.get{0}());", column.CodeJava);
                }
                foreach (var index in Table.Indexes.Where(x=> !ColumnsPrimaryKeys.Exists(j=>j.Name == x.ColumnName)))
                {
                    sw.WriteLine(@"         this.set{0}(data.get{0}());", index.ColumnCode);
                }
                sw.WriteLine();
                sw.WriteLine(@"         if (option == Options.Me || option == Options.All)");
                sw.WriteLine(@"         {");
                sw.WriteLine();
                foreach (var column in Table.Columns.Where(x=>!x.IsPrimaryKey))
                {
                    sw.WriteLine(@"         this.set{0}(data.get{0}());", column.CodeJava);
                }
                sw.WriteLine(@"             }");
                sw.WriteLine(@"          }");
                sw.WriteLine(@"       }");
                sw.WriteLine();

                if (ColumnsPrimaryKeys.Count == 1 && ColumnsPrimaryKeys.Exists(x => x.Name.EndsWith("rowid")))
                {
                    ColumnModel columnPK = ColumnsPrimaryKeys[0];
                    if (columnPK.IsIdentity)
                    {
                        sw.WriteLine(@"   {0}", "@Id");
                        sw.WriteLine(@"   {0}Column(name = ""{1}"", nullable = {2})", "@", columnPK.Name, (!columnPK.IsRequired).ToString().ToLower());
                        sw.WriteLine(@"   @GeneratedValue(generator = ""gen_{0}"")", columnPK.Name);
                        sw.WriteLine(@"   @GenericGenerator(name = ""gen_{0}"", strategy = ""native"",", columnPK.Name);
                        sw.WriteLine(@"      parameters = {");
                        sw.WriteLine(@"         @Parameter(name = ""sequence_name"", value = ""sq_{0}"")", columnPK.Name);
                        sw.WriteLine(@"      })");
                        sw.WriteLine(@"   private  {0} {1};", GCUtil.GetJavaDataType(columnPK), GCUtil.CamelCase(columnPK.CodeJava));
                        sw.WriteLine();
                        sw.WriteLine(@"   public {0} get{1}() {2}", GCUtil.GetJavaDataType(columnPK), columnPK.CodeJava, "{");
                        sw.WriteLine(@"      return {0};", GCUtil.CamelCase(columnPK.CodeJava));
                        sw.WriteLine(@"   }");
                        sw.WriteLine();
                        sw.WriteLine(@"   public void set{1}({0} data) {2}", GCUtil.GetJavaDataType(columnPK), columnPK.CodeJava, "{");
                        sw.WriteLine(@"      this.{0} = data;", GCUtil.CamelCase(columnPK.CodeJava));
                        sw.WriteLine(@"   }");
                        sw.WriteLine();
                    }
                    else
                    {
                        sw.WriteLine(@"   {0}", "@Id");
                        sw.WriteLine(@"   {0}Column(name = ""{1}"", nullable = {2})", "@", columnPK.Name, (!columnPK.IsRequired).ToString().ToLower());
                        sw.WriteLine(@"   @TableGenerator(table = ""w0013_secuencias"", name = ""{0}PK"", allocationSize = 1, initialValue = 1, pkColumnName = ""c0013_id"", valueColumnName = ""c0013_secuencia"", pkColumnValue = ""{0}PK"")", Table.Code);
                        sw.WriteLine(@"   @GeneratedValue(strategy = GenerationType.TABLE, generator = ""{0}PK"")", Table.Code);
                        sw.WriteLine(@"   private  {0} {1};", GCUtil.GetJavaDataType(columnPK), GCUtil.CamelCase(columnPK.CodeJava));
                        sw.WriteLine();
                        sw.WriteLine(@"   public {0} get{1}() {2}", GCUtil.GetJavaDataType(columnPK), columnPK.CodeJava, "{");
                        sw.WriteLine(@"      return {0};", GCUtil.CamelCase(columnPK.CodeJava));
                        sw.WriteLine(@"   }");
                        sw.WriteLine();
                        sw.WriteLine(@"   public void set{1}({0} data) {2}", GCUtil.GetJavaDataType(columnPK), columnPK.CodeJava, "{");
                        sw.WriteLine(@"      this.{0} = data;", GCUtil.CamelCase(columnPK.CodeJava));
                        sw.WriteLine(@"   }");
                        sw.WriteLine();
                    }
                }
                else
                {
                    foreach (var columnPK in ColumnsPrimaryKeys) {
                        sw.WriteLine(@"   {0}", "@Id");
                        sw.WriteLine(@"   {0}Column(name = ""{1}"", nullable = {2} {3})", "@", columnPK.Name, (!columnPK.IsRequired).ToString().ToLower(), (GCUtil.GetJavaDataType(columnPK) == "String" ? ", length = " + columnPK.Length : ""));
                        sw.WriteLine(@"   private  {0} {1};", GCUtil.GetJavaDataType(columnPK), GCUtil.CamelCase(columnPK.CodeJava));
                        sw.WriteLine();
                        sw.WriteLine(@"   public {0} get{1}() {2}", GCUtil.GetJavaDataType(columnPK), columnPK.CodeJava, "{");
                        sw.WriteLine(@"      return {0};", GCUtil.CamelCase(columnPK.CodeJava));
                        sw.WriteLine(@"   }");
                        sw.WriteLine();
                        sw.WriteLine(@"   public void set{1}({0} data) {2}", GCUtil.GetJavaDataType(columnPK), columnPK.CodeJava, "{");
                        sw.WriteLine(@"      this.{0} = data;", GCUtil.CamelCase(columnPK.CodeJava));
                        sw.WriteLine(@"   }");
                        sw.WriteLine();
                    }
                }
                foreach (var column in Table.Columns.Where(x=>!x.IsPrimaryKey))
                {
                    sw.WriteLine(@"   {0}Column(name = ""{1}"", nullable = {2} {3})", "@", column.Name, (!column.IsRequired).ToString().ToLower(), (GCUtil.GetJavaDataType(column) == "String" ? ", length = " + column.Length : ""));
                    sw.WriteLine(@"   private  {0} {1};", GCUtil.GetJavaDataType(column), GCUtil.CamelCase(column.CodeJava));
                    sw.WriteLine();
                    sw.WriteLine(@"   public {0} get{1}() {2}", GCUtil.GetJavaDataType(column), column.CodeJava, "{");
                    sw.WriteLine(@"      return {0};", GCUtil.CamelCase(column.CodeJava));
                    sw.WriteLine(@"   }");
                    sw.WriteLine();
                    sw.WriteLine(@"   public void set{1}({0} data) {2}", GCUtil.GetJavaDataType(column), column.CodeJava, "{");
                    sw.WriteLine(@"      this.{0} = data;", GCUtil.CamelCase(column.CodeJava));
                    sw.WriteLine(@"   }");
                    sw.WriteLine();
                }
                sw.WriteLine(@"}");

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
            

        }

    }
}
