﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaJavaHibernate
{
    public class BLL
    {

        public string Name { get; set; } = "BLL";
        public int Order { get; set; } = 3;
        public string NameFile { get; set; } = "BLL.java";
        public string Description { get; set; } = "Generates a Java BLL Class for use with Hibernate.";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public BLL() { } // Para usarse con reflection

        #endregion

        public BLL(CodeGeneratorModel CodeGeneratorModel)
        {
            #region asignacion
            OutputFolder = CodeGeneratorModel.PathGenerate + "/SIESA/"+ CodeGeneratorModel.TableGenerated.NameProject + "/hibernate/src/main/java/com/siesa/bll/";
            FullNameFile = CodeGeneratorModel.TableGenerated.Code + this.NameFile;
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate() 
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                sw.WriteLine(@"package com.siesa.bll;");
                sw.WriteLine();
                sw.WriteLine(@"import com.siesa.bll.base.*;");
                sw.WriteLine(@"import com.siesa.dal.*;");
                sw.WriteLine(@"import com.siesa.entities.*;");
                sw.WriteLine();
                sw.WriteLine(@"import com.siesa.framework.application.UNOException;");
                sw.WriteLine();
                sw.WriteLine(@"import org.hibernate.Session;");
                sw.WriteLine();
                sw.WriteLine(@"   public class {0}BLL extends Base{0}BLL", Table.Code);
                sw.WriteLine(@"   {");
                sw.WriteLine();
                sw.WriteLine(@"      public {0}BLL(String sessionName) throws UNOException", Table.Code);
                sw.WriteLine(@"      {");
                sw.WriteLine(@"         super(sessionName);");
                sw.WriteLine(@"         dalNH = new {0}DAO(this.getSession());", Table.Code);
                sw.WriteLine(@"      }");
                sw.WriteLine();
                sw.WriteLine(@"      public {0}BLL(Session session)", Table.Code);
                sw.WriteLine(@"      {");
                sw.WriteLine(@"         super(session);");
                sw.WriteLine(@"         dalNH = new {0}DAO(session);", Table.Code);
                sw.WriteLine(@"      }");
                sw.WriteLine();
                sw.WriteLine(@"      public void setCommonRules({0} data)", Table.Code);
                sw.WriteLine(@"      {");
                sw.WriteLine(@"         super.setCommonRules(data);");
                sw.WriteLine(@"         //Se agregan las reglas a las foraneas");
                sw.WriteLine(@"      }");
                sw.WriteLine();
                sw.WriteLine(@"      public void setSaveRules({0} data)", Table.Code);
                sw.WriteLine(@"      {");
                sw.WriteLine(@"         super.setSaveRules(data);");
                sw.WriteLine(@"      }");
                sw.WriteLine();
                sw.WriteLine(@"      public void setUpdateRules({0} data)", Table.Code);
                sw.WriteLine(@"      {");
                sw.WriteLine(@"         super.setUpdateRules(data);");
                sw.WriteLine(@"      }");
                sw.WriteLine();
                sw.WriteLine(@"      public void setDeleteRules({0} data)", Table.Code);
                sw.WriteLine(@"      {");
                sw.WriteLine(@"         super.setDeleteRules(data);");
                sw.WriteLine(@"      } ");
                sw.WriteLine();
                sw.WriteLine(@"      public void setSelectMeRules({0} data)", Table.Code);
                sw.WriteLine(@"      {");
                sw.WriteLine(@"         super.setSelectMeRules(data);");
                sw.WriteLine(@"      } ");
                sw.WriteLine();
                sw.WriteLine(@"   }");
                sw.WriteLine(@"   //adicionar metodo a la clase /UNOWeb BLL/src/Siesa/UNOWeb/BusinessBLL.java");
                sw.WriteLine(@"   //public IBLL<{0}> get{0}BLL()", Table.Code);
                sw.WriteLine(@"   //   throws UNOException {");
                sw.WriteLine(@"   //   return new {0}BLL(sessionName);", Table.Code);
                sw.WriteLine(@"   //}");

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
            

        }

    }
}
