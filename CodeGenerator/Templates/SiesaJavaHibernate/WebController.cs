﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaJavaHibernate
{
    public class WebController
    {

        public string Name { get; set; } = "Web Controlador";
        public int Order { get; set; } = 7;
        public string NameFile { get; set; } = ".java";
        public string Description { get; set; } = "Generates a JAVA class Controller.";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public WebController() { } // Para usarse con reflection

        #endregion

        public WebController(CodeGeneratorModel CodeGeneratorModel)
        {
            #region asignacion
            OutputFolder = CodeGeneratorModel.PathGenerate + "/SIESA/" + CodeGeneratorModel.TableGenerated.NameProject + "/web/src/main/java/com/siesa/unoweb/controlador/";
            FullNameFile = "Controlador" + CodeGeneratorModel.TableGenerated.Code + this.NameFile;
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate()
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                sw.WriteLine(@"package com.siesa.unoweb.controlador;");
                sw.WriteLine();
                sw.WriteLine(@"import java.util.ArrayList;");
                sw.WriteLine(@"import java.util.List;");
                sw.WriteLine();
                sw.WriteLine(@"import javax.servlet.http.HttpServletRequest;");
                sw.WriteLine(@"import javax.servlet.http.HttpServletResponse;");
                sw.WriteLine();
                sw.WriteLine(@"import org.json.simple.JSONArray;");
                sw.WriteLine(@"import org.json.simple.JSONObject;");
                sw.WriteLine(@"import org.springframework.validation.BindException;");
                sw.WriteLine(@"import org.springframework.web.servlet.ModelAndView;");
                sw.WriteLine();
                sw.WriteLine(@"import com.siesa.business.{0}BusinessBLL;", GetBusinessByModule());                
                sw.WriteLine(@"import com.siesa.entities.{0};", Table.Code);
                sw.WriteLine(@"import com.siesa.framework.application.UNOException;");
                sw.WriteLine(@"import com.siesa.framework.persistence.Actions;");
                sw.WriteLine(@"import com.siesa.framework.persistence.Options;");
                sw.WriteLine(@"import com.siesa.unoweb.general.negocio.GeneralComun;");
                sw.WriteLine(@"import com.siesa.unoweb.general.negocio.Modulos;");
                sw.WriteLine(@"import com.siesa.unoweb.general.negocio.SP_Parametros;");
                sw.WriteLine(@"import com.siesa.unoweb.general.negocio.integration.ConsultantesGeneral1_2Delegate;");
                sw.WriteLine(@"import com.siesa.unoweb.general.negocio.transfer.BeanDTO;");
                sw.WriteLine(@"import com.siesa.unoweb.general.negocio.transfer.ConsultasRtoDTO;");
                sw.WriteLine(@"import com.siesa.unoweb.general.negocio.transfer.GenConsultaMaestroDTO;");
                sw.WriteLine(@"import com.siesa.unoweb.seguridad.negocio.integration.ConsultantesSeguridad1_1Delegate;");
                sw.WriteLine(@"import com.siesa.unoweb.seguridad.negocio.transfer.PermisosMetodosXClaseRtoDTO;");
                sw.WriteLine(@"import com.siesa.unoweb.seguridad.negocio.transfer.UsuarioTO;");
                sw.WriteLine(@"import org.springframework.stereotype.Controller;");
                sw.WriteLine(@"import org.springframework.web.bind.annotation.RequestMapping;");
                sw.WriteLine(@"import org.springframework.web.bind.annotation.RequestMethod;");
                sw.WriteLine(@"import org.springframework.web.bind.annotation.ModelAttribute;");
                sw.WriteLine(@"import org.springframework.validation.BindingResult;");
                sw.WriteLine(@"import com.siesa.unoweb.general.negocio.Util;");

                sw.WriteLine();
                sw.WriteLine(@"@Controller()");
                sw.WriteLine(@"public class Controlador{0} extends ControladorBaseUno {1}", Table.Code, "{");
                sw.WriteLine();
                sw.WriteLine();
                sw.WriteLine(@"    private Short userLanguage;");
                sw.WriteLine();
                sw.WriteLine(@"    public Controlador{0}() {1}", Table.Code, "{");
                sw.WriteLine(@"        super();");
                sw.WriteLine(@"        //setCommandClass({0}.class);", Table.Code);
                sw.WriteLine(@"    }");
                sw.WriteLine();
                sw.WriteLine(@"    @RequestMapping(value = ""/{0}.do"", method = {1}RequestMethod.POST{2})", Table.Code, "{", "}");
                sw.WriteLine(@"    public final ModelAndView handle(HttpServletRequest request, HttpServletResponse response, @ModelAttribute() {0} command, BindingResult errors) throws Exception{1}", Table.Code, "{");
                sw.WriteLine(@"        String accion = null;");
                sw.WriteLine(@"        JSONObject jsonRespuesta = new JSONObject();");
                sw.WriteLine(@"        try {");
                sw.WriteLine(@"            String clase = ""{0}"";", Table.Code);
                sw.WriteLine(@"            int ubicacion = 0;");
                sw.WriteLine(@"            int modulo = Modulos.Modulo_Socupacional;");
                sw.WriteLine(@"            accion = recuperarVariable(request, ""accion"", true);");
                sw.WriteLine(@"            preproceso(request, response, command, errors, accion);");
                sw.WriteLine(@"            UsuarioTO usuario = GeneralComun.obtenerUsuarioSesion(request);");
                sw.WriteLine(@"            if (!esSesionValida(request, usuario, jsonRespuesta))");
                sw.WriteLine(@"               return null;");
                sw.WriteLine(@"            userLanguage = (short) usuario.getIdLenguaje();");
                sw.WriteLine(@"            {0}BusinessBLL business = new {0}BusinessBLL(usuario);",GetBusinessByModule());
                sw.WriteLine(@"            SP_Parametros parametros = new SP_Parametros(modulo);");
                sw.WriteLine(@"            {0} data = ({0})command;", Table.Code);
                sw.WriteLine(@"            data.setUser(usuario);");
                sw.WriteLine(@"            JSONArray jaColumnas = new JSONArray();");
                sw.WriteLine(@"            PermisosMetodosXClaseRtoDTO permisosMetodosXClaseRtoDTO = leerDatosPermisosMetodos(request, accion);");
                sw.WriteLine(@"            ConsultantesSeguridad1_1Delegate consultantesSeguridad1_1Delegate = new ConsultantesSeguridad1_1Delegate();			");
                sw.WriteLine(@"            ConsultantesGeneral1_2Delegate consultantesGeneral1_2Delegate = new ConsultantesGeneral1_2Delegate();");
                sw.WriteLine(@"            ConsultasRtoDTO consultasRtoDTO = new ConsultasRtoDTO();");
                sw.WriteLine(@"            // acciones");
                sw.WriteLine(@"            if (accion.equalsIgnoreCase(Accion_PermisosBarra)) {");
                sw.WriteLine(@"                ArrayList<PermisosMetodosXClaseRtoDTO> alPermisosMetodosXClaseRtoDTO = (ArrayList<PermisosMetodosXClaseRtoDTO>)consultantesSeguridad1_1Delegate.consultarPermisosBarraHerramientas(permisosMetodosXClaseRtoDTO, parametros, usuario);");
                sw.WriteLine(@"                JSONArray jaPermisos = alPermisosMetodosXClaseRtoDTOToJSONArray(jaColumnas, alPermisosMetodosXClaseRtoDTO);");
                sw.WriteLine(@"                adicionarDatosRespuesta(""permisosBarra"", jaPermisos, jsonRespuesta );");
                sw.WriteLine(@"                adicionarDatosRespuesta(Etiqueta_Columnas, jaColumnas, jsonRespuesta);");
                sw.WriteLine(@"            } else if (accion.equalsIgnoreCase(Accion_PermisosCampos)) {");
                sw.WriteLine(@"                ArrayList<PermisosMetodosXClaseRtoDTO> alPermisosMetodosXClaseRtoDTO = (ArrayList<PermisosMetodosXClaseRtoDTO>)consultantesSeguridad1_1Delegate.consultarPermisosCampos(permisosMetodosXClaseRtoDTO, parametros, usuario);");
                sw.WriteLine(@"                JSONArray jaPermisos = alPermisosMetodosXClaseRtoDTOToJSONArray(jaColumnas, alPermisosMetodosXClaseRtoDTO);");
                sw.WriteLine(@"                adicionarDatosRespuesta(""permisosCampos"", jaPermisos, jsonRespuesta);");
                sw.WriteLine(@"                adicionarDatosRespuesta(Etiqueta_Columnas, jaColumnas, jsonRespuesta);");
                sw.WriteLine(@"            } else if (accion.equalsIgnoreCase(Accion_Crear)) {");
                sw.WriteLine(@"                consultantesSeguridad1_1Delegate.validarPermisoMetodo(permisosMetodosXClaseRtoDTO, parametros, usuario);");

                if (Table.Columns.FirstOrDefault(x => GCUtil.PascalCase(x.CodeJava).Contains("UsuarioActualizacion")) != null)
                {
                    sw.WriteLine(@"                data.set{0}(usuario.getUserId());", Table.Columns.FirstOrDefault(x => GCUtil.PascalCase(x.CodeJava).Contains("UsuarioActualizacion")).CodeJava);
                    sw.WriteLine(@"                data.set{0}(usuario.getUserId());", Table.Columns.FirstOrDefault(x => GCUtil.PascalCase(x.CodeJava).Contains("UsuarioCreacion")).CodeJava);
                }

                sw.WriteLine(@"                {0} data2 = business.get{0}BLL().setExecute(data, Actions.Save, Options.Me);", Table.Code);
                sw.WriteLine(@"                if (data2 != null)");
                sw.WriteLine(@"                    data2 = business.get{0}BLL().setExecute(data, Actions.Select, Options.Me);", Table.Code);

                sw.WriteLine(@"                if (existeError(parametros)) {");
                sw.WriteLine(@"                    registraErrores(parametros, jsonRespuesta);");
                sw.WriteLine(@"                } else {");
                sw.WriteLine(@"                    JSONArray jaData = {0}ToJSONArray(jaColumnas, data2, Accion_Consultar, true);", Table.Code);
                sw.WriteLine(@"                    adicionarDatosRespuesta(clase, jaData, jsonRespuesta);");
                sw.WriteLine(@"                    adicionarDatosRespuesta(Etiqueta_Columnas, jaColumnas, jsonRespuesta);");
                sw.WriteLine(@"                    adicionarDatosRespuesta(Etiqueta_Ubicacion, (data2 != null ? data2.getUbication() : 0), jsonRespuesta );");
                sw.WriteLine(@"                }");

                sw.WriteLine(@"            } else if (accion.equalsIgnoreCase(Accion_Modificar)) {");
                sw.WriteLine(@"                consultantesSeguridad1_1Delegate.validarPermisoMetodo(permisosMetodosXClaseRtoDTO, parametros, usuario);");

                sw.WriteLine(@"                {0} data2 = business.get{0}BLL().setExecute(data, Actions.Update, Options.Me);", Table.Code);
                sw.WriteLine(@"                if (data2 != null)");
                sw.WriteLine(@"                    data2 = business.get{0}BLL().setExecute(data, Actions.Select, Options.Me);", Table.Code);
                sw.WriteLine(@"                if (existeError(parametros)) {");
                sw.WriteLine(@"                    registraErrores(parametros, jsonRespuesta);");
                sw.WriteLine(@"                } else {");
                sw.WriteLine(@"                    JSONArray jaData = {0}ToJSONArray(jaColumnas, data2, Accion_Consultar, true);", Table.Code);
                sw.WriteLine(@"                    adicionarDatosRespuesta(clase, jaData, jsonRespuesta);");
                sw.WriteLine(@"                    adicionarDatosRespuesta(Etiqueta_Columnas, jaColumnas, jsonRespuesta);");
                sw.WriteLine(@"                    adicionarDatosRespuesta(Etiqueta_Ubicacion, (data2 != null ? data2.getUbication() : 0), jsonRespuesta);");
                sw.WriteLine(@"                }");

                sw.WriteLine(@"            } else if (accion.equalsIgnoreCase(Accion_Borrar)) {");
                sw.WriteLine(@"                consultantesSeguridad1_1Delegate.validarPermisoMetodo(permisosMetodosXClaseRtoDTO, parametros, usuario);");

                sw.WriteLine(@"                {0} data2 = business.get{0}BLL().setExecute(data, Actions.Delete, Options.Me);", Table.Code);

                sw.WriteLine(@"                if (existeError(parametros)) {");
                sw.WriteLine(@"                    registraErrores(parametros, jsonRespuesta);");
                sw.WriteLine(@"                } else {");
                sw.WriteLine(@"                    JSONArray jaData = {0}ToJSONArray(jaColumnas, data2, Accion_Consultar, true);	", Table.Code);
                sw.WriteLine(@"                    adicionarDatosRespuesta(clase, jaData, jsonRespuesta);");
                sw.WriteLine(@"                    adicionarDatosRespuesta(Etiqueta_Columnas, jaColumnas, jsonRespuesta);");
                sw.WriteLine(@"                    adicionarDatosRespuesta(Etiqueta_Ubicacion, (data2 != null ? data2.getUbication() : 0), jsonRespuesta);");
                sw.WriteLine(@"                }");

                sw.WriteLine(@"            } else if (accion.equalsIgnoreCase(Accion_Paginar)) {");
                sw.WriteLine(@"                int pagina = Integer.parseInt(request.getParameter(""pagina""));");
                sw.WriteLine(@"                int filas = Integer.parseInt(request.getParameter(""filas""));");
                sw.WriteLine(@"                String condicion = request.getParameter(""condicion"");");
                sw.WriteLine(@"                String orden = request.getParameter(""orden"");");
                sw.WriteLine(@"                data.setHasPaging(true);");
                sw.WriteLine(@"                data.setPageSize(filas);");
                sw.WriteLine(@"                data.setCurrentPage(pagina);");
                sw.WriteLine(@"                data.setOrderBy(orden);");
                sw.WriteLine(@"                //Ejemplo de parametros adicionales");
                sw.WriteLine(@"                //data.setAditionalsValue(""cxxxxDescripcion"", request.getParameter(""cxxxxDescripcion""));");
                sw.WriteLine();
                sw.WriteLine(@"                String where = condicion;");
                sw.WriteLine(@"                BeanDTO beanDTO = business.get{0}BLL().setSelectAll(data, where, Options.All);", Table.Code);
                sw.WriteLine(@"                int numeroRegistros = beanDTO.getEntero();");
                sw.WriteLine(@"                List<{0}> datas = (List<{0}>) beanDTO.getLista();", Table.Code);

                sw.WriteLine(@"                JSONArray jaData = {0}ListToJSONArray(jaColumnas, datas, Accion_Paginar);", Table.Code);
                sw.WriteLine(@"                adicionarDatosRespuesta(clase, jaData, jsonRespuesta);");
                sw.WriteLine(@"                adicionarDatosRespuesta(Etiqueta_Columnas, jaColumnas, jsonRespuesta);");
                sw.WriteLine(@"                adicionarDatosRespuesta(Etiqueta_NumRegistros, numeroRegistros, jsonRespuesta);				");

                sw.WriteLine(@"            } else if (accion.equalsIgnoreCase(Accion_Consultar)) {");

                sw.WriteLine(@"                int operacion = Integer.parseInt(request.getParameter(""operacion""));");
                sw.WriteLine(@"                data.setOrderBy(request.getParameter(""orden""));");
                sw.WriteLine(@"                data.setIdColumnOrder(leerEntero(request, ""idColumnOrder""));");
                sw.WriteLine(@"                data.setOperacion(leerEntero(request, ""operacion""));");
                sw.WriteLine(@"                String condicion = request.getParameter(""condicion"");");
                sw.WriteLine(@"                //Ejemplo de columna adicional para lograr ordenar");
                sw.WriteLine(@"                //data.setAditionalsValue(""cxxxxDescripcion"", request.getParameter(""cxxxxDescripcion""));");
                sw.WriteLine();
                sw.WriteLine(@"                Options opts = null;");

                sw.WriteLine(@"                if (operacion == 1)");
                sw.WriteLine(@"                    opts = Options.First;");
                sw.WriteLine(@"                if (operacion == 2)");
                sw.WriteLine(@"                    opts = Options.Next;	");
                sw.WriteLine(@"                if (operacion == 3)");
                sw.WriteLine(@"                    opts = Options.Previous;				");
                sw.WriteLine(@"                if (operacion == 4)				");
                sw.WriteLine(@"                    opts = Options.Last;");
                sw.WriteLine(@"                if (operacion == 0 || operacion == 7 || operacion == 8)				");
                sw.WriteLine(@"                    opts = Options.Me;");

                sw.WriteLine(@"                String where = condicion;");
                sw.WriteLine(@"                {0} data2 =  business.get{0}BLL().setExecute(data, where, Actions.Select, opts);", Table.Code);

                sw.WriteLine(@"                if (existeError(parametros)) {");
                sw.WriteLine(@"                    registraErrores(parametros, jsonRespuesta);");
                sw.WriteLine(@"                } else {");
                sw.WriteLine(@"                    JSONArray jaData = {0}ToJSONArray(jaColumnas, data2, Accion_Consultar, true);	", Table.Code);
                sw.WriteLine(@"                    adicionarDatosRespuesta(clase, jaData, jsonRespuesta);");
                sw.WriteLine(@"                    adicionarDatosRespuesta(Etiqueta_Columnas, jaColumnas, jsonRespuesta);");
                sw.WriteLine(@"                    adicionarDatosRespuesta(Etiqueta_Ubicacion, (data2 != null ? data2.getUbication() : 0), jsonRespuesta);");
                sw.WriteLine(@"                    //System.out.print(ubicacion);");

                sw.WriteLine(@"                }");
                sw.WriteLine(@"            } else if(accion.equalsIgnoreCase(Accion_Imprimir)) {");
                sw.WriteLine(@"                controlarTreegridInicial(request, response);");
                sw.WriteLine(@"                consultasRtoDTO = UtilLocal.leerConsultaRto(request);");
                sw.WriteLine(@"                consultasRtoDTO.setIdLenguaje(Util.leerBigDecimal(usuario.getIdLenguaje()));");
                sw.WriteLine(@"                GenConsultaMaestroDTO genConsultaMaestroDTO = consultantesGeneral1_2Delegate.consultarImpGenConsultaMaestro(consultasRtoDTO, parametros, usuario);");
                sw.WriteLine(@"                controlarTreegridPlano(genConsultaMaestroDTO, request, response, parametros, jsonRespuesta);");
                sw.WriteLine(@"            } else {");
                sw.WriteLine(@"                adicionarDatosRespuesta(Etiqueta_TipoError, 1, jsonRespuesta);");
                sw.WriteLine(@"                adicionarMensajeRespuesta(""general"", ""m12"");");
                sw.WriteLine(@"                adicionarErrorRespuesta(""Accion no soportada."", ""Accion=[""+ accion +""]."", jsonRespuesta);");
                sw.WriteLine(@"            }");
                sw.WriteLine(@"        } catch (UNOException e) {");
                sw.WriteLine(@"            registrarExcepcion(e, jsonRespuesta);");
                sw.WriteLine(@"        } catch (Throwable t) {");
                sw.WriteLine(@"            registrarThrowable(t, jsonRespuesta);");
                sw.WriteLine(@"        } finally {");
                sw.WriteLine(@"            enviarRespuesta(response,request, jsonRespuesta);");
                sw.WriteLine(@"        }");

                sw.WriteLine(@"        return null;");
                sw.WriteLine(@"    }");

                sw.WriteLine();
                sw.WriteLine(@"    private JSONArray {0}ListToJSONArray(JSONArray jaColumnas, List<{1}> {0}List, String accion){2}", Table.Code, Table.Code, "{");
                sw.WriteLine(@"        JSONArray jaLista = new JSONArray();");
                sw.WriteLine(@"        if ({0}List == null)", Table.Code);
                sw.WriteLine(@"            return jaLista;");

                sw.WriteLine(@"        int size = {0}List.size();", Table.Code);
                sw.WriteLine(@"        boolean esPrimero = true;");
                sw.WriteLine(@"        for (int i=0; i<size; i++) {");
                sw.WriteLine(@"            {0} objeto = ({0}){1}List.get(i);", Table.Code, Table.Code);
                sw.WriteLine(@"            jaLista.add({0}ToJSONArray(jaColumnas, objeto, accion, esPrimero));", Table.Code);
                sw.WriteLine(@"            esPrimero = false;");
                sw.WriteLine(@"        }");

                sw.WriteLine(@"        return jaLista;");
                sw.WriteLine(@"    }");

                sw.WriteLine();
                sw.WriteLine(@"    private JSONArray {0}ToJSONArray(JSONArray jaColumnas, {1} data, String accion, boolean esPrimero){2}", Table.Code, Table.Code, "{");
                sw.WriteLine(@"        JSONArray jaFila = new JSONArray();");

                sw.WriteLine(@"        if (data == null)");
                sw.WriteLine(@"            return jaFila;");
                sw.WriteLine(@"        //Ejemplo de valores adicionales		");
                sw.WriteLine(@"        //if (esPrimero) jaColumnas.add(""cxxxxDescripcion"");");
                sw.WriteLine(@"        //jaFila.add(data.getAditionalValue(""cxxxxDescripcion""));");
                sw.WriteLine();

                foreach (var item in Table.Columns)
                {
                    if (!(item.CodeJava.EndsWith("FechaCreacion") || item.CodeJava.EndsWith("UsuarioCreacion") || item.CodeJava.EndsWith("FechaActualizacion") || item.CodeJava.EndsWith("UsuarioActualizacion") || item.CodeJava.EndsWith("Notas")))
                    {
                        if (GCUtil.GetJavaDataType(item) != "Calendar")
                        {
                            sw.WriteLine(@"        if(esPrimero)");
                            sw.WriteLine(@"            jaColumnas.add(""{0}"");", GCUtil.CamelCase(item.CodeJava));
                            sw.WriteLine(@"        jaFila.add(data.get{0}());", item.CodeJava);
                        }

                        if (GCUtil.GetJavaDataType(item) == "Calendar")
                        {
                            sw.WriteLine(@"        if(esPrimero)");
                            sw.WriteLine(@"            jaColumnas.add(""{0}"");", GCUtil.CamelCase(item.CodeJava));
                            sw.WriteLine(@"        jaFila.add( Utiles.formatearFecha( data.get{0}() ));", item.CodeJava);

                        }
                        sw.WriteLine();
                    }

                }

                sw.WriteLine(@"        if (!accion.equalsIgnoreCase(Accion_Paginar)) {");
                sw.WriteLine();

                foreach (var item in Table.Columns)
                {
                    if ((item.CodeJava.EndsWith("FechaCreacion") || item.CodeJava.EndsWith("UsuarioCreacion") || item.CodeJava.EndsWith("FechaActualizacion") || item.CodeJava.EndsWith("UsuarioActualizacion") || item.CodeJava.EndsWith("Notas")))
                    {
                        if (GCUtil.GetJavaDataType(item) != "Calendar")
                        {
                            sw.WriteLine(@"        if(esPrimero)");
                            sw.WriteLine(@"            jaColumnas.add(""{0}"");", GCUtil.CamelCase(item.CodeJava));
                            sw.WriteLine(@"        jaFila.add(data.get{0}());", item.CodeJava);
                        }

                        if (GCUtil.GetJavaDataType(item) == "Calendar")
                        {
                            sw.WriteLine(@"        if(esPrimero)");
                            sw.WriteLine(@"            jaColumnas.add(""{0}"");", GCUtil.CamelCase(item.CodeJava));
                            sw.WriteLine(@"        jaFila.add( Utiles.formatearFecha( data.get{0}() ));", item.CodeJava);

                        }

                        sw.WriteLine();
                    }

                }
                sw.WriteLine();
                sw.WriteLine(@"        }");
                sw.WriteLine(@"        return jaFila;");
                sw.WriteLine(@"    }");

                sw.WriteLine(@"}");

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
        }

        public string GetBusinessByModule()
        {
            if (Table.Prefix.Equals("soc"))
                return "SaludOcupacional";
            if (Table.Prefix.Equals("gpe"))
                return "GestionPersonal";
            if (Table.Prefix.Equals("nom"))
                return "Nomina";
            else
                return "General";
        }

    }
}
