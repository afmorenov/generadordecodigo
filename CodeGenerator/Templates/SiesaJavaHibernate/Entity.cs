﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaJavaHibernate
{
    public class Entity
    {

        public string Name { get; set; } = "Entity";
        public int Order { get; set; } = 1;
        public string NameFile { get; set; } = ".java";
        public string Description { get; set; } = "Generates a Java BLL Class for use with Hibernate.";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public Entity() { } // Para usarse con reflection

        #endregion

        public Entity(CodeGeneratorModel CodeGeneratorModel)
        {
            #region asignacion
            OutputFolder = CodeGeneratorModel.PathGenerate + "/SIESA/" + CodeGeneratorModel.TableGenerated.NameProject + "/hibernate/src/main/java/com/siesa/entities/";
            FullNameFile = CodeGeneratorModel.TableGenerated.Code + this.NameFile;
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate() 
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                sw.WriteLine(@"package com.siesa.entities;");
                sw.WriteLine();
                sw.WriteLine(@"import javax.persistence.*;");
                sw.WriteLine();
                sw.WriteLine(@"import com.siesa.framework.persistence.Options;");
                sw.WriteLine(@"import com.siesa.entities.base.*;");
                sw.WriteLine();
                sw.WriteLine(@"{0}Entity", "@");
                sw.WriteLine(@"{0}Table(name = ""{1}"")", "@", Table.Name);
                sw.WriteLine(@"public class {0}  extends Base{0} {1}", Table.Code, "{");
                sw.WriteLine();
                sw.WriteLine(@"   private static final long serialVersionUID = 1L;");
                sw.WriteLine();
                sw.WriteLine(@"   public {0}() {1}", Table.Code, "{");
                sw.WriteLine(@"      super();");
                sw.WriteLine(@"   }");
                sw.WriteLine();
                sw.WriteLine(@"   public {0}({0} data, Options option)", Table.Code);
                sw.WriteLine(@"   {");
                sw.WriteLine(@"      super(data, option);");
                sw.WriteLine(@"      if (option == Options.Light || option == Options.Me || option == Options.All)");
                sw.WriteLine(@"      {");
                sw.WriteLine(@"         //Options.Light");
                sw.WriteLine(@"      }");
                sw.WriteLine(@"   }");
                sw.WriteLine();
                sw.WriteLine(@"}");

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
            

        }

    }
}
