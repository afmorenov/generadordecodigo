﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaJavaHibernate
{
    public class DAO
    {

        public string Name { get; set; } = "DAO";
        public int Order { get; set; } = 5;
        public string NameFile { get; set; } = "DAO.java";
        public string Description { get; set; } = "Generates the Concrete DAO Hibernate for accessing instances from DB.";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public DAO() { } // Para usarse con reflection

        #endregion

        public DAO(CodeGeneratorModel CodeGeneratorModel)
        {
            #region asignacion
            OutputFolder = CodeGeneratorModel.PathGenerate + "/SIESA/" + CodeGeneratorModel.TableGenerated.NameProject + "/hibernate/src/main/java/com/siesa/dal/";
            FullNameFile = CodeGeneratorModel.TableGenerated.Code + this.NameFile;
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate() 
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                sw.WriteLine(@"package com.siesa.dal;");
                sw.WriteLine();
                sw.WriteLine(@"import com.siesa.entities.*;");
                sw.WriteLine(@"import com.siesa.dal.base.*;");
                sw.WriteLine();
                sw.WriteLine(@"import java.util.HashMap;");
                sw.WriteLine(@"import java.util.List;");
                sw.WriteLine(@"import java.util.Map;");
                sw.WriteLine();
                sw.WriteLine(@"import org.hibernate.Query;");
                sw.WriteLine(@"import org.hibernate.Session;");
                sw.WriteLine();
                sw.WriteLine(@"    public class {0}DAO extends Base{0}DAO ", Table.Code);
                sw.WriteLine(@"    {");
                sw.WriteLine();
                sw.WriteLine(@"       public Map getQueryBaseMap({0} data)", Table.Code);
                sw.WriteLine(@"       {");
                sw.WriteLine(@"          Map queryMap = new HashMap();");
                sw.WriteLine(@"          data.getParametersValue().clear();");
                sw.WriteLine(@"          //Ejemplo de parametros adicionales");
                sw.WriteLine(@"          //data.setParameterValue(""cxxxxDescripcion"", ""b.cxxxxDescripcion"");");
                sw.WriteLine(@"");
                sw.WriteLine(@"          String dmlSelect = ""Select a, 1"";");
                sw.WriteLine(@"          String dmlFrom = ""from {0} as a "" ;", Table.Code);
                sw.WriteLine(@"          String dmlWhere = ""where 1=1 "" ;");
                sw.WriteLine();
                sw.WriteLine(@"          queryMap.put(""select"", dmlSelect);");
                sw.WriteLine(@"          queryMap.put(""from"", dmlFrom);");
                sw.WriteLine(@"          queryMap.put(""where"", dmlWhere);");
                sw.WriteLine(@"          return queryMap;");
                sw.WriteLine(@"       }");
                sw.WriteLine();
                sw.WriteLine(@"       public {0}DAO(Session session) {1}", Table.Code, "{");
                sw.WriteLine(@"          super(session);");
                sw.WriteLine(@"       }");
                sw.WriteLine();
                sw.WriteLine(@"        public {0} setSave({0} data)", Table.Code);
                sw.WriteLine(@"        {");
                sw.WriteLine(@"           return super.setSave(data);");
                sw.WriteLine(@"        }");
                sw.WriteLine();
                sw.WriteLine(@"        public {0} setUpdate({0} data)", Table.Code);
                sw.WriteLine(@"        {");
                sw.WriteLine(@"           return super.setUpdate(data);");
                sw.WriteLine(@"        }");
                sw.WriteLine();
                sw.WriteLine(@"        public {0} setDelete({0} data)", Table.Code);
                sw.WriteLine(@"        {");
                sw.WriteLine(@"           return super.setDelete(data);");
                sw.WriteLine(@"        } ");
                sw.WriteLine();
                sw.WriteLine(@"         public String getQuery({0} data, String where, Boolean byId, Boolean onlyCount, Boolean onlySelect)", Table.Code);
                sw.WriteLine(@"         {");
                sw.WriteLine(@"             String dml = super.getQuery(data, byId, onlyCount, onlySelect);");
                sw.WriteLine(@"             if (!onlySelect) {");
                sw.WriteLine(@"                 if (byId) {");
                sw.WriteLine(@"                     //Incluir solo las columnas que hagan parte de la llave primaria");
                sw.WriteLine(@"                 }");
                sw.WriteLine(@"                 else {");
                sw.WriteLine(@"                     //Incluir los campos que no son parte de la llave primaria");
                sw.WriteLine(@"                 }");
                sw.WriteLine(@"             }");
                sw.WriteLine(@"             if (where != null && !where.isEmpty())");
                sw.WriteLine(@"             {");
                sw.WriteLine(@"                 dml += "" And "" + where + "" "";");
                sw.WriteLine(@"             }");
                sw.WriteLine(@"             String orderBy = getOrderBy(data);");
                sw.WriteLine(@"             if (!onlySelect && !onlyCount)");
                sw.WriteLine(@"                 dml += ""order by "" + (orderBy != null && !orderBy.isEmpty() ? orderBy : ""{0}"");", GCUtil.GetOrderByJavaDAO(Table.Columns));
                sw.WriteLine(@"           return dml;");
                sw.WriteLine(@"        }");
                sw.WriteLine();
                sw.WriteLine(@"        public void setQueryParameters(Query query, {0} data, Boolean byId)", Table.Code);
                sw.WriteLine(@"        {");
                sw.WriteLine(@"           super.setQueryParameters(query, data, byId);");
                sw.WriteLine(@"           //Incluir parametros adicionales");
                sw.WriteLine(@"        }");
                sw.WriteLine();
                sw.WriteLine(@"        public {0} getSelectMe({0} data)", Table.Code);
                sw.WriteLine(@"        {");
                sw.WriteLine(@"           return super.getSelectMe(data);");
                sw.WriteLine(@"        } ");
                sw.WriteLine();
                sw.WriteLine(@"        public List<{0}> getSelectLight({0} data)", Table.Code);
                sw.WriteLine(@"        {");
                sw.WriteLine(@"           return super.getSelectLight(data);");
                sw.WriteLine(@"        } ");
                sw.WriteLine();
                sw.WriteLine(@"        public List<{0}> getSelectAll({0} data)", Table.Code);
                sw.WriteLine(@"        {");
                sw.WriteLine(@"           return super.getSelectAll(data);");
                sw.WriteLine(@"        }");
                sw.WriteLine();
                sw.WriteLine(@"        public {0} getSelectFirst({0} data)", Table.Code);
                sw.WriteLine(@"        {");
                sw.WriteLine(@"           return super.getSelectFirst(data);");
                sw.WriteLine(@"        }");
                sw.WriteLine();
                sw.WriteLine(@"        public {0} getSelectPrevious({0} data)", Table.Code);
                sw.WriteLine(@"        {");
                sw.WriteLine(@"           return super.getSelectPrevious(data);");
                sw.WriteLine(@"        }");
                sw.WriteLine();
                sw.WriteLine(@"        public {0} getSelectNext({0} data)", Table.Code);
                sw.WriteLine(@"        {");
                sw.WriteLine(@"           return super.getSelectNext(data);");
                sw.WriteLine(@"        }");
                sw.WriteLine();
                sw.WriteLine(@"        public {0} getSelectLast({0} data)", Table.Code);
                sw.WriteLine(@"        {");
                sw.WriteLine(@"           return super.getSelectLast(data);");
                sw.WriteLine(@"        }");
                sw.WriteLine();
                sw.WriteLine(@"     }");
                sw.WriteLine();

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
            

        }

    }
}
