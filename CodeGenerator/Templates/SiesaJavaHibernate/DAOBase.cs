﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaJavaHibernate
{
    public class DAOBase
    {

        public string Name { get; set; } = "DAO Base";
        public int Order { get; set; } = 6;
        public string NameFile { get; set; } = "DAO.java";
        public string Description { get; set; } = "Generates the Concrete DAO Hibernate for accessing instances from DB.";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public DAOBase() { } // Para usarse con reflection

        #endregion

        public DAOBase(CodeGeneratorModel CodeGeneratorModel)
        {
            #region asignacion
            OutputFolder = CodeGeneratorModel.PathGenerate + "/SIESA/" + CodeGeneratorModel.TableGenerated.NameProject + "/hibernate/src/main/java/com/siesa/dal/base/";
            FullNameFile = "Base" + CodeGeneratorModel.TableGenerated.Code + this.NameFile;
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate() 
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                List<ColumnModel> ColumnsPrimaryIndex = Table.Columns.Where(x => x.IsPrimaryKey || x.IsIndexUnique).ToList();
                List<ColumnModel> ColumnsPrimaryKeys= Table.Columns.Where(x => x.IsPrimaryKey).ToList();

                sw.WriteLine(@"package com.siesa.dal.base;");
                sw.WriteLine();
                sw.WriteLine();
                sw.WriteLine(@"import com.siesa.entities.{0};", Table.Code);
                sw.WriteLine();
                sw.WriteLine(@"import com.siesa.framework.persistence.IDao;");
                sw.WriteLine(@"import com.siesa.framework.persistence.Options;");
                sw.WriteLine();
                sw.WriteLine(@"import java.util.ArrayList;");
                sw.WriteLine(@"import java.util.HashMap;");
                sw.WriteLine(@"import java.util.LinkedHashMap;");
                sw.WriteLine(@"import java.util.List;");
                sw.WriteLine();
                sw.WriteLine(@"import org.hibernate.Query;");
                sw.WriteLine(@"import org.hibernate.Session;");
                sw.WriteLine();
                if (Table.Columns.Exists(x=> GCUtil.GetJavaDataType(x) == "BigDecimal"))
                {
                    sw.WriteLine(@"import java.math.BigDecimal;");
                    sw.WriteLine();
                }
                sw.WriteLine();
                sw.WriteLine(@"public class Base{0}DAO extends IDao<{0}> {1}", Table.Code, "{");
                sw.WriteLine();
                sw.WriteLine(@"    public Base{0}DAO(Session session) {1}", Table.Code, "{");
                sw.WriteLine(@"        super(session);");
                sw.WriteLine(@"    }");
                sw.WriteLine();
                sw.WriteLine(@"    protected String entityName = ""{0}"";", Table.Code);
                sw.WriteLine(@"    protected String[] keyFields = { ");
                for (int i = 0; i < ColumnsPrimaryKeys.Count; i++)
                {
                    if (i == 0)
                        sw.WriteLine(@"    ""{0}"" ", GCUtil.CamelCase(ColumnsPrimaryIndex[i].CodeJava));
                    else
                        sw.WriteLine(@"    , ""{0}"" ", GCUtil.CamelCase(ColumnsPrimaryIndex[i].CodeJava));
                }
                sw.WriteLine(@"    };");
                sw.WriteLine(@"    protected String getEntityName() {");
                sw.WriteLine(@"        return entityName;");
                sw.WriteLine(@"    }");
                sw.WriteLine(@"    protected String[] getKeyFields() {");
                sw.WriteLine(@"        return keyFields;");
                sw.WriteLine(@"    }");
                sw.WriteLine();
                sw.WriteLine(@"    public String getQuery({0} data, Boolean byId, Boolean onlyCount, Boolean onlySelect)  {1}", Table.Code, "{");
                sw.WriteLine(@"        HashMap queryBaseMap = (HashMap)getQueryBaseMap(data);");
                sw.WriteLine(@"        String dml = queryBaseMap.get(""select"") + "" "" + queryBaseMap.get(""from"") + "" "" + queryBaseMap.get(""where"") + "" "";");
                sw.WriteLine(@"        if (onlyCount)");
                sw.WriteLine(@"            dml = ""Select count(0)  "" + queryBaseMap.get(""from"") + "" "" + queryBaseMap.get(""where"") + "" "";");
                sw.WriteLine();
                sw.WriteLine(@"        if (onlySelect)");
                sw.WriteLine(@"            ;");
                sw.WriteLine(@"        else if (byId) {");
                foreach(var item in ColumnsPrimaryIndex)
                {
                    if (GCUtil.GetJavaDataType(item).Equals("String"))
                    {
                        sw.WriteLine(@"            if (data.get{0}() !=null && !data.get{0}().isEmpty())", GCUtil.PascalCase(item.CodeJava));
                        sw.WriteLine(@"                dml += ""  AND a.{0} = :{0} \n"";", GCUtil.CamelCase(item.CodeJava));
                    }
                    else if (GCUtil.GetJavaDataType(item).Equals("Calendar"))
                    {
                        sw.WriteLine(@"            if (data.get{0}()!=null)", GCUtil.PascalCase(item.CodeJava));
                        sw.WriteLine(@"                dml += ""  AND a.{0} = :{0} \n"";", GCUtil.CamelCase(item.CodeJava));
                    }
                    else if (GCUtil.GetJavaDataType(item).Equals("BigDecimal"))
                    {
                        sw.WriteLine(@"            if (data.get{0}()!=null &&  !data.get{0}().equals(BigDecimal.ZERO))", GCUtil.PascalCase(item.CodeJava));
                        sw.WriteLine(@"                dml += ""  AND a.{0} = :{0} \n"";", GCUtil.CamelCase(item.CodeJava));
                    }
                    else if (GCUtil.GetJavaDataType(item).Equals("Integer"))
                    {
                        sw.WriteLine(@"            if (data.get{0}() != null && data.get{0}() != 0)", GCUtil.PascalCase(item.CodeJava));
                        sw.WriteLine(@"                dml += ""  AND a.{0} = :{0} \n"";", GCUtil.CamelCase(item.CodeJava));
                    }
                    else if (GCUtil.GetJavaDataType(item).Equals("Short"))
                    {
                        sw.WriteLine(@"            if (data.get{0}() != null)", GCUtil.PascalCase(item.CodeJava));
                        sw.WriteLine(@"                dml += ""  AND a.{0} = :{0} \n"";", GCUtil.CamelCase(item.CodeJava));
                    }
                }
                sw.WriteLine(@"        } else {");
                sw.WriteLine(@"            dml += getQueryAditionalParameters(data);");
                foreach (var item in Table.Columns)
                {
                    if (!(item.CodeJava.EndsWith("FechaCreacion") || item.CodeJava.EndsWith("UsuarioCreacion") || item.CodeJava.EndsWith("FechaActualizacion") || item.CodeJava.EndsWith("UsuarioActualizacion")))
                    {
                        if (GCUtil.GetJavaDataType(item).Equals("String"))
                        {
                            sw.WriteLine(@"            if (data.get{0}() !=null && !data.get{0}().isEmpty())", GCUtil.PascalCase(item.CodeJava));
                            sw.WriteLine(@"                dml += ""  AND upper(a.{0}) like :{0} \n"";", GCUtil.CamelCase(item.CodeJava));
                        }
                        else if (GCUtil.GetJavaDataType(item).Equals("Calendar"))
                        {
                            sw.WriteLine(@"            if (data.get{0}()!=null)", GCUtil.PascalCase(item.CodeJava));
                            sw.WriteLine(@"                dml += ""  AND a.{0} = :{0} \n"";", GCUtil.CamelCase(item.CodeJava));
                        }
                        else if (GCUtil.GetJavaDataType(item).Equals("BigDecimal"))
                        {
                            sw.WriteLine(@"            if (data.get{0}()!=null &&  !data.get{0}().equals(BigDecimal.ZERO))", GCUtil.PascalCase(item.CodeJava));
                            sw.WriteLine(@"                dml += ""  AND a.{0} = :{0} \n"";", GCUtil.CamelCase(item.CodeJava));
                        }
                        else if (GCUtil.GetJavaDataType(item).Equals("Integer"))
                        {
                            sw.WriteLine(@"            if (data.get{0}() != null)", GCUtil.PascalCase(item.CodeJava));
                            sw.WriteLine(@"                dml += ""  AND a.{0} = :{0} \n"";", GCUtil.CamelCase(item.CodeJava));
                        }
                        else if (GCUtil.GetJavaDataType(item).Equals("Short"))
                        {
                            sw.WriteLine(@"            if (data.get{0}() != null)", GCUtil.PascalCase(item.CodeJava));
                            sw.WriteLine(@"                dml += ""  AND a.{0} = :{0} \n"";", GCUtil.CamelCase(item.CodeJava));
                        }
                    } 
                }
                sw.WriteLine();
                sw.WriteLine(@"        }");
                sw.WriteLine(@"        return dml;");
                sw.WriteLine(@"    }");
                sw.WriteLine();
                sw.WriteLine(@"    @Override");
                sw.WriteLine(@"    public void setQueryParameters(Query query, {0} data, Boolean byId) {1}", Table.Code, "{");
                sw.WriteLine(@"        if (byId) {");
                foreach (var item in ColumnsPrimaryIndex)
                {
                    if (GCUtil.GetJavaDataType(item).Equals("String"))
                    {
                        sw.WriteLine(@"            if (data.get{0}() !=null && !data.get{0}().isEmpty())", GCUtil.PascalCase(item.CodeJava));
                        sw.WriteLine(@"                query.set{1}(""{0}"", data.get{2}());", GCUtil.CamelCase(item.CodeJava), GCUtil.PascalCase(GCUtil.GetJavaDataType(item)), GCUtil.PascalCase(item.CodeJava));
                    }
                    else if (GCUtil.GetJavaDataType(item).Equals("Calendar"))
                    {
                        sw.WriteLine(@"            if (data.get{0}()!=null)", GCUtil.PascalCase(item.CodeJava));
                        sw.WriteLine(@"                query.set{1}(""{0}"", data.get{2}());", GCUtil.CamelCase(item.CodeJava), GCUtil.PascalCase(GCUtil.GetJavaDataType(item)), GCUtil.PascalCase(item.CodeJava));
                    }
                    else if (GCUtil.GetJavaDataType(item).Equals("BigDecimal"))
                    {
                        sw.WriteLine(@"            if (data.get{0}()!=null &&  !data.get{0}().equals(BigDecimal.ZERO))", GCUtil.PascalCase(item.CodeJava));
                        sw.WriteLine(@"                query.set{1}(""{0}"", data.get{2}());", GCUtil.CamelCase(item.CodeJava), GCUtil.PascalCase(GCUtil.GetJavaDataType(item)), GCUtil.PascalCase(item.CodeJava));
                    }
                    else if (GCUtil.GetJavaDataType(item).Equals("Integer"))
                    {
                        sw.WriteLine(@"            if (data.get{0}() != null && data.get{0}() != 0)", GCUtil.PascalCase(item.CodeJava));
                        sw.WriteLine(@"                query.set{1}(""{0}"", data.get{2}());", GCUtil.CamelCase(item.CodeJava), GCUtil.PascalCase(GCUtil.GetJavaDataType(item)), GCUtil.PascalCase(item.CodeJava));
                    }
                    else if (GCUtil.GetJavaDataType(item).Equals("Short"))
                    {
                        sw.WriteLine(@"            if (data.get{0}() != null)", GCUtil.PascalCase(item.CodeJava));
                        sw.WriteLine(@"                query.set{1}(""{0}"", data.get{2}());", GCUtil.CamelCase(item.CodeJava), GCUtil.PascalCase(GCUtil.GetJavaDataType(item)), GCUtil.PascalCase(item.CodeJava));
                    }
                }
                sw.WriteLine(@"        } else {");
                sw.WriteLine(@"            setQueryAditionalParameters(query, data);");
                foreach (var item in Table.Columns)
                {
                    if (!(item.CodeJava.EndsWith("FechaCreacion") || item.CodeJava.EndsWith("UsuarioCreacion") || item.CodeJava.EndsWith("FechaActualizacion") || item.CodeJava.EndsWith("UsuarioActualizacion")))
                    {
                        if (GCUtil.GetJavaDataType(item).Equals("String"))
                        {
                            sw.WriteLine(@"            if (data.get{0}() !=null && !data.get{0}().isEmpty())", GCUtil.PascalCase(item.CodeJava));
                            sw.WriteLine(@"                query.set{1}(""{0}"", data.get{2}().toUpperCase() + ""%"");", GCUtil.CamelCase(item.CodeJava), GCUtil.PascalCase(GCUtil.GetJavaDataType(item)), GCUtil.PascalCase(item.CodeJava));
                        }
                        else if (GCUtil.GetJavaDataType(item).Equals("Calendar"))
                        {
                            sw.WriteLine(@"            if (data.get{0}()!=null)", GCUtil.PascalCase(item.CodeJava));
                            sw.WriteLine(@"                query.set{1}(""{0}"", data.get{2}());", GCUtil.CamelCase(item.CodeJava), GCUtil.PascalCase(GCUtil.GetJavaDataType(item)), GCUtil.PascalCase(item.CodeJava));
                        }
                        else if (GCUtil.GetJavaDataType(item).Equals("BigDecimal"))
                        {
                            sw.WriteLine(@"            if (data.get{0}()!=null &&  !data.get{0}().equals(BigDecimal.ZERO))", GCUtil.PascalCase(item.CodeJava));
                            sw.WriteLine(@"                query.set{1}(""{0}"", data.get{2}());", GCUtil.CamelCase(item.CodeJava), GCUtil.PascalCase(GCUtil.GetJavaDataType(item)), GCUtil.PascalCase(item.CodeJava));
                        }
                        else if (GCUtil.GetJavaDataType(item).Equals("Integer"))
                        {
                            sw.WriteLine(@"            if (data.get{0}() != null)", GCUtil.PascalCase(item.CodeJava));
                            sw.WriteLine(@"                query.set{1}(""{0}"", data.get{2}());", GCUtil.CamelCase(item.CodeJava), GCUtil.PascalCase(GCUtil.GetJavaDataType(item)), GCUtil.PascalCase(item.CodeJava));
                        }
                        else if (GCUtil.GetJavaDataType(item).Equals("Short"))
                        {
                            sw.WriteLine(@"            if (data.get{0}() != null)", GCUtil.PascalCase(item.CodeJava));
                            sw.WriteLine(@"                query.set{1}(""{0}"", data.get{2}());", GCUtil.CamelCase(item.CodeJava), GCUtil.PascalCase(GCUtil.GetJavaDataType(item)), GCUtil.PascalCase(item.CodeJava));
                        }
                    }
                }
                sw.WriteLine(@"        }");
                sw.WriteLine(@"    }");
                sw.WriteLine();
                sw.WriteLine(@"    /**");
                sw.WriteLine(@"     * Método que busca un registro de un maestro, a través de los campos que formen su llave primaria,");
                sw.WriteLine(@"     * además calcula su ubicación dentro del conjunto de registros que maneje el maestro,");
                sw.WriteLine(@"     * , en la propiedad ubicación establecerá uno de los siguientes valores:");
                sw.WriteLine(@"     * - 0: Esta en medio");
                sw.WriteLine(@"     * - 1: Es registro unico, es primero y último");
                sw.WriteLine(@"     * - 2: Es primero");
                sw.WriteLine(@"     * - 3: Es último");
                sw.WriteLine(@"     * @return El registro buscado o el primer registro si la consulta arroja varios registros");
                sw.WriteLine(@"     * @version: 2");
                sw.WriteLine(@"      */");
                sw.WriteLine(@"    @Override");
                sw.WriteLine(@"    public {0} getSelectMe({0} data, String where) {1}", Table.Code, "{");
                sw.WriteLine(@"        String[] keyFields = getKeyFields();");
                sw.WriteLine(@"        return getSelectMe(data, where, keyFields, Options.Me);");
                sw.WriteLine(@"    }");
                sw.WriteLine();

                sw.WriteLine(@"    public {0} getSelectMe({0} data, String where, String[] keyFields, Options option) {1}", Table.Code, "{");
                sw.WriteLine(@"        String [] arrayFields = getFieldsOrder(data, keyFields);");
                sw.WriteLine(@"        data.setAditionalsValue(""withEqual"", true);");
                sw.WriteLine(@"        String dml = getQueryForSelect(data, where, arrayFields, false, false, option);");
                sw.WriteLine(@"        LinkedHashMap<String, Object> aditionalColumns = data.getParametersValue();");
                sw.WriteLine(@"        Query  query = getSession().createQuery(dml);");
                sw.WriteLine(@"        setQueryKeyParameters(query, data, arrayFields);");
                sw.WriteLine(@"        data.setPageSize(2);");
                sw.WriteLine(@"        data.setCurrentPage(1);");
                sw.WriteLine(@"        query.setFirstResult((data.getPageSize() * data.getCurrentPage()) - data.getPageSize());");
                sw.WriteLine(@"        query.setMaxResults(data.getPageSize());");
                sw.WriteLine(@"        List<Object[]> listResults = query.list();");
                sw.WriteLine(@"        {0} data2 = null;", Table.Code);
                sw.WriteLine(@"        if (listResults != null && listResults.size() > 0) {");
                sw.WriteLine(@"            Object[] arrayResult = listResults.get(0);");
                sw.WriteLine(@"            data2 = new {0}(({0}) arrayResult[0], Options.All);", Table.Code);
                sw.WriteLine(@"            data2.addAditionalFieldsToEntity(aditionalColumns, arrayResult);");
                sw.WriteLine(@"            data.setAditionalsValue(""withEqual"", true);");
                sw.WriteLine(@"            if (getSelectiveCount(data, where, arrayFields, Options.Previous) > 1) {");
                sw.WriteLine(@"                data2.setUbication(listResults.size() > 1 ? 0 : 1);");
                sw.WriteLine(@"            } else {");
                sw.WriteLine(@"                data2.setUbication(listResults.size() > 1 ? 2 : 3);");
                sw.WriteLine(@"            }");
                sw.WriteLine(@"        }");
                sw.WriteLine(@"        return data2;");
                sw.WriteLine(@"    }");
                sw.WriteLine();

                sw.WriteLine(@"    /**");
                sw.WriteLine(@"     * Método que busca un registro de un maestro, busca por id, es decir, busca");
                sw.WriteLine(@"     * por los campos de la llave primaria o por los campos de los índices únicos.");
                sw.WriteLine(@"     * Esta búsqueda la realiza sin calcular ubicación.");
                sw.WriteLine(@"     * @return El registro buscado o el primer registro si la consulta arroja varios registros");
                sw.WriteLine(@"     * @version: 2");
                sw.WriteLine(@"     */");
                sw.WriteLine(@"    @Override");
                sw.WriteLine(@"    public {0} getSelectMeLight({0} data, String where)", Table.Code);
                sw.WriteLine(@"    {");
                sw.WriteLine(@"        String[] keyFields = getKeyFields();");
                sw.WriteLine(@"        return getSelectMeLight(data, where, Options.Me);");
                sw.WriteLine(@"    }");
                sw.WriteLine();

                sw.WriteLine(@"    public {0} getSelectMeLight({0} data, String where, Options option)", Table.Code);
                sw.WriteLine(@"    {");
                sw.WriteLine(@"        data.setAditionalsValue(""withEqual"", true);");
                sw.WriteLine(@"        String dml = getQuery(data, where, true, false);");
                sw.WriteLine(@"        LinkedHashMap<String, Object> aditionalColumns = data.getParametersValue();");
                sw.WriteLine(@"        Query  query = getSession().createQuery(dml);");
                sw.WriteLine(@"        setQueryParameters(query, data, true);");
                sw.WriteLine(@"        data.setPageSize(2);");
                sw.WriteLine(@"        data.setCurrentPage(1);");
                sw.WriteLine(@"        query.setFirstResult((data.getPageSize() * data.getCurrentPage()) - data.getPageSize());");
                sw.WriteLine(@"        query.setMaxResults(data.getPageSize());");
                sw.WriteLine(@"        List<Object[]> listResults = query.list();");
                sw.WriteLine(@"        {0} data2 = null;", Table.Code);
                sw.WriteLine(@"        if (listResults != null && listResults.size() > 0) {");
                sw.WriteLine(@"            Object[] arrayResult = listResults.get(0);");
                sw.WriteLine(@"            data2 = new {0}(({0}) arrayResult[0], Options.All);", Table.Code);
                sw.WriteLine(@"            data2.addAditionalFieldsToEntity(aditionalColumns, arrayResult);");
                sw.WriteLine(@"            data.setAditionalsValue(""withEqual"", true);");
                sw.WriteLine(@"        }");
                sw.WriteLine(@"        return data2;");
                sw.WriteLine(@"    }");
                sw.WriteLine();

                sw.WriteLine(@"    @Override");
                sw.WriteLine(@"    @SuppressWarnings(""unchecked"")");
                sw.WriteLine(@"    public List<{0}> getSelectLight({0} data, String where) {1}", Table.Code, "{");
                sw.WriteLine(@"        String dml = getQuery(data, where, false, false);");
                sw.WriteLine(@"        Query  query = getSession().createQuery(dml);");
                sw.WriteLine(@"        LinkedHashMap<String, Object> aditionalColumns = data.getParametersValue();");
                sw.WriteLine(@"        setQueryParameters(query, data, false);");
                sw.WriteLine(@"        if (data.getHasPaging()) {");
                sw.WriteLine(@"            if (data.getPageSize() == 0)");
                sw.WriteLine(@"                data.setPageSize(1000);");
                sw.WriteLine(@"            if (data.getCurrentPage() == 0)");
                sw.WriteLine(@"                data.setCurrentPage(1);");
                sw.WriteLine(@"            query.setFirstResult((data.getPageSize() * data.getCurrentPage()) - data.getPageSize());");
                sw.WriteLine(@"            query.setMaxResults(data.getPageSize());");
                sw.WriteLine(@"        }");
                sw.WriteLine(@"        List<{0}> datas = null;", Table.Code);
                sw.WriteLine(@"        List<Object[]> list = query.list();");
                sw.WriteLine(@"        if(list != null && list.size() > 0) {");
                sw.WriteLine(@"            datas = new ArrayList<{0}>();", Table.Code);
                sw.WriteLine(@"            for (Object[] arrayResult : list) {");
                sw.WriteLine(@"                {0} dataTemp = ({0})arrayResult[0];", Table.Code);
                sw.WriteLine(@"                dataTemp.addAditionalFieldsToEntity(aditionalColumns, arrayResult);");
                sw.WriteLine(@"                datas.add(dataTemp);");
                sw.WriteLine(@"            }");

                sw.WriteLine(@"            if (datas != null && datas.size() > 0)");
                sw.WriteLine(@"                for ({0} item : datas) {1}", Table.Code, "{");
                sw.WriteLine(@"                    item = new {0}(item, Options.Light);", Table.Code);
                sw.WriteLine(@"                }");
                sw.WriteLine(@"        }");
                sw.WriteLine(@"        return datas;");
                sw.WriteLine(@"    }");
                sw.WriteLine();
                sw.WriteLine(@"    @Override");
                sw.WriteLine(@"    @SuppressWarnings(""unchecked"")");
                sw.WriteLine(@"    public List<{0}> getSelectAll({0} data, String where) {1}", Table.Code, "{");
                sw.WriteLine(@"        String dml = getQuery(data, where, false, false);");
                sw.WriteLine(@"        LinkedHashMap<String, Object> aditionalColumns = data.getParametersValue();");
                for (int i = 0; i < ColumnsPrimaryKeys.Count; i++)
                {
                    sw.WriteLine(@"        dml = dml.replaceAll(""= :{0}"", ""LIKE :{0}"");", GCUtil.CamelCase(ColumnsPrimaryKeys[i].CodeJava));
                }
                ColumnModel column9 = Table.Columns.FirstOrDefault(x => x.Name.ToLower().EndsWith("_id"));
                if (column9 != null)
                {
                    sw.WriteLine(@"        dml = dml.replaceAll(""= :{0}"", ""LIKE :{0}"");", GCUtil.CamelCase(column9.CodeJava));
                }
                sw.WriteLine(@"        Query  query = getSession().createQuery(dml);");
                sw.WriteLine(@"        setQueryParameters(query, data, false);");
                sw.WriteLine(@"        if (data.getHasPaging()) {");
                sw.WriteLine(@"            if (data.getPageSize() == 0)");
                sw.WriteLine(@"                data.setPageSize(1000);");
                sw.WriteLine(@"            if (data.getCurrentPage() == 0)");
                sw.WriteLine(@"                data.setCurrentPage(1);");
                sw.WriteLine(@"            query.setFirstResult((data.getPageSize() * data.getCurrentPage()) - data.getPageSize());");
                sw.WriteLine(@"            query.setMaxResults(data.getPageSize());");
                sw.WriteLine(@"        }");
                sw.WriteLine(@"        List<{0}> datas = null;", Table.Code);
                sw.WriteLine(@"        List<Object[]> list = query.list();");
                sw.WriteLine(@"        if(list != null && list.size() > 0) {");
                sw.WriteLine(@"            datas = new ArrayList<{0}>();", Table.Code);
                sw.WriteLine(@"            for (Object[] arrayResult : list) {");
                sw.WriteLine(@"                {0} dataTemp = ({0})arrayResult[0];", Table.Code);
                sw.WriteLine(@"                dataTemp.addAditionalFieldsToEntity(aditionalColumns, arrayResult);");
                sw.WriteLine(@"                datas.add(dataTemp);");
                sw.WriteLine(@"            }");

                sw.WriteLine(@"            if (datas != null && datas.size() > 0)");
                sw.WriteLine(@"                for ({0} item : datas) {1}", Table.Code, "{");
                sw.WriteLine(@"                    item = new {0}(item, Options.All);", Table.Code);
                sw.WriteLine(@"                }");
                sw.WriteLine(@"        }");
                sw.WriteLine(@"        return datas;");
                sw.WriteLine(@"    }");
                sw.WriteLine();
                sw.WriteLine(@"    @Override");
                sw.WriteLine(@"    public {0} getSelectFirst({0} data, String where) {1}", Table.Code, "{");
                sw.WriteLine(@"        String[] keyFields = getKeyFields();");
                sw.WriteLine(@"        return getSelectFirst(data, where, keyFields);");
                sw.WriteLine(@"    }");
                sw.WriteLine();
                sw.WriteLine(@"    @SuppressWarnings(""unchecked"")");
                sw.WriteLine(@"    public {0} getSelectFirst({0} data, String where, String[] keyFields) {1}", Table.Code, "{");
                sw.WriteLine(@"        String [] arrayFields = getFieldsOrder(data, keyFields);");
                sw.WriteLine(@"        String dml = getQueryForSelect(data, where, arrayFields, false, false, Options.First);");
                sw.WriteLine(@"        LinkedHashMap<String, Object> aditionalColumns = data.getParametersValue();");
                sw.WriteLine(@"        Query  query = getSession().createQuery(dml);");
                sw.WriteLine(@"        data.setPageSize(2);");
                sw.WriteLine(@"        data.setCurrentPage(1);");
                sw.WriteLine(@"        query.setFirstResult((data.getPageSize() * data.getCurrentPage()) - data.getPageSize());");
                sw.WriteLine(@"        query.setMaxResults(data.getPageSize());");
                sw.WriteLine(@"        List<Object[]> listResults = query.list();");
                sw.WriteLine(@"        {0} data2 = null;", Table.Code);
                sw.WriteLine(@"        if (listResults != null && listResults.size() > 0) {");
                sw.WriteLine(@"            Object[] arrayResult = listResults.get(0);");
                sw.WriteLine(@"            data2 = new {0}(({0})arrayResult[0], Options.All);", Table.Code);
                sw.WriteLine(@"            data2.addAditionalFieldsToEntity(aditionalColumns, arrayResult);");
                sw.WriteLine(@"            data2.setUbication(listResults.size() > 1 ? 2 : 3);");
                sw.WriteLine(@"        }");
                sw.WriteLine(@"        return data2;");
                sw.WriteLine(@"    }");
                sw.WriteLine();
                sw.WriteLine(@"    @Override");
                sw.WriteLine(@"    @SuppressWarnings(""unchecked"")");
                sw.WriteLine(@"    public {0} getSelectPrevious({0} data, String where) {1}", Table.Code, "{");
                sw.WriteLine(@"        String[] keyFields = getKeyFields();");
                sw.WriteLine(@"        return getSelectPrevious(data, where, keyFields, Options.Previous);");
                sw.WriteLine(@"    }");
                sw.WriteLine();
                sw.WriteLine(@"    public {0} getSelectPrevious({0} data, String where, String[] keyFields, Options option) {1}", Table.Code, "{");
                sw.WriteLine(@"        String [] arrayFields = getFieldsOrder(data, keyFields);");
                sw.WriteLine(@"        String dml = getQueryForSelect(data, where, arrayFields, false, false, option);");
                sw.WriteLine(@"        LinkedHashMap<String, Object> aditionalColumns = data.getParametersValue();");
                sw.WriteLine(@"        Query  query = getSession().createQuery(dml);");
                sw.WriteLine(@"        setQueryKeyParameters(query, data, arrayFields);");
                sw.WriteLine(@"        data.setPageSize(2);");
                sw.WriteLine(@"        data.setCurrentPage(1);");
                sw.WriteLine(@"        query.setFirstResult((data.getPageSize() * data.getCurrentPage()) - data.getPageSize());");
                sw.WriteLine(@"        query.setMaxResults(data.getPageSize());");
                sw.WriteLine(@"        List<Object[]> listResults = query.list();");
                sw.WriteLine(@"        {0} data2 = null;", Table.Code);
                sw.WriteLine(@"        if(listResults != null && listResults.size() > 0) {");
                sw.WriteLine(@"            Object[] arrayResult = listResults.get(0);");
                sw.WriteLine(@"            data2 = new {0}(({0}) arrayResult[0], Options.All);", Table.Code);
                sw.WriteLine(@"            data2.addAditionalFieldsToEntity(aditionalColumns, arrayResult);");
                sw.WriteLine(@"            if(getSelectiveCount(data, where, keyFields, Options.Next) > 0) {");
                sw.WriteLine(@"                data2.setUbication(listResults.size() > 1 ? 0 : 2);");
                sw.WriteLine(@"            } else {");
                sw.WriteLine(@"                data2.setUbication(listResults.size() > 1 ? 1 : 3);");
                sw.WriteLine(@"            }");
                sw.WriteLine(@"        }");
                sw.WriteLine(@"        return data2;");
                sw.WriteLine(@"    }");
                sw.WriteLine();
                sw.WriteLine(@"    @Override");
                sw.WriteLine(@"    @SuppressWarnings(""unchecked"")");
                sw.WriteLine(@"    public {0} getSelectNext({0} data, String where) {1}", Table.Code, "{");
                sw.WriteLine(@"        String[] keyFields = getKeyFields();");
                sw.WriteLine(@"        return getSelectNext(data, where, keyFields, Options.Next);");
                sw.WriteLine(@"    }");
                sw.WriteLine();
                sw.WriteLine(@"    public {0} getSelectNext({0} data, String where, String[] keyFields, Options option) {1}", Table.Code, "{");
                sw.WriteLine(@"        String [] arrayFields = getFieldsOrder(data, keyFields);");
                sw.WriteLine(@"        String dml = getQueryForSelect(data, where, arrayFields, false, false, option);");
                sw.WriteLine(@"        LinkedHashMap<String, Object> aditionalColumns = data.getParametersValue();");
                sw.WriteLine(@"        Query query = getSession().createQuery(dml);");
                sw.WriteLine(@"        setQueryKeyParameters(query, data, arrayFields);");
                sw.WriteLine(@"        data.setPageSize(2);");
                sw.WriteLine(@"        data.setCurrentPage(1);");
                sw.WriteLine(@"        query.setFirstResult((data.getPageSize() * data.getCurrentPage())- data.getPageSize());");
                sw.WriteLine(@"        query.setMaxResults(data.getPageSize());");
                sw.WriteLine(@"        List<Object[]> listResults = query.list();");
                sw.WriteLine(@"        {0} data2 = null;", Table.Code);
                sw.WriteLine(@"        if (listResults != null && listResults.size() > 0) {");
                sw.WriteLine(@"            Object[] arrayResult = listResults.get(0);");
                sw.WriteLine(@"            data2 = new {0}(({0}) arrayResult[0], Options.All);", Table.Code);
                sw.WriteLine(@"            data2.addAditionalFieldsToEntity(aditionalColumns, arrayResult);");
                sw.WriteLine(@"            if(getSelectiveCount(data, where, keyFields, Options.Previous) > 0) {");
                sw.WriteLine(@"                data2.setUbication(listResults.size() > 1 ? 0 : 1);");
                sw.WriteLine(@"            } else {");
                sw.WriteLine(@"                data2.setUbication(listResults.size() > 1 ? 2 : 3);");
                sw.WriteLine(@"            }");
                sw.WriteLine(@"        }");
                sw.WriteLine(@"        return data2;");
                sw.WriteLine(@"    }");

                sw.WriteLine();
                sw.WriteLine(@"    @Override");
                sw.WriteLine(@"    @SuppressWarnings(""unchecked"")");
                sw.WriteLine(@"    public {0} getSelectLast({0} data, String where) {1}", Table.Code, "{");
                sw.WriteLine(@"        String[] keyFields = getKeyFields();");
                sw.WriteLine(@"        return getSelectLast(data, where, keyFields);");
                sw.WriteLine(@"    }");
                sw.WriteLine();

                sw.WriteLine(@"    public {0} getSelectLast({0} data, String where, String[] keyFields) {1}", Table.Code, "{");
                sw.WriteLine(@"        String [] arrayFields = getFieldsOrder(data, keyFields);");
                sw.WriteLine(@"        String dml = getQueryForSelect(data, where, arrayFields, false, false, Options.Last);");
                sw.WriteLine(@"        LinkedHashMap<String, Object> aditionalColumns = data.getParametersValue();");
                sw.WriteLine(@"        Query query = getSession().createQuery(dml);");
                sw.WriteLine(@"        data.setPageSize(2);");
                sw.WriteLine(@"        data.setCurrentPage(1);");
                sw.WriteLine(@"        query.setFirstResult((data.getPageSize() * data.getCurrentPage())- data.getPageSize());");
                sw.WriteLine(@"        query.setMaxResults(data.getPageSize());");
                sw.WriteLine(@"        List<Object[]> listResults = query.list();");
                sw.WriteLine(@"        {0} data2 = null;", Table.Code);
                sw.WriteLine(@"        if (listResults != null && listResults.size() > 0) {");
                sw.WriteLine(@"            Object[] arrayResult = listResults.get(0);");
                sw.WriteLine(@"            data2 = new {0}(({0})arrayResult[0], Options.All);", Table.Code);
                sw.WriteLine(@"            data2.addAditionalFieldsToEntity(aditionalColumns, arrayResult);");
                sw.WriteLine(@"            data2.setUbication(listResults.size() > 1 ? 1 : 3);");
                sw.WriteLine(@"        }");
                sw.WriteLine(@"        return data2;");
                sw.WriteLine(@"    }");

                sw.WriteLine();
                sw.WriteLine(@"    @Override");
                sw.WriteLine(@"    public {0} setDelete({0} data) {1}", Table.Code, "{");
                sw.WriteLine(@"        String dml = ""delete from {0} where 1 = 1 "";", Table.Code);
                foreach (var column2 in ColumnsPrimaryKeys)
                    sw.WriteLine(@"        dml += "" and {0}= :{0} "" ;", GCUtil.CamelCase(column2.CodeJava));
                sw.WriteLine(@"        Query update = getSession().createQuery(dml);");
                foreach (var column2 in ColumnsPrimaryKeys)
                   sw.WriteLine(@"        update.set{0}(""{1}"", data.get{2}()); ", GCUtil.GetJavaDataType(column2), GCUtil.CamelCase(column2.CodeJava), GCUtil.PascalCase(column2.CodeJava));
                sw.WriteLine(@"        update.executeUpdate();");
                sw.WriteLine(@"        {0} oldData = new {0}();", Table.Code);
                foreach (var column2 in ColumnsPrimaryKeys)
                    sw.WriteLine(@"        oldData.set{0}(data.get{0}());", column2.CodeJava);
                sw.WriteLine(@"        oldData.setUser(data.getUser());");
                sw.WriteLine(@"        data = getSelectNext(oldData);");
                sw.WriteLine(@"        if (data == null)");
                sw.WriteLine(@"            data = getSelectPrevious(oldData);");
                sw.WriteLine(@"        return data;");

                sw.WriteLine(@"    } ");
                sw.WriteLine();
                sw.WriteLine(@"}");
                sw.WriteLine();

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
            

        }

    }
}
