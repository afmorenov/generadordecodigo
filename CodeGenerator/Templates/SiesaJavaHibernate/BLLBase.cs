﻿
using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaJavaHibernate
{
    public class BLLBase
    {

        public string Name { get; set; } = "BLL Base";
        public int Order { get; set; } = 4;
        public string NameFile { get; set; } = "BLL.java";
        public string Description { get; set; } = "Generates a Java BLL Class for use with Hibernate.";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public BLLBase() { } // Para usarse con reflection

        #endregion

        public BLLBase(CodeGeneratorModel CodeGeneratorModel)
        {
            #region asignacion
            OutputFolder = CodeGeneratorModel.PathGenerate + "/SIESA/"+ CodeGeneratorModel.TableGenerated.NameProject+"/hibernate/src/main/java/com/siesa/bll/base/";
            FullNameFile = "Base" + CodeGeneratorModel.TableGenerated.Code + this.NameFile;
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate() 
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                List<ColumnModel> ColumnsPrimaryKeys = Table.Columns.Where(x => x.IsPrimaryKey).ToList();

                sw.WriteLine(@"package com.siesa.bll.base;");
                sw.WriteLine();
                sw.WriteLine(@"import com.siesa.dal.*;");
                sw.WriteLine(@"import com.siesa.entities.*;");
                sw.WriteLine();
                sw.WriteLine(@"import com.siesa.framework.application.UNOException;");
                sw.WriteLine(@"import com.siesa.framework.persistence.IBLL;");
                sw.WriteLine(@"import com.siesa.framework.persistence.Resources;");
                sw.WriteLine();
                sw.WriteLine(@"import org.hibernate.Session;");
                sw.WriteLine();
                sw.WriteLine(@"   public class Base{0}BLL extends IBLL<{0}>", Table.Code);
                sw.WriteLine(@"   {");
                sw.WriteLine();
                sw.WriteLine(@"      public Base{0}BLL(String sessionName) throws UNOException", Table.Code);
                sw.WriteLine(@"      {");
                sw.WriteLine(@"         super(sessionName);");
                sw.WriteLine(@"         dalNH = new {0}DAO(this.getSession());", Table.Code);
                sw.WriteLine(@"      }");
                sw.WriteLine();
                sw.WriteLine(@"      public Base{0}BLL(Session session)", Table.Code);
                sw.WriteLine(@"      {");
                sw.WriteLine(@"         super(session);");
                sw.WriteLine(@"         dalNH = new {0}DAO(session);", Table.Code);
                sw.WriteLine(@"      }");
                sw.WriteLine();
                sw.WriteLine(@"      public void setCommonRules({0} data)", Table.Code);
                sw.WriteLine(@"      {");
                foreach (var column in Table.Columns)
                {
                    if (!(column.CodeJava.EndsWith("FechaCreacion") || column.CodeJava.EndsWith("UsuarioCreacion") || column.CodeJava.EndsWith("FechaActualizacion") || column.CodeJava.EndsWith("UsuarioActualizacion") || column.IsPrimaryKey))
                    {
                        if (GCUtil.GetJavaDataType(column).Equals("String"))
                        {
                            if (column.IsRequired)
                            {
                                sw.WriteLine(@"      if (data.get{0}() == null || data.get{0}().isEmpty())", column.CodeJava);
                                sw.WriteLine(@"         addExceptionMessage(Resources.getResource(( data == null || data.getUser() == null) ? defaultLanguage : data.getUser().getLanguage(), ""BLL.TABLE.COLUMNS.ISREQUIRED"", ""{0}""));", GCUtil.ColumnCodeNoNumber(column.Name));
                            }
                            sw.WriteLine(@"      if (data.get{0}() != null && data.get{0}().length() > {1})", column.CodeJava, column.Length);
                            sw.WriteLine(@"         addExceptionMessage(Resources.getResource(( data == null || data.getUser() == null) ? defaultLanguage : data.getUser().getLanguage(), ""BLL.TABLE.COLUMNS.MAXLENGTH"", ""{0}"", {1}));", GCUtil.ColumnCodeNoNumber(column.Name), column.Length);
                        }
                        else if (GCUtil.GetJavaDataType(column).Equals("Calendar"))
                        {
                            if (column.IsRequired)
                            {
                                sw.WriteLine(@"      if (data.get{0}() == null)", column.CodeJava);
                                sw.WriteLine(@"         addExceptionMessage(Resources.getResource(( data == null || data.getUser() == null) ? defaultLanguage : data.getUser().getLanguage(), ""BLL.TABLE.COLUMNS.ISREQUIRED"", ""{0}""));", GCUtil.ColumnCodeNoNumber(column.Name));
                            }
                        }
                        else if (GCUtil.GetJavaDataType(column).Equals("BigDecimal") || GCUtil.GetJavaDataType(column).Equals("Integer") || GCUtil.GetJavaDataType(column).Equals("Short"))
                        {
                            if (column.IsRequired)
                            {
                                sw.WriteLine(@"      if (data.get{0}() == null)", column.CodeJava);
                                sw.WriteLine(@"         addExceptionMessage(Resources.getResource(( data == null || data.getUser() == null) ? defaultLanguage : data.getUser().getLanguage(), ""BLL.TABLE.COLUMNS.ISREQUIRED"", ""{0}""));", GCUtil.ColumnCodeNoNumber(column.Name));
                            }
                        }
                    }  
                }
                sw.WriteLine(@"      }");
                sw.WriteLine();
                sw.WriteLine(@"      @Override");
                sw.WriteLine(@"      public void setSaveRules({0} data)", Table.Code);
                sw.WriteLine(@"      {");
                sw.WriteLine(@"         if (data == null)");
                sw.WriteLine(@"            addExceptionMessage(Resources.getResource(( data == null || data.getUser() == null) ? defaultLanguage : data.getUser().getLanguage(), ""BLL.TABLE.VALUE.NULL"", ""{0}"",""{1}""));", Table.Code, Table.Name);
                foreach (var column in Table.Columns)
                {
                    if (column.CodeJava.EndsWith("FechaActualizacion") || column.CodeJava.EndsWith("FechaCreacion"))
                    {
                        sw.WriteLine(@"         data.set{0}(getNow());", column.CodeJava);
                    }
                    if (column.CodeJava.EndsWith("UsuarioActualizacion") || column.CodeJava.EndsWith("UsuarioCreacion"))
                    {
                        sw.WriteLine(@"         data.set{0}(data.getUser().getUserId());", column.CodeJava);
                    }

                }
                foreach (var column in ColumnsPrimaryKeys)
                {
                    if (GCUtil.GetJavaDataType(column).Equals("String"))
                    {
                        sw.WriteLine(@"         if (data.get{0}() == null || data.get{0}().isEmpty())", column.CodeJava);
                    }
                    else if (GCUtil.GetJavaDataType(column).Equals("Calendar"))
                    {
                        sw.WriteLine(@"         if (data.get{0}() == null)", column.CodeJava);
                    }
                    else if (GCUtil.GetJavaDataType(column).Equals("BigDecimal") || GCUtil.GetJavaDataType(column).Equals("Integer") || GCUtil.GetJavaDataType(column).Equals("Short"))
                    {
                        sw.WriteLine(@"         if (data.get{0}() !=null && data.get{0}() != 0)", column.CodeJava);
                    }
                    sw.WriteLine(@"            addExceptionMessage(Resources.getResource(( data == null || data.getUser() == null) ? defaultLanguage : data.getUser().getLanguage(), ""BLL.TABLE.ROWID.NULL"", ""{0}""));", Table.Code);
                }
                sw.WriteLine(@"         {0} dataExist = dalNH.getSelectMeLight(data);", Table.Code);
                sw.WriteLine(@"         if (dataExist != null)");
                sw.WriteLine(@"            addExceptionMessage(Resources.getResource(( data == null || data.getUser() == null) ? defaultLanguage : data.getUser().getLanguage(), ""BLL.TABLE.VALUE.EXIST""));");
                sw.WriteLine(@"         setCommonRules(data);");
                sw.WriteLine(@"      }");
                sw.WriteLine();
                sw.WriteLine(@"    @Override");
                sw.WriteLine(@"    public void setUpdateRules({0} data)", Table.Code);
                sw.WriteLine(@"    {");
                sw.WriteLine(@"        if (data == null)");
                sw.WriteLine(@"            addExceptionMessage(Resources.getResource(( data == null || data.getUser() == null) ? defaultLanguage : data.getUser().getLanguage(), ""BLL.TABLE.VALUE.NULL"", ""{0}"",""{1}""));", Table.Code, Table.Name);
                foreach (var column in ColumnsPrimaryKeys)
                {
                    if (GCUtil.GetJavaDataType(column).Equals("String"))
                    {
                        sw.WriteLine(@"        if (data.get{0}() == null || data.get{0}().isEmpty())", column.CodeJava);
                    }
                    else if (GCUtil.GetJavaDataType(column).Equals("Calendar"))
                    {
                        sw.WriteLine(@"        if (data.get{0}() == null)", column.CodeJava);
                    }
                    else if (GCUtil.GetJavaDataType(column).Equals("BigDecimal") || GCUtil.GetJavaDataType(column).Equals("Integer") || GCUtil.GetJavaDataType(column).Equals("Short"))
                    {
                        sw.WriteLine(@"         if (data.get{0}() == null || data.get{0}() == 0)", column.CodeJava);
                    }
                    sw.WriteLine(@"            addExceptionMessage(Resources.getResource(( data == null || data.getUser() == null) ? defaultLanguage : data.getUser().getLanguage(), ""BLL.TABLE.ID.NOTNULL"", ""{0}""));", Table.Code);
                }
                sw.WriteLine(@"        // Inicio verificar regla llave unica");
                sw.WriteLine(@"        String where = """";");
                foreach (var colSchema in ColumnsPrimaryKeys)
                {
                    sw.WriteLine(@"        {0} {1} = data.get{2}();", GCUtil.GetJavaDataType(colSchema), GCUtil.CamelCase(colSchema.CodeJava), colSchema.CodeJava);
                    sw.WriteLine(@"        data.set{0}(null);", colSchema.CodeJava);
                    if (GCUtil.GetJavaDataType(colSchema) == "String" || GCUtil.GetJavaDataType(colSchema) == "Calendar")
                        sw.WriteLine(@"        where += (!where.isEmpty() ? "" And"" : """") + "" a.{0} != '""+ {0} ""'"";", GCUtil.CamelCase(colSchema.CodeJava));
                    else
                        sw.WriteLine(@"        where += (!where.isEmpty() ? "" And"" : """") + "" a.{0} != ""+ {0};", GCUtil.CamelCase(colSchema.CodeJava));
                }
                sw.WriteLine(@"        {0} dataExist = dalNH.getSelectMeLight(data, where);", Table.Code);
                sw.WriteLine(@"        if (dataExist != null)");
                sw.WriteLine(@"            addExceptionMessage(Resources.getResource(( data == null || data.getUser() == null) ? defaultLanguage : data.getUser().getLanguage(), ""BLL.TABLE.VALUE.EXIST""));");
                foreach (var colSchema in ColumnsPrimaryKeys)
                {
                    sw.WriteLine(@"        data.set{0}({1});", colSchema.CodeJava, GCUtil.CamelCase(colSchema.CodeJava));
                }
                sw.WriteLine(@"        // Fin verificar regla llave unica");
                foreach (var column in Table.Columns)
                {
                    if (column.CodeJava.EndsWith("UsuarioActualizacion"))
                    {
                        sw.WriteLine(@"        data.set{0}(data.getUser().getUserId());", column.CodeJava);
                    }
                    if (column.CodeJava.EndsWith("FechaActualizacion"))
                    {
                        sw.WriteLine(@"         data.set{0}(getNow());", column.CodeJava);
                    }
                }
                sw.WriteLine(@"        setCommonRules(data);");
                sw.WriteLine(@"    }");
                sw.WriteLine();
                sw.WriteLine(@"      @Override");
                sw.WriteLine(@"      public void setDeleteRules({0} data)", Table.Code);
                sw.WriteLine(@"      {");
                sw.WriteLine(@"         if (data == null)");
                sw.WriteLine(@"            addExceptionMessage(Resources.getResource(( data == null || data.getUser() == null) ? defaultLanguage : data.getUser().getLanguage(), ""BLL.TABLE.VALUE.NULL"", ""{0}"",""{1}""));", Table.Code, Table.Name);
                foreach (var column in ColumnsPrimaryKeys)
                {
                    if (GCUtil.GetJavaDataType(column).Equals("String"))
                    {
                        sw.WriteLine(@"         if (data.get{0}() == null || data.get{0}().isEmpty())", column.CodeJava);
                    }
                    else if (GCUtil.GetJavaDataType(column).Equals("Calendar"))
                    {
                        sw.WriteLine(@"        if (data.get{0}() == null)", column.CodeJava);
                    }
                    else if (GCUtil.GetJavaDataType(column).Equals("BigDecimal") || GCUtil.GetJavaDataType(column).Equals("Integer") || GCUtil.GetJavaDataType(column).Equals("Short"))
                    {
                        sw.WriteLine(@"         if (data.get{0}() == null || data.get{0}() == 0)", column.CodeJava);
                    }
                    sw.WriteLine(@"            addExceptionMessage(Resources.getResource(( data == null || data.getUser() == null) ? defaultLanguage : data.getUser().getLanguage(), ""BLL.TABLE.ROWID.NULL"", ""{0}""));", Table.Code);
                }
                foreach (var outRef in Table.OutReferences.Select(x=> new { x.ParentTableCode , x.OutReferencesName, x.ParentTableName }).Distinct())
                {
                    sw.WriteLine(@"         //Valida que no haya registros en la tabla {0} por medio de la {1}", outRef.ParentTableName, outRef.OutReferencesName);
                    sw.WriteLine(@"         {0}DAO dao{0}DAO = new {0}DAO(this.getSession());", outRef.ParentTableCode);
                    sw.WriteLine(@"         {0} data{0} = new {0}();", outRef.ParentTableCode);
                    foreach (var fkColumns in Table.OutReferences.Where(x=>x.OutReferencesName == outRef.OutReferencesName))
                    {
                        sw.WriteLine(@"         data{0}.set{1}(data.get{2}());", outRef.ParentTableCode, fkColumns.ParentColumnCode, fkColumns.ColumnCode);
                    }
                    sw.WriteLine(@"         data{0}.setUser(data.getUser());", outRef.ParentTableCode);
                    sw.WriteLine(@"         int i{0} = (dao{0}DAO.getSelectLight(data{0}) == null) ? 0 : dao{0}DAO.getSelectLight(data{0}).size();", outRef.ParentTableCode);
                    sw.WriteLine(@"         if (i{0} != 0)", outRef.ParentTableCode);
                    sw.WriteLine(@"            addExceptionMessage(Resources.getResource((data == null || data.getUser() == null) ? defaultLanguage : data.getUser().getLanguage(), ""BLL.TABLE.REFERENCE.BY"", ""{0}""));", outRef.ParentTableName);
                    sw.WriteLine();
                }
                sw.WriteLine(@"      }");
                sw.WriteLine();
                sw.WriteLine(@"      @Override");
                sw.WriteLine(@"      public void setSelectMeRules({0} data)", Table.Code);
                sw.WriteLine(@"      {");
                sw.WriteLine(@"         if (data == null)");
                sw.WriteLine(@"            addExceptionMessage(Resources.getResource(( data == null || data.getUser() == null) ? defaultLanguage : data.getUser().getLanguage(), ""BLL.TABLE.VALUE.NULL"", ""{0}"",""{1}""));", Table.Code, Table.Name);
                sw.WriteLine(@"      }");
                sw.WriteLine();
                sw.WriteLine(@"   }");
                sw.WriteLine();


                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
            

        }

    }
}
