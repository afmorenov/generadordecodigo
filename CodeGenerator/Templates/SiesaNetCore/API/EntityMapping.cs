﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaNetCore
{
    public class EntityMapping
    {

        public string Name { get; set; } = "API - Mapping";
        public int Order { get; set; } = 2;
        public string Description { get; set; } = "Generates a Base Class Mappings for .NET";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private string Project { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public EntityMapping() { } // Para usarse con reflection

        #endregion

        public EntityMapping(CodeGeneratorModel CodeGeneratorModel)
        {
            OutputFolder = CodeGeneratorModel.PathGenerate + "/API/" + CodeGeneratorModel.TableGenerated.Comment + "/Mappings/";
            FullNameFile = CodeGeneratorModel.TableGenerated.Code + "Map.cs";

            #region asignacion
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            Project = CodeGeneratorModel.Domain;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate() 
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                if (Table.Columns.Exists(x=>x.IsPrimaryKey))
                {
                    List<ColumnModel> pks = Table.Columns.Where(x => x.IsPrimaryKey).ToList();
                    List<ColumnModel> normalColumns = Table.Columns.Where(x => !x.IsPrimaryKey && !x.IsFKIn).ToList();
                    List<ColumnModel> inReferences = Table.Columns.Where(x => x.IsFKIn && !x.IsPrimaryKey).ToList();
                    sw.WriteLine(@"using Microsoft.EntityFrameworkCore;");
                    sw.WriteLine(@"using Microsoft.EntityFrameworkCore.Metadata.Builders;");
                    sw.WriteLine(@"using {0}.Infraestructura.Entidades;", Project);
                    sw.WriteLine();
                    sw.WriteLine(@"namespace {0}.Infraestructura.Mappings", Project);
                    sw.WriteLine(@"{");
                    sw.WriteLine(@"   public class {0}Map : IEntityTypeConfiguration<{0}>", Table.Code);
                    sw.WriteLine(@"   {");

                    sw.WriteLine(@"      public void Configure(EntityTypeBuilder<{0}> builder)", Table.Code);
                    sw.WriteLine(@"      {");
                    sw.WriteLine(@"         builder.ToTable(""{0}"");", Table.Name.ToUpper());
                    sw.WriteLine();
                    sw.WriteLine(@"         #region Columnas llave primaria)");
                    sw.WriteLine();
                    if (pks != null && pks.Count == 1)
                    {
                        ColumnModel column = pks[0];
                        sw.WriteLine(@"         builder.HasKey(t => t.{0});", firstIsNumber(column.Code));
                    }
                    else
                    {
                        sw.WriteLine(@"         builder.HasKey(t => new {1} {0} {2} );", GetPkStringCols(pks), "{", "}");
                    }
                    if (pks != null && pks.Count > 0)
                    {
                        for (int i = 0; i < pks.Count; i++)
                        {
                            ColumnModel column = pks[i];
                            sw.WriteLine(@"         builder.Property(p => p.{1}).HasColumnName(""{0}"").IsRequired(){2};", column.Name.ToUpper(), firstIsNumber(column.Code), (pks.Count == 1 && column.IsIdentity) ? ".ValueGeneratedOnAdd()" : "");
                        }
                    }
                    sw.WriteLine();
                    sw.WriteLine(@"         #endregion");
                    sw.WriteLine();
                    sw.WriteLine(@"         #region Columnas normales)");
                    sw.WriteLine();
                    if (normalColumns != null && normalColumns.Count > 0)
                        for (int i = 0; i < normalColumns.Count; i++)
                        {
                            ColumnModel column = normalColumns[i];
                            if (GCUtil.GetNetDataType(column)  == "String")
                            {
                                sw.WriteLine(@"         builder.Property(p => p.{0}).HasColumnName(""{3}""){1}HasMaxLength({2});", firstIsNumber(column.Code), column.IsRequired ? ".IsRequired()." : ".IsRequired(false).", column.Length == 0 || column.Length == -1 ? int.MaxValue : column.Length , column.Name.ToUpper());
                            }
                            else if (GCUtil.GetNetDataType(column) == "DateTime")
                            {
                                sw.WriteLine(@"         builder.Property(p => p.{0}).HasColumnName(""{2}""){1};", firstIsNumber(column.Code), column.IsRequired ? ".IsRequired()" : ".IsRequired(false)", column.Name.ToUpper());
                            }
                            else if (GCUtil.GetNetDataType(column) == "Decimal")
                            {
                                sw.WriteLine(@"         builder.Property(p => p.{0}).HasColumnName(""{2}""){1};", firstIsNumber(column.Code), column.IsRequired ? ".IsRequired()" : ".IsRequired(false)", column.Name.ToUpper());
                            }
                            else if (GCUtil.GetNetDataType(column) == "Byte[]")
                            {
                                sw.WriteLine(@"         builder.Property(p => p.{0}).HasColumnName(""{2}""){1}/*.HasColumnType(""BLOB"").HasMaxLength(65536)*/;", firstIsNumber(column.Code), column.IsRequired ? ".IsRequired()" : ".IsRequired(false)", column.Name.ToUpper());
                            }
                            else
                                sw.WriteLine(@"         builder.Property(p => p.{0}).HasColumnName(""{2}""){1};", firstIsNumber(column.Code), column.IsRequired ? ".IsRequired()" : ".IsRequired(false)", column.Name.ToUpper());

                        }
                    sw.WriteLine();
                    sw.WriteLine(@"         #endregion");
                    sw.WriteLine();
                    sw.WriteLine(@"         #region Columnas referenciales)");
                    sw.WriteLine();
                    foreach (var column in inReferences)
                    {
                        if (GCUtil.GetNetDataType(column) == "String")
                        {
                            sw.WriteLine(@"         builder.Property(p => p.{0}).HasColumnName(""{3}""){1}HasMaxLength({2});", firstIsNumber(column.Code), column.IsRequired ? ".IsRequired()." : ".IsRequired(false).", column.Length == 0 || column.Length == -1 ? int.MaxValue : column.Length, column.Name.ToUpper());
                        }
                        else if (GCUtil.GetNetDataType(column) == "DateTime")
                        {
                            sw.WriteLine(@"         builder.Property(p => p.{0}).HasColumnName(""{2}""){1};", firstIsNumber(column.Code), column.IsRequired ? ".IsRequired()" : ".IsRequired(false)", column.Name.ToUpper());
                        }
                        else if (GCUtil.GetNetDataType(column) == "Decimal")
                        {
                            sw.WriteLine(@"         builder.Property(p => p.{0}).HasColumnName(""{2}""){1};", firstIsNumber(column.Code), column.IsRequired ? ".IsRequired()" : ".IsRequired(false)", column.Name.ToUpper());
                        }
                        else
                            sw.WriteLine(@"         builder.Property(p => p.{0}).HasColumnName(""{2}""){1};", firstIsNumber(column.Code), column.IsRequired ? ".IsRequired()" : ".IsRequired(false)", column.Name.ToUpper());
                    }
                    sw.WriteLine();
                    sw.WriteLine(@"         #endregion");
                    sw.WriteLine(@"      }");
                    sw.WriteLine(@"   }");
                    sw.WriteLine(@"}");
                }

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
            

        }

        private string GetPkStringCols(List<ColumnModel> pks)
        {
            string cols = "";
            for (int i = 0; i < pks.Count; i++)
            {
                if (i == 0)
                    cols += "t." + pks[i].Code;
                else
                    cols += " ,t." + pks[i].Code;
            }
            return cols;
        }


        private string firstIsNumber(string codeName)
        {
            int i = 0;
            bool firstNumber = int.TryParse(codeName[0].ToString(), out i);
            if (firstNumber)
                codeName += "F" + codeName;
            return codeName;
        }

    }
}
