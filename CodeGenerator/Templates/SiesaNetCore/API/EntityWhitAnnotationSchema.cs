﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace CodeGenerator.Templates.SiesaNetCore
{
    public class EntityWhitAnnotationSchema
    {

        public string Name { get; set; } = "API - Entity";
        public int Order { get; set; } = 1;
        public string Description { get; set; } = "Generates a Base Class Entity for .NET";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private string Project { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public EntityWhitAnnotationSchema() { } // Para usarse con reflection

        #endregion

        public EntityWhitAnnotationSchema(CodeGeneratorModel CodeGeneratorModel)
        {
            //SetTableModule(CodeGeneratorModel.Tables);
            OutputFolder = CodeGeneratorModel.PathGenerate + "/API/" + CodeGeneratorModel.TableGenerated.Comment + "/Entities/";
            FullNameFile = CodeGeneratorModel.TableGenerated.Code + ".cs";

            #region asignacion
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            Project = CodeGeneratorModel.Domain;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate()
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                bool defaultNotas = false;

                if (Table.Columns.Exists(x => x.IsPrimaryKey))
                {
                    List<ColumnModel> pks = Table.Columns.Where(x => x.IsPrimaryKey).ToList();
                    List<ColumnModel> normalColumns = Table.Columns.Where(x => !x.IsPrimaryKey && !x.IsFKIn).ToList();
                    
                    List<ColumnModel> inReferences = Table.Columns.Where(x => x.IsFKIn && !x.IsPrimaryKey).ToList();

                    var unique = Table.Indexes.Where(x => x.IsUnique && !x.IsPrimaryKey).GroupBy(x => x.Name).ToList();
                    var RefIn = Table.InReferences.GroupBy(x=>x.InReferencesName).ToList();
                    var RefOut = Table.OutReferences.GroupBy(x => x.OutReferencesName).ToList();

                    for (int i = 0; i < RefIn.Count(); i++)
                    {
                        if (!RefIn.ElementAt(i).All(x => x.IsRequired))
                        {
                            RefIn.Remove(RefIn.ElementAt(i));
                            i -= 1;
                        }

                    }            

                    sw.WriteLine(@"using Hefestos.Backend.Core.Data;");
                    sw.WriteLine(@"using System;");
                    sw.WriteLine(@"using System.ComponentModel.DataAnnotations;");
                    sw.WriteLine(@"using System.ComponentModel.DataAnnotations.Schema;");
                    sw.WriteLine(@"using System.Linq.Expressions;");
                    sw.WriteLine(@"using System.Collections.Generic;");
                    sw.WriteLine(@"using Serialize.Linq.Extensions;");
                    //if (Table.Columns.FirstOrDefault(x => x.IsRequired == true) != null)
                    //    sw.WriteLine(@"using System.ComponentModel.DataAnnotations;");
                    sw.WriteLine();
                    sw.WriteLine(@"namespace {0}.Infraestructura.Entidades", Project);
                    sw.WriteLine(@"{");
                    sw.WriteLine(@"    /// <summary>");
                    sw.WriteLine(@"    /// {0} object for mapped table {1}.", Table.Code, Table.Name);
                    sw.WriteLine(@"    /// </summary>");
                    sw.WriteLine(@"    [RecursoDisplayName(""{0}"")]", Table.Name);
                    sw.WriteLine(@"    public partial class {0} : EntidadInfraestructura", Table.Code);
                    sw.WriteLine(@"    {");
                    sw.WriteLine();
                    sw.WriteLine(@"        #region Llave primaria");
                    sw.WriteLine();
                    if (pks != null && pks.Count > 0)
                        for (int i = 0; i < pks.Count; i++)
                        {
                            ColumnModel column = pks[i];
                            sw.WriteLine(@"        [Key]");
                            sw.WriteLine(@"        [RecursoDisplayName(""{0}"")]", Table.Name + "." + column.Name);
                            if (column.IsRequired)
                            {
                                //WriteLine(@"       [Required(AllowEmptyStrings = true, ErrorMessage = ""El campo {0} es requerido"")]", column.Name);
                                sw.WriteLine(@"        [RecursoRequired(""{0}"")]", Table.Name + "." + column.Name);
                                //if (GCUtil.GetNetDataType(column) == "Int32")
                                //    sw.WriteLine(@"       [RecursoIntRequired(""{0}"")]", Table.Name + "." + column.Name);
                            }

                            if (GCUtil.GetNetDataType(column) == "DateTime")
                                sw.WriteLine(@"        [DisplayDateFormat()]");

                            if (GCUtil.GetNetDataType(column) == "String")
                                //WriteLine(@"       [StringLength({1}, ErrorMessage = ""El campo {0} no debe tener mas de {1} caracteres"")]", column.Name, column.Length == 0 ? int.MaxValue : column.Length);
                                sw.WriteLine(@"        [RecursoStringLength(""{0}"",{1})]", Table.Name + "." + column.Name, column.Length == 0 || column.Length == -1 ? int.MaxValue : column.Length);
                            if (GCUtil.GetNetDataType(column) == "Boolean")
                                sw.WriteLine(@"        public virtual " + GCUtil.GetNetDataType(column) + " " + column.Code + " { get; set; }");
                            else
                                sw.WriteLine(@"        public virtual " + GCUtil.GetNetDataType(column) + ((!column.IsRequired && GCUtil.GetNetDataType(column) != "String") ? "? " : " ") + GCUtil.PascalCase(column.Code) + " { get; set; }");
                            sw.WriteLine();
                        }
                    sw.WriteLine(@"        #endregion");
                    sw.WriteLine();
                    sw.WriteLine(@"        #region Columnas normales");
                    sw.WriteLine();
                    if (normalColumns != null && normalColumns.Count > 0)
                        foreach (var column in normalColumns)
                        {
                            if (column.Code != "LastUpdate" && column.Code != "UpdatedBy")
                            {
                                if(!defaultNotas)
                                    defaultNotas = (column.Code == "Notas" ? column.Default != null : false);

                                sw.WriteLine(@"        [RecursoDisplayName(""{0}"")]", Table.Name + "." + column.Name);
                                if (column.IsRequired && !defaultNotas)
                                {
                                    //WriteLine(@"       [Required(AllowEmptyStrings = true, ErrorMessage = ""El campo {0} es requerido"")]", column.Name);
                                    sw.WriteLine(@"        [RecursoRequired(""{0}"")]", Table.Name + "." + column.Name);
                                    //if (GCUtil.GetNetDataType(column) == "Int32") 
                                    //    sw.WriteLine(@"       [RecursoIntRequired(""{0}"")]", Table.Name + "." + column.Name);
                                }
                                if (GCUtil.GetNetDataType(column) == "Byte[]") {
                                    sw.WriteLine(@"        //[MaxLength(65536)]");
                                    sw.WriteLine(@"        [Column(""BLOB_COLUMN"", TypeName = ""BLOB"")]");
                                }

                                if (GCUtil.GetNetDataType(column) == "DateTime")
                                    sw.WriteLine(@"        [DisplayDateFormat()]");

                                if (GCUtil.GetNetDataType(column) == "String")
                                    sw.WriteLine(@"        [RecursoStringLength(""{0}"",{1})]", Table.Name + "." + column.Name, column.Length == 0 || column.Length == -1 ? int.MaxValue : column.Length);
                                if (GCUtil.GetNetDataType(column) == "Boolean" || GCUtil.GetNetDataType(column) == "Byte[]")
                                    sw.WriteLine(@"        public virtual " + GCUtil.GetNetDataType(column) + " " + column.Code + " { get; set; }");
                                else
                                    sw.WriteLine(@"        public virtual " + GCUtil.GetNetDataType(column) + ((!column.IsRequired && GCUtil.GetNetDataType(column) != "String") ? "? " : " ") + GCUtil.PascalCase(column.Code) + " { get; set; }");
                                sw.WriteLine();
                            }
                        }
                    sw.WriteLine(@"        #endregion");
                    sw.WriteLine();
                    sw.WriteLine(@"        #region Columnas referenciales");
                    sw.WriteLine();
                    foreach (var column in inReferences)
                    {
                        sw.WriteLine(@"        [RecursoDisplayName(""{0}"")]", Table.Name + "." + column.Name);
                        if (GCUtil.GetNetDataType(column) == "DateTime")
                            sw.WriteLine(@"        [DisplayDateFormat()]");

                        if (column.IsRequired)
                        {
                            sw.WriteLine(@"        [RecursoRequired(""{0}"")]", Table.Name + "." + column.Name);
                            //if (GCUtil.GetNetDataType(column) == "Int32")
                            //    sw.WriteLine(@"       [RecursoIntRequired(""{0}"")]", Table.Name + "." + column.Name);
                        }
                        if (GCUtil.GetNetDataType(column) == "String")
                            sw.WriteLine(@"        [RecursoStringLength(""{0}"",{1})]", Table.Name + "." + column.Name, column.Length == 0 || column.Length == -1 ? int.MaxValue : column.Length);
                        if (GCUtil.GetNetDataType(column) == "Boolean")
                            sw.WriteLine(@"        public virtual " + GCUtil.GetNetDataType(column) + " " + GCUtil.PascalCase(column.Code) + " { get; set; }");
                        else
                            sw.WriteLine(@"        public virtual " + GCUtil.GetNetDataType(column) + ((!column.IsRequired && GCUtil.GetNetDataType(column) != "String") ? "? " : " ") + GCUtil.PascalCase(column.Code) + " { get; set; }");
                        sw.WriteLine();
                    }
                    sw.WriteLine(@"        #endregion");
                    sw.WriteLine();
                    sw.WriteLine();

                    sw.WriteLine(@"        #region Reglas expression");

                    //if (!(Project.Contains("Nomina") && Table.Name.StartsWith("t")))
                    //{

                        sw.WriteLine();

                        sw.WriteLine($@"        public override Expression<Func<T, bool>> PrimaryKeyExpression<T>()");
                        sw.WriteLine($@"        {{");
                        sw.WriteLine($@"            Expression<Func<{Table.Code}, bool>> expression = entity => {GetExpresionStringCols(pks.Select(x => x.Code))};");
                        sw.WriteLine($@"            return expression as Expression<Func<T, bool>>;");
                        sw.WriteLine($@"        }}");

                        sw.WriteLine();


                        //Adicionar

                        sw.WriteLine($@"        public override List<ExpRecurso> GetAdicionarExpression<T>()");
                        sw.WriteLine($@"        {{");
                        sw.WriteLine($@"            var rules = new List<ExpRecurso>();");
                        sw.WriteLine($@"            Expression<Func<{Table.Code}, bool>> expression = null;");
                        sw.WriteLine();

                        foreach (var item in unique)
                        {
                            sw.WriteLine($@"            expression = entity => {GetExpresionStringCols(item.Select(x => GCUtil.CleanName(x.ColumnName, true)))};");
                            sw.WriteLine($@"            rules.Add(new ExpRecurso(expression.ToExpressionNode(), new Recurso(""BLL.BUSINESS.UNIQUE"",{ GetExpresionStringCols(item.Select(x => x)) })));");
                            sw.WriteLine();
                        }

                        //foreach (var item in RefIn)
                        //{
                        //    var id = RefIn.IndexOf(item);
                        //    var entity = item.Select(x => x.ParentTableCode).FirstOrDefault();

                        //    sw.WriteLine($@"        Expression<Func<{entity}, bool>> expression{id} = entity => {GetExpresionStringCols(item.Select(x => GCUtil.CleanName(x.ColumnName, true)), item.Select(x => GCUtil.CleanName(x.ParentColumnName, true)))};");
                        //    sw.WriteLine($@"        rules.Add(new ExpRecurso(expression{id}.ToExpressionNode() , ""Valor for in no existe"", typeof({entity}), false));");
                        //    sw.WriteLine();
                        //}

                        //foreach (var item in RefOut)
                        //{
                        //    var id = RefOut.IndexOf(item);
                        //    var entity = item.Select(x => x.ParentTableCode).FirstOrDefault();

                        //    sw.WriteLine($@"        Expression<Func<{entity}, bool>> expression{id} = entity => {GetExpresionStringCols(item.Select(x => GCUtil.CleanName(x.ColumnName, true)), item.Select(x => GCUtil.CleanName(x.ParentColumnName, true)))};");
                        //    sw.WriteLine($@"        rules.Add(new ExpRecurso(expression{id}.ToExpressionNode() , ""Valor for out duplicado"", typeof({entity})));");
                        //    sw.WriteLine();
                        //}

                        sw.WriteLine($@"            return rules;");
                        sw.WriteLine($@"        }}");


                        sw.WriteLine();


                        //Modificar

                        sw.WriteLine($@"        public override List<ExpRecurso> GetModificarExpression<T>()");
                        sw.WriteLine($@"        {{");
                        sw.WriteLine($@"            var rules = new List<ExpRecurso>();");
                        sw.WriteLine($@"            Expression<Func<{Table.Code}, bool>> expression = null;");
                        sw.WriteLine();

                        foreach (var item in unique)
                        {
                            sw.WriteLine($@"            expression = entity => !({GetExpresionStringCols(pks.Select(x => x.Code))} && {GetExpresionStringCols(item.Select(x => GCUtil.CleanName(x.ColumnName, true)))})");
                            sw.WriteLine($@"                                   && {GetExpresionStringCols(item.Select(x => GCUtil.CleanName(x.ColumnName, true)))};");
                            sw.WriteLine($@"            rules.Add(new ExpRecurso(expression.ToExpressionNode(), new Recurso(""BLL.BUSINESS.UNIQUE"",{ GetExpresionStringCols(item.Select(x => x)) })));");
                            sw.WriteLine();
                        }

                        //foreach (var item in RefIn)
                        //{
                        //    var id = RefIn.IndexOf(item);
                        //    var entity = item.Select(x => x.ParentTableCode).FirstOrDefault();

                        //    sw.WriteLine($@"        Expression<Func<{entity}, bool>> expression{id} = entity => {GetExpresionStringCols(item.Select(x => GCUtil.CleanName(x.ColumnName, true)), item.Select(x => GCUtil.CleanName(x.ParentColumnName, true)))};");
                        //    sw.WriteLine($@"        rules.Add(new ExpRecurso(expression{id}.ToExpressionNode() , ""Valor for in no existe"", typeof({entity}), false));");
                        //    sw.WriteLine();
                        //}

                        //foreach (var item in RefOut)
                        //{
                        //    var id = RefOut.IndexOf(item);
                        //    var entity = item.Select(x => x.ParentTableCode).FirstOrDefault();

                        //    sw.WriteLine($@"        Expression<Func<{entity}, bool>> expression{id} = entity => {GetExpresionStringCols(item.Select(x => GCUtil.CleanName(x.ColumnName, true)), item.Select(x => GCUtil.CleanName(x.ParentColumnName, true)))};");
                        //    sw.WriteLine($@"        rules.Add(new ExpRecurso(expression{id}.ToExpressionNode() , ""Valor for out no existe"", typeof({entity}), false));");
                        //    sw.WriteLine();
                        //}

                        sw.WriteLine($@"            return rules;");
                        sw.WriteLine($@"        }}");


                        sw.WriteLine();


                        //Eliminar

                        sw.WriteLine($@"        public override List<ExpRecurso> GetEliminarExpression<T>()");
                        sw.WriteLine($@"        {{");
                        sw.WriteLine($@"            var rules = new List<ExpRecurso>();");
                        //sw.WriteLine($@"        Expression<Func<{Table.Code}, bool>> expression = null;");
                        //sw.WriteLine();

                        //foreach (var item in unique)
                        //{
                        //    sw.WriteLine($@"        expression = entity => {GetExpresionStringCols(item.Select(x => GCUtil.CleanName(x.Code, true)))};");
                        //    sw.WriteLine($@"        rules.Add(new ExpRecurso(expression.ToExpressionNode() , ""Valor unico duplicado""));");
                        //    sw.WriteLine();
                        //}

                        //foreach (var item in RefIn)
                        //{
                        //    var id = RefIn.IndexOf(item);
                        //    var entity = item.Select(x => x.ParentTableCode).FirstOrDefault();

                        //    sw.WriteLine($@"        Expression<Func<{entity}, bool>> expression{id} = entity => {GetExpresionStringCols(item.Select(x => GCUtil.CleanName(x.ColumnName, true)), item.Select(x => GCUtil.CleanName(x.ParentColumnName, true)))};");
                        //    sw.WriteLine($@"        rules.Add(new ExpRecurso(expression{id}.ToExpressionNode() , ""Valor for in duplicado"", typeof({entity})));");
                        //    sw.WriteLine();
                        //}


                        for (int i = 0; i < RefOut.Count; i++)
                        {
                            //if (i == 0)
                            //{
                            //    sw.WriteLine($@"       public override List<ExpRecurso> GetEliminarExpression<T>()");
                            //    sw.WriteLine($@"       {{");
                            //    sw.WriteLine($@"        var rules = new List<ExpRecurso>();");
                            //}
                            
                            var tableName = RefOut[i].Select(x => x.ParentTableName).FirstOrDefault();

                            if ((Project.Contains("Nomina") && !tableName.StartsWith("t")) || !Project.Contains("Nomina"))
                            {
                                var id = RefOut.IndexOf(RefOut[i]);
                                var tableNameClass = RefOut[i].Select(x => GCUtil.PascalCase(x.ParentTableName)).FirstOrDefault();

                                sw.WriteLine($@"            Expression<Func<{tableNameClass}, bool>> expression{id} = entity => {GetExpresionStringCols(RefOut[i].Select(x => GCUtil.CleanName(x.ColumnName, true)), RefOut[i].Select(x => GCUtil.CleanName(x.ParentColumnName, true)))};");
                                sw.WriteLine($@"            rules.Add(new ExpRecurso(expression{id}.ToExpressionNode(), new Recurso(""BLL.BUSINESS.DELETE_REL"",""{ tableName }""), typeof({tableNameClass})));");
                                sw.WriteLine();
                            }
                        }

                        //if (RefOut.Count > 0) { 
                            sw.WriteLine($@"            return rules;");
                            sw.WriteLine($@"        }}");                        
                        //}

                        ////foreach (var item in RefOut)
                        ////{
                        ////    var id = RefOut.IndexOf(item);
                        ////    var tableNameClass = item.Select(x => GCUtil.PascalCase(x.ParentTableName)).FirstOrDefault();
                        ////    var tableName = item.Select(x => x.ParentTableName).FirstOrDefault();

                        ////    sw.WriteLine($@"        Expression<Func<{tableNameClass}, bool>> expression{id} = entity => {GetExpresionStringCols(item.Select(x => GCUtil.CleanName(x.ColumnName, true)), item.Select(x => GCUtil.CleanName(x.ParentColumnName, true)))};");
                        ////    sw.WriteLine($@"        rules.Add(new ExpRecurso(expression{id}.ToExpressionNode() , new Recurso(""BLL.BUSINESS.DELETE_REL"",""{ tableName }""), typeof({tableNameClass})));");
                        ////    sw.WriteLine();
                        ////}

                        ////sw.WriteLine($@"       return rules;");
                        ////sw.WriteLine($@"       }}");


                        sw.WriteLine();



                        //sw.WriteLine($@"       public override Expression<Func<T, bool>> GetAdicionarExpression<T>()");
                        //sw.WriteLine($@"       {{");
                        //sw.WriteLine($@"       Expression<Func<{Table.Code}, bool>> expression = entity => {GetPkExpresionStringCols(pks)};");
                        //sw.WriteLine($@"       return expression as Expression<Func<T, bool>>;");
                        //sw.WriteLine($@"       }}");

                        //sw.WriteLine();

                        //sw.WriteLine($@"       public override Expression<Func<T, bool>> GetModificarExpression<T>()");
                        //sw.WriteLine($@"       {{");
                        //sw.WriteLine($@"       Expression<Func<{Table.Code}, bool>> expression = entity => {GetPkExpresionStringCols(pks)};");
                        //sw.WriteLine($@"       return expression as Expression<Func<T, bool>>;");
                        //sw.WriteLine($@"       }}");

                        //sw.WriteLine();

                        //sw.WriteLine($@"       public override Expression<Func<T, bool>> GetEliminarExpression<T>()");
                        //sw.WriteLine($@"       {{");
                        //sw.WriteLine($@"       Expression<Func<{Table.Code}, bool>> expression = entity => {GetPkExpresionStringCols(pks)};");
                        //sw.WriteLine($@"       return expression as Expression<Func<T, bool>>;");
                        //sw.WriteLine($@"       }}");

                        sw.WriteLine();

                    //}

                    sw.WriteLine(@"        #endregion");

                    if (defaultNotas) { 
                        sw.WriteLine();
                        sw.WriteLine();

                        sw.WriteLine(@"        #region Valores defecto");

                        sw.WriteLine();

                        sw.WriteLine($@"        public {Table.Code}() {{");
                        sw.WriteLine($@"            this.Notas = ""{normalColumns.FirstOrDefault(x => x.Code == "Notas").Default.Replace("('", "").Replace("')","")}"";");
                        sw.WriteLine($@"        }}");

                        sw.WriteLine();

                        sw.WriteLine(@"        #endregion Valores defecto");
                    }

                        sw.WriteLine();

                    sw.WriteLine(@"    }");
                    sw.WriteLine(@"}");
                }

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }


        }

        private string GetExpresionStringCols(IEnumerable<string> cols)
        {
            string text = "";
            bool first = true;

            foreach (var item in cols)
            {
                if (first)
                {
                    text += "entity." + item + " == this." + item;
                    first = false;
                }
                else
                    text += " && entity." + item + " == this." + item;

            }

            return text;
        }

        private string GetExpresionStringCols(IEnumerable<IndexModel> cols)
        {
            string text = "";
            bool first = true;

            foreach (var item in cols)
            {
                if (first)
                {
                    text += $@" ""{item.TableName}.{item.ColumnName}""";
                    first = false;
                }
                else
                    text += $@", ""{item.TableName}.{item.ColumnName}""";

            }

            return text;
        }

        private string GetExpresionStringCols(IEnumerable<string> colsSource, IEnumerable<string> colsEntity)
        {
            string text = "";
            bool first = true;

            for (int i = 0; i < colsSource.Count(); i++)
            {
                if (first)
                {
                    text += "entity." + colsEntity.ElementAt(i) + " == this." + colsSource.ElementAt(i);
                    first = false;
                }
                else
                    text += " && entity." + colsEntity.ElementAt(i) + " == this." + colsSource.ElementAt(i);
            }

            return text;
        }

      


    }
}
