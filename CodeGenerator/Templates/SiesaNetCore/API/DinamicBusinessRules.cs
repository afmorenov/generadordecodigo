﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using Microsoft.EntityFrameworkCore.Metadata.Conventions;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace CodeGenerator.Templates.SiesaNetCore
{
    public class DinamicBusinessRules
    {

        public string Name { get; set; } = "API - Reglas dinamicas";
        public int Order { get; set; } = 10;
        public string Description { get; set; } = "Genera las reglas dinamicas para el funcionamiento de la aplicacion";

        #region private property
        
        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private string Project { get; set; }
        //private TableModel Table { get; set; }
        private List<TableModel> Tables { get; set; }
        private TemplateModel Template { get; set; }
        public DinamicBusinessRules() { } // Para usarse con reflection

        #endregion

        public DinamicBusinessRules(CodeGeneratorModel CodeGeneratorModel)
        {
            OutputFolder = CodeGeneratorModel.PathGenerate + "/API/Rules/";
            FullNameFile = "DinamicBusinessRules.json";

            var test = GCUtil.DataBaseInfo.FirstOrDefault(x => x.NumberConnection == 1).Tables.OrderBy(x => x.Name).ToList();


            #region asignacion
            //Table = CodeGeneratorModel.TableGenerated;
            Tables = CodeGeneratorModel.Tables;
            Template = CodeGeneratorModel.TemplateGenerated;
            Project = CodeGeneratorModel.Domain;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate()
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                //SetTableRules(Table, sw);

                ProduceCode(sw);

                //int idRule = 1;
                //sw.WriteLine("[");
                //foreach (var column in Table.Columns)
                //{
                //    WriteValidation("1", idRule, column, sw);
                //    idRule++;
                //}
                //sw.WriteLine("]");

                //idRule = 1;

                sw.Flush();
                sw.Close();

            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }


        }

        private void WriteValidationPrimaryKeys(List<ColumnModel> columns, StreamWriter sw)
        {

        }

        private int WriteValidation(int ruleType, int idRule, ColumnModel column, TableModel Table, StreamWriter sw)
        {
            bool primary = column.IsPrimaryKey;
            bool normal = !column.IsPrimaryKey && !column.IsFKIn;
            bool inReference = column.IsFKIn && !column.IsPrimaryKey;

            if (ruleType == ((int)GCUtil.RuleType.CommonRules))
            {
                if (!primary && normal) {
                    idRule = Required(ruleType, idRule, column, Table, sw);
                    idRule = MaxLength(ruleType, idRule, column, Table, sw);
                }
            }
            else if (ruleType == ((int)GCUtil.RuleType.AddRules))
            {
                if (!primary && !normal) {
                    idRule = Required(ruleType, idRule, column, Table, sw);
                    idRule = MaxLength(ruleType, idRule, column, Table, sw);
                }
            }
            else if (ruleType == ((int)GCUtil.RuleType.MofidyRules))
            {
                if (!normal) { 
                    idRule = Required(ruleType, idRule, column, Table, sw);
                    idRule = MaxLength(ruleType, idRule, column, Table, sw);
                }
            }
            else if (ruleType == ((int)GCUtil.RuleType.RemoveRules))
            {
                if (primary)
                {
                    idRule = Required(ruleType, idRule, column, Table, sw);
                }
            }
            return idRule;
        }

        private int Required(int ruleType, int idRule, ColumnModel column, TableModel Table, StreamWriter sw)
        {
            string type = GCUtil.GetNetDataType(column);

            if (column.IsRequired) {

                if (type == "String")
                {
                    sw.WriteLine(@"  {");
                    sw.WriteLine(@"    ""Id"" : {0},", idRule);
                    sw.WriteLine(@"    ""Rule"": ""{0} != null"",", column.Code);
                    sw.WriteLine(@"    ""RuleType"": {0},", ruleType);
                    sw.WriteLine(@"    ""Type"": ""{0}.Infraestructura.Entidades.{1}"",", Project, Table.Code);
                    sw.WriteLine(@"    ""ResourceId"": ""{0}"",", "BLL.TABLE.COLUMNS.ISREQUIRED");
                    sw.WriteLine(@"    ""ResourceFieldsId"": [""{0}""]", Table.Name + "." + column.Name);
                    sw.WriteLine(@"  },");
                    idRule++;
                }
                else if (type.Contains("Int") || type == "Double" || type == "Float" || type == "Short")
                {
                    sw.WriteLine(@"  {");
                    sw.WriteLine(@"    ""Id"" : {0},", idRule);
                    sw.WriteLine(@"    ""Rule"": ""{0} != 0"",", column.Code);
                    sw.WriteLine(@"    ""RuleType"": {0},", ruleType);
                    sw.WriteLine(@"    ""Type"": ""{0}.Infraestructura.Entidades.{1}"",", Project, Table.Code);
                    sw.WriteLine(@"    ""ResourceId"": ""{0}"",", "BLL.TABLE.COLUMNS.ISREQUIRED");
                    sw.WriteLine(@"    ""ResourceFieldsId"": [""{0}""]", Table.Name + "." + column.Name);
                    sw.WriteLine(@"  },");
                    idRule++;
                }
            }

            
            return idRule;
        }

        private int MaxLength(int ruleType, int idRule, ColumnModel column, TableModel Table, StreamWriter sw)
        {
            string type = GCUtil.GetNetDataType(column);
            
            if (type == "String")
            {
                if (column.Length > 0)
                {
                    sw.WriteLine(@"  {");
                    sw.WriteLine(@"    ""Id"" : {0},", idRule);
                    sw.WriteLine(@"    ""Rule"": ""({0} ?? \""\"").Length <= {1}"",", column.Code, column.Length);
                    sw.WriteLine(@"    ""RuleType"": {0},", ruleType);
                    sw.WriteLine(@"    ""Type"": ""{0}.Infraestructura.Entidades.{1}"",", Project, Table.Code);
                    sw.WriteLine(@"    ""ResourceId"": ""{0}"",", "BLL.TABLE.COLUMNS.MAXLENGTH");
                    sw.WriteLine(@"    ""ResourceFieldsId"": [""{0}"", ""{1}""]", Table.Name + "." + column.Name, column.Length);
                    sw.WriteLine(@"  },");
                    idRule++;
                }
            }
            return idRule;
        }

        public int SetTableRules(TableModel tableSiesa, StreamWriter sw, int idRule)
        {

            List<ColumnModel> pks = tableSiesa.Columns.Where(x => x.IsPrimaryKey).ToList();
            List<ColumnModel> normalColumns = tableSiesa.Columns.Where(x => !x.IsPrimaryKey && !x.IsFKIn).ToList();

            List<ColumnModel> inReferences = tableSiesa.Columns.Where(x => x.IsFKIn && !x.IsPrimaryKey).ToList();

            if (pks != null && pks.Count > 0)
            {
                #region Reglas
                foreach (ColumnModel column in pks)
                {
                    foreach (var item in RuleTypeDic)
                    {
                        idRule = WriteValidation(item.Value, idRule, column, tableSiesa, sw);
                    }
                    
                }
                #endregion
            }

            if (normalColumns != null && normalColumns.Count > 0)
            {
                #region Reglas
                foreach (ColumnModel column in normalColumns)
                {
                    foreach (var item in RuleTypeDic)
                    {
                        idRule = WriteValidation(item.Value, idRule, column, tableSiesa, sw);
                    }
                }
                #endregion
            }

            if (inReferences != null && inReferences.Count > 0)
            {
                #region Reglas
                foreach (ColumnModel column in inReferences)
                {
                    foreach (var item in RuleTypeDic)
                    {
                        idRule = WriteValidation(item.Value, idRule, column, tableSiesa, sw);
                    }
                }
                #endregion
            }

            sw.WriteLine("");

            return idRule;

        }

        public void ProduceCode(StreamWriter sw)
        {
            sw.WriteLine("[");
            int idRule = 1;
            foreach (TableModel table in Tables)
                idRule = SetTableRules(table, sw, idRule);
            sw.WriteLine("]");
        }



        private Dictionary<String, int> RuleTypeDic = new Dictionary<string, int>()
        {
            { "CommonRules" , ((int)GCUtil.RuleType.CommonRules)},
            { "AddRules" , ((int)GCUtil.RuleType.AddRules)},
            { "MofidyRules" , ((int)GCUtil.RuleType.MofidyRules)},
            { "RemoveRules" , ((int)GCUtil.RuleType.RemoveRules)}
        };

        

    }
}
