﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaNetCore
{
    public class ListViewDetail
    {

        public string Name { get; set; } = "APP - View ListDetail";
        public int Order { get; set; } = 6;
        public string Description { get; set; } = "Generates a Razor page for .NET";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private string Project { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public ListViewDetail() { } // Para usarse con reflection

        #endregion

        public ListViewDetail(CodeGeneratorModel CodeGeneratorModel)
        {
            OutputFolder = CodeGeneratorModel.PathGenerate + "/APP/" + CodeGeneratorModel.TableGenerated.Comment + "/Views/" + GCUtil.GetNumber(CodeGeneratorModel.TableGenerated.Name) + "/";
            FullNameFile = GCUtil.GetNumber(CodeGeneratorModel.TableGenerated.Name) + "ListDetail.cshtml";

            #region asignacion
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            Project = CodeGeneratorModel.Domain;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate() 
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                if (Table.Columns.Exists(x=>x.IsPrimaryKey))
                {
                    sw.WriteLine(@"@* ");
                    sw.WriteLine();
                    sw.WriteLine(@"@using {0}.UI.Modules.{1}.Models", Project, Table.Comment);
                    sw.WriteLine(@"@model WXXXXModel //Modelo del padre ");
                    sw.WriteLine(@"@{ ");
                    sw.WriteLine(@"    var viewData = new Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary(ViewData); ");
                    sw.WriteLine(@"    viewData.TemplateInfo.HtmlFieldPrefix = ViewBag.Prefix; ");
                    sw.WriteLine(@"    string Tablename = ViewBag.TableName; ");
                    sw.WriteLine(@"    string Prefix = ""{0}""; ", GCUtil.GetNumber(Table.Name));
                    sw.WriteLine(@"    string PrefixPadre = ViewBag.Prefix; ", GCUtil.GetNumber(Table.Name));
                    sw.WriteLine();
                    sw.WriteLine(@"    string UrlClick = Url.Action(""Edit"", ""{2}"", new {0} area = ""{3}"" {1}); ", "{", "}", GCUtil.GetNumber(Table.Name), Table.Comment);
                    sw.WriteLine(@"    string UrlNew = Url.Action(""New"", ""{2}"", new {0} area = ""{3}"", {4} {1}); ", "{", "}", GCUtil.GetNumber(Table.Name), Table.Comment, GCUtil.GetPkStringColsAndNameModel3(Table));
                    sw.WriteLine(@"    string UrlDeleteList = Url.Action(""DeleteList"", ""{2}"", new {0} area = ""{3}"" {1}); ", "{", "}", GCUtil.GetNumber(Table.Name), Table.Comment);
                    sw.WriteLine(@"    string UrlEditPadre = Url.Action(""Edit"", PrefixPadre, new {0} area = ""{3}"", {4} {1}); ", "{", "}", GCUtil.GetNumber(Table.Name), Table.Comment, GCUtil.GetPkStringColsAndNameModel3(Table));
                    sw.WriteLine();
                    sw.WriteLine(@"    var DataGridDetailConfig = new DataGridConfiguration<{0}Model>(Prefix,PrefixPadre) ", Table.Code);
                    sw.WriteLine(@"        .OnClick(UrlClick , HCore.ActionViewSecurity(Context,UrlClick), new {0} rowid = ""Rowid"" {1}) ", "{", "}");
                    sw.WriteLine(@"        .New(UrlNew,HCore.ActionViewSecurity(Context,UrlNew)) ");
                    sw.WriteLine(@"        .DeleteList(UrlDeleteList,HCore.ActionViewSecurity(Context,UrlDeleteList)) ");
                    sw.WriteLine(@"        .LinkNavigation( ");
                    sw.WriteLine(@"            HCore.Recursos.GetRecurso(Tablename), ");
                    sw.WriteLine(@"            UrlEditPadre, ");
                    sw.WriteLine(@"            HCore.Recursos.GetRecurso(""{0}"")) ", Table.Name);
                    sw.WriteLine(@"        .TooltipText(""Descripcion_Padre""); //Agregar atributo descripcion ej: Model.Campo o HCore.Recursos.GetRecurso o new JS(""FuncionJS"") ");
                    sw.WriteLine(@"} ");
                    sw.WriteLine();
                    sw.WriteLine(@"<div class=""tab-pane"" id=""@(PrefixPadre)TabPane@(Prefix)"">");
                    sw.WriteLine(@"@if (Model.EsNuevo)");
                    sw.WriteLine(@"{ ");
                    sw.WriteLine(@"    <div class=""text-warning""> ");
                    sw.WriteLine(@"        <ul><li>@HCore.Recursos.GetRecurso(""{0}.c0000_nuevo_registro"")</li></ul> ", Table.Name);
                    sw.WriteLine(@"    </div> ");
                    sw.WriteLine(@"} ");
                    sw.WriteLine(@"<div class=""box-body table-responsive no-padding""> ");
                    sw.WriteLine(@"    @(Html.Siesa().DataGridSimple<{0}Model>(DataGridDetailConfig) ", Table.Code);
                    sw.WriteLine(@"        .Disabled(Model.EsNuevo) ");
                    sw.WriteLine(@"        .DataSource(d => d.Mvc().LoadMethod(""POST"").Area(""{0}"").Controller(""{1}"").LoadAction(""Get"").Key({2}) ", Table.Comment, GCUtil.GetNumber(Table.Name), GCUtil.GetPkStringColsModel(Table));
                    sw.WriteLine(@"        .LoadParams(new {0} RowidPadre = Model.Rowid {1})) //agregar RowidPadre o LLave primaria en metodo GetDetail del controlador ", "{", "}");
                    sw.WriteLine(@"        .Columns(columns => ");
                    sw.WriteLine(@"        { ");
                    if (Table.Columns != null && Table.Columns.Count > 0)
                        for (int i = 0; i < Table.Columns.Count; i++)
                        {
                            var column = Table.Columns[i];
                            if (column.Code.Contains("FechaCreacion") || column.Code.Contains("UsuarioCreacion") || column.Code.Contains("FechaActualizacion") || column.Code.Contains("UsuarioActualizacion"))
                                sw.WriteLine(@"            columns.AddFor(m => m.{0}).HidingPriority({1}); ", column.Code, (i + 1));
                            else
                                sw.WriteLine(@"            columns.AddFor(m => m.{0}); ", column.Code);
                        }
                    sw.WriteLine(@"        }).ElementAttr(""class"",""gridToolbar"")   ");
                    sw.WriteLine(@"    ) ");
                    sw.WriteLine(@"    </div>");
                    sw.WriteLine(@"</div>");
                    sw.WriteLine();
                    sw.WriteLine(@"*@ ");
                }

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
            

        }

        private string GetPkStringCols(List<ColumnModel> pks)
        {
            string cols = "";
            for (int i = 0; i < pks.Count; i++)
            {
                if (i == 0)
                    cols += "t." + pks[i].Code;
                else
                    cols += " ,t." + pks[i].Code;
            }
            return cols;
        }

    }
}
