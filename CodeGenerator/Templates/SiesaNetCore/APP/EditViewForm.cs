﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaNetCore
{
    public class EditViewForm
    {

        public string Name { get; set; } = "APP - View _Edit (Form)";
        public int Order { get; set; } = 8;
        public string Description { get; set; } = "Generates a Razor page for .NET";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private string Project { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public EditViewForm() { } // Para usarse con reflection

        #endregion

        public EditViewForm(CodeGeneratorModel CodeGeneratorModel)
        {
            OutputFolder = CodeGeneratorModel.PathGenerate + "/APP/" + CodeGeneratorModel.TableGenerated.Comment + "/Views/" + GCUtil.GetNumber(CodeGeneratorModel.TableGenerated.Name) + "/";
            FullNameFile = "_" + GCUtil.GetNumber(CodeGeneratorModel.TableGenerated.Name) + "Edit.cshtml";

            #region asignacion
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            Project = CodeGeneratorModel.Domain;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate() 
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                if (Table.Columns.Exists(x=>x.IsPrimaryKey))
                {
                    sw.WriteLine(@"@using {0}.UI.Modules.{1}.Models", Project, Table.Comment);
                    sw.WriteLine(@"@model {0}Model ", Table.Code);
                    sw.WriteLine(@"@{string Prefix = ViewBag.Prefix;} ");
                    sw.WriteLine();
                    sw.WriteLine(@"@*<partial name=""_{0}EditJs.cshtml"" model=""Model"" view-data=""ViewData"" />*@ ", GCUtil.GetNumber(Table.Name));
                    sw.WriteLine();
                    sw.WriteLine(@"<form id = ""@(ViewBag.Prefix)Form"" asp-area=""{0}"" asp-controller=""{1}"" asp-action=@ViewBag.AspAction class=""form-horizontal"" data-ajax-mode=""replace"" data-ajax-update=@ViewBag.AjaxUpdatePanel data-ajax-success=""@(ViewBag.Prefix)FormSuccess"" data-ajax-failure=""SE.SDI.FormFailure"" data-ajax=""true""> ", Table.Comment, GCUtil.GetNumber(Table.Name));
                    sw.WriteLine(@"    @(Html.Siesa().CheckBoxFor(m => m.EsNuevo).ReadOnly(true).ID(ViewBag.Prefix + "".EsNuevo"").Visible(false))");
                    if (Table.Columns != null && Table.Columns.Count > 0)
                        for (int i = 0; i < Table.Columns.Count; i++)
                        {
                            var column = Table.Columns[i];
                            if (column.IsPrimaryKey || column.Code.Contains("FechaCreacion") || column.Code.Contains("UsuarioCreacion") || column.Code.Contains("FechaActualizacion") || column.Code.Contains("UsuarioActualizacion"))
                            {
                                if (column.Code.Equals("IdCia"))
                                {
                                    sw.WriteLine(@"    @(Html.Siesa().NumberBoxFor(m => m.IdCia).ReadOnly(true).ID(ViewBag.Prefix + ""IdCia"").Visible(false)) ", column.Code);
                                }
                                else
                                {
                                    if ((GCUtil.GetNetDataType(column) == "Int" || GCUtil.GetNetDataType(column) == "Int16" || GCUtil.GetNetDataType(column) == "Int32" || GCUtil.GetNetDataType(column) == "Int64" || GCUtil.GetNetDataType(column) == "Short"))
                                        sw.WriteLine(@"    @(Html.Siesa().NumberBoxFor(m => m.{0}).ReadOnly(!Model.EsNuevo).ID(ViewBag.Prefix + ""{0}"").Visible(false)) ", column.Code);
                                    else if ((GCUtil.GetNetDataType(column) == "Double" || GCUtil.GetNetDataType(column) == "Float" || GCUtil.GetNetDataType(column) == "Decimal"))
                                        sw.WriteLine(@"    @(Html.Siesa().NumberBoxFor(m => m.{0}).ReadOnly(!Model.EsNuevo).ID(ViewBag.Prefix + ""{0}"").Visible(false)) ", column.Code);
                                    else if (GCUtil.GetNetDataType(column) == "DateTime")
                                        sw.WriteLine(@"    @(Html.Siesa().DateBoxFor(m => m.{0}).ReadOnly(!Model.EsNuevo).DisplayFormat(""dd/MM/yyyy"").ID(ViewBag.Prefix + ""{0}"").Visible(false)) ", column.Code);
                                    else
                                        sw.WriteLine(@"    @(Html.Siesa().TextBoxFor(m => m.{0}).ReadOnly(!Model.EsNuevo).ID(ViewBag.Prefix + ""{0}"").Visible(false)) ", column.Code);
                                }
                            }
                        }
                    sw.WriteLine();

                    sw.WriteLine(@"    <div asp-validation-summary=""All"" class=""text-danger"" id=""@(ViewBag.Prefix)ListError""></div> ");
                    sw.WriteLine();
                    if (Table.Columns != null && Table.Columns.Count > 0)
                    {
                        sw.WriteLine(@"    <div class=""card""> ");
                        sw.WriteLine(@"        <div class=""card-body""> ");
                        for (int i = 0; i < Table.Columns.Count; i++)
                        {
                            var column = Table.Columns[i];
                            if (!(column.IsPrimaryKey || column.Code.Equals("Notas") || column.Code.Contains("FechaCreacion") || column.Code.Contains("UsuarioCreacion") || column.Code.Contains("FechaActualizacion") || column.Code.Contains("UsuarioActualizacion")))
                            {
                                sw.WriteLine(@"            <div class=""row""> ");
                                sw.WriteLine(@"                <div class=""col-sm-12""> ");
                                sw.WriteLine(@"                    <div id=""@(ViewBag.Prefix){0}"" class=""form-group""> ", "DivField" + column.Code);
                                sw.WriteLine(@"                    <label for=""@(ViewBag.Prefix + ""_{0}"")"">@HCore.Recursos.GetRecurso(""{1}"") {2}</label> ", column.Code, Table.Name + "." + column.Name, (column.IsRequired ? "<span class=\"text-danger\">*</span>" : ""));
                                if ((GCUtil.GetNetDataType(column) == "Int" || GCUtil.GetNetDataType(column) == "Int16" || GCUtil.GetNetDataType(column) == "Int32" || GCUtil.GetNetDataType(column) == "Int64" || GCUtil.GetNetDataType(column) == "Short"))
                                    sw.WriteLine(@"                    @(Html.Siesa().NumberBoxFor(m => m.{0}).ID(ViewBag.Prefix + ""{0}"").InputAttr(""class"", ""form-control"")) ", column.Code);
                                else if ((GCUtil.GetNetDataType(column) == "Double" || GCUtil.GetNetDataType(column) == "Float" || GCUtil.GetNetDataType(column) == "Decimal"))
                                    sw.WriteLine(@"                    @(Html.Siesa().NumberBoxFor(m => m.{0}).ID(ViewBag.Prefix + ""{0}"").InputAttr(""class"", ""form-control"")) ", column.Code);
                                else if (GCUtil.GetNetDataType(column) == "DateTime")
                                    sw.WriteLine(@"                    @(Html.Siesa().DateBoxFor(m => m.{0}).ID(ViewBag.Prefix + ""{0}"").DisplayFormat(""dd/MM/yyyy"").InputAttr(""class"", ""form-control"")) ", column.Code);
                                else
                                    sw.WriteLine(@"                    @(Html.Siesa().TextBoxFor(m => m.{0}).ID(ViewBag.Prefix + ""{0}"").InputAttr(""class"", ""form-control"")) ", column.Code);

                                sw.WriteLine(@"                    </div> ");
                                sw.WriteLine(@"                </div> ");
                                sw.WriteLine(@"            </div> ");
                                sw.WriteLine();
                            }
                        }
                        sw.WriteLine(@"        </div>");
                        sw.WriteLine(@"    </div> ");
                    }
                    sw.WriteLine();

                    ColumnModel columnPanel = Table.Columns.ToList().Find(x => x.Code == "Notas");
                    if (columnPanel != null)
                    {
                        sw.WriteLine(@"	<div class=""card card-nav-tabs card-persistent""> ");
                        sw.WriteLine(@"		<div class=""scrollmenu card-header card-header-primary"">  ");
                        sw.WriteLine(@"			<div class=""nav-tabs-navigation"">  ");
                        sw.WriteLine(@"				<div class=""nav-tabs-wrapper"">  ");
                        sw.WriteLine(@"					<ul class=""nav nav-tabs"" data-tabs=""tabs"">  ");
                        sw.WriteLine(@"						@*<li class=""nav-item"">  ");
                        sw.WriteLine(@"							<a class=""nav-link active"" href=""#@(ViewBag.Prefix)TabPaneGenerales"" data-toggle=""tab""> @HCore.Recursos.GetRecurso(""{0}.c0000_generales"")</a>  ", Table.Name);
                        sw.WriteLine(@"						</li>  ");
                        sw.WriteLine(@"						<li class=""nav-item"">  ");
                        sw.WriteLine(@"							<a class=""nav-link"" href=""#@(ViewBag.Prefix)TabPaneWXXXX"" data-toggle=""tab""> @HCore.Recursos.GetRecurso(""RECURSO_HIJO"")</a>  ");
                        sw.WriteLine(@"						</li>*@  ");
                        sw.WriteLine(@"						<li class=""nav-item"">  ");
                        sw.WriteLine(@"							<a class=""nav-link"" href=""#@(ViewBag.Prefix)TabPaneNotas"" data-toggle=""tab""> @HCore.Recursos.GetRecurso(""{0}"")</a>  ", Table.Name + "." + columnPanel.Name);
                        sw.WriteLine(@"						</li>  ");

                        sw.WriteLine(@"						@*@if (!string.IsNullOrWhiteSpace(Model.IdTipoEntidadDinamica()))");
                        sw.WriteLine(@"						{");
                        sw.WriteLine(@"						    <li class=""nav-item"">");
                        sw.WriteLine(@"						        <a class=""nav-link"" href=""#@(ViewBag.Prefix + ""TabPaneEntidad"" + Model.IdTipoEntidadDinamica())"" data-toggle=""tab""> Entidad Dinamica</a>");
                        sw.WriteLine(@"						    </li>");
                        sw.WriteLine(@"						}*@");

                        sw.WriteLine(@"					</ul>  ");
                        sw.WriteLine(@"				</div>  ");
                        sw.WriteLine(@"			</div>  ");
                        sw.WriteLine(@"		</div>  ");
                        sw.WriteLine(@"		<div class=""card-body"">  ");
                        sw.WriteLine(@"			<div class=""tab-content"">  ");
                        sw.WriteLine(@"			@*<div class=""tab-pane active"" id=""@(ViewBag.Prefix)TabPaneGenerales"">  ");
                        sw.WriteLine(@"				CONTENIDO  ");
                        sw.WriteLine(@"			</div>  ");
                        sw.WriteLine(@"			<partial name=""../WXXXX/WXXXXListDetail.cshtml"" model=Model />*@  ");
                        sw.WriteLine(@"				<div class=""tab-pane"" id=""@(ViewBag.Prefix)TabPaneNotas"">  ");
                        sw.WriteLine(@"					<div class=""row"">  ");
                        sw.WriteLine(@"						<div class=""col-sm-12"">  ");
                        sw.WriteLine(@"						@(Html.Siesa().TextAreaFor(m => m.Notas).ID(ViewBag.Prefix + ""Notas"").InputAttr(""class"", ""form-control""))  ");
                        sw.WriteLine(@"						</div>  ");
                        sw.WriteLine(@"					</div>  ");
                        sw.WriteLine(@"				</div>  ");

                        sw.WriteLine(@"				@*@if (!string.IsNullOrWhiteSpace(Model.IdTipoEntidadDinamica()))");
                        sw.WriteLine(@"			    {");
                        sw.WriteLine(@"				    <partial name=""~/Views/Shared/EntidadDinamica/EntidadDinamicaGrupo.cshtml"" model=Model />");
                        sw.WriteLine(@"			    }*@");

                        sw.WriteLine(@"			</div>  ");
                        sw.WriteLine(@"		</div>  ");
                        sw.WriteLine(@"	</div> ");
                    }
                    else
                    {
                        sw.WriteLine(@"	@* ");
                        sw.WriteLine(@"	<div class=""card card-nav-tabs card-persistent""> ");
                        sw.WriteLine(@"		<div class=""scrollmenu card-header card-header-primary"">  ");
                        sw.WriteLine(@"			<div class=""nav-tabs-navigation"">  ");
                        sw.WriteLine(@"				<div class=""nav-tabs-wrapper"">  ");
                        sw.WriteLine(@"					<ul class=""nav nav-tabs"" data-tabs=""tabs"">  ");
                        sw.WriteLine(@"						<li class=""nav-item"">  ");
                        sw.WriteLine(@"							<a class=""nav-link active"" href=""#@(ViewBag.Prefix)TabPaneGenerales"" data-toggle=""tab""> @HCore.Recursos.GetRecurso(""{0}.c0000_generales"")</a>  ", Table.Name);
                        sw.WriteLine(@"						</li>  ");
                        sw.WriteLine(@"						<li class=""nav-item"">  ");
                        sw.WriteLine(@"							<a class=""nav-link"" href=""#@(ViewBag.Prefix)TabPaneWXXXX"" data-toggle=""tab""> @HCore.Recursos.GetRecurso(""RECURSO_HIJO"")</a>  ");
                        sw.WriteLine(@"						</li>  ");

                        sw.WriteLine(@"						@*@if (!string.IsNullOrWhiteSpace(Model.IdTipoEntidadDinamica()))");
                        sw.WriteLine(@"						{");
                        sw.WriteLine(@"						    <li class=""nav-item"">");
                        sw.WriteLine(@"						        <a class=""nav-link"" href=""#@(ViewBag.Prefix + ""TabPaneEntidad"" + Model.IdTipoEntidadDinamica())"" data-toggle=""tab""> Entidad Dinamica</a>");
                        sw.WriteLine(@"						    </li>");
                        sw.WriteLine(@"						}*@");

                        sw.WriteLine(@"					</ul>  ");
                        sw.WriteLine(@"				</div>  ");
                        sw.WriteLine(@"			</div>  ");
                        sw.WriteLine(@"		</div>  ");
                        sw.WriteLine(@"		<div class=""card-body"">  ");
                        sw.WriteLine(@"			<div class=""tab-content"">  ");
                        sw.WriteLine(@"			<div class=""tab-pane active"" id=""@(ViewBag.Prefix)TabPaneGenerales"">  ");
                        sw.WriteLine(@"				CONTENIDO  ");
                        sw.WriteLine(@"			</div>  ");
                        sw.WriteLine(@"			<partial name=""../WXXXX/WXXXXListDetail.cshtml"" model=Model />  ");

                        sw.WriteLine(@"				@*@if (!string.IsNullOrWhiteSpace(Model.IdTipoEntidadDinamica()))");
                        sw.WriteLine(@"			    {");
                        sw.WriteLine(@"				    <partial name=""~/Views/Shared/EntidadDinamica/EntidadDinamicaGrupo.cshtml"" model=Model />");
                        sw.WriteLine(@"			    }*@");

                        sw.WriteLine(@"			</div>  ");
                        sw.WriteLine(@"		</div>  ");
                        sw.WriteLine(@"	</div> ");
                        sw.WriteLine(@"	*@ ");
                    }

                    sw.WriteLine();
                    sw.WriteLine(@"</form> ");
                }

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
            

        }

        private string GetPkStringCols(List<ColumnModel> pks)
        {
            string cols = "";
            for (int i = 0; i < pks.Count; i++)
            {
                if (i == 0)
                    cols += "t." + pks[i].Code;
                else
                    cols += " ,t." + pks[i].Code;
            }
            return cols;
        }

    }
}
