﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaNetCore
{
    public class UIController
    {

        public string Name { get; set; } = "APP - Controller";
        public int Order { get; set; } = 4;
        public string Description { get; set; } = "Generates a Base Class Controller for .NET";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private string Project { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public UIController() { } // Para usarse con reflection

        #endregion

        public UIController(CodeGeneratorModel CodeGeneratorModel)
        {
            OutputFolder = CodeGeneratorModel.PathGenerate + "/APP/" + CodeGeneratorModel.TableGenerated.Comment + "/Controllers/";
            FullNameFile = GCUtil.GetNumber(CodeGeneratorModel.TableGenerated.Name) + "Controller.cs";

            #region asignacion
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            Project = CodeGeneratorModel.Domain;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate() 
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                if (Table.Columns.Exists(x=>x.IsPrimaryKey))
                {
                    sw.WriteLine(@"using DevExtreme.AspNet.Data.ResponseModel;");
                    sw.WriteLine(@"using DevExtreme.AspNet.Mvc;");
                    sw.WriteLine(@"using Hefestos.Frontend.Core.Controllers;");
                    sw.WriteLine(@"using Microsoft.AspNetCore.Authorization;");
                    sw.WriteLine(@"using Microsoft.AspNetCore.Http;");
                    sw.WriteLine(@"using Microsoft.AspNetCore.Mvc;");
                    sw.WriteLine(@"using Microsoft.Extensions.Configuration;");
                    sw.WriteLine(@"using {0}.Infraestructura.Entidades;", Project);
                    sw.WriteLine(@"using {0}.UI.Modules.{1}.Models;", Project, Table.Comment);
                    sw.WriteLine(@"using System;");
                    sw.WriteLine(@"using System.Collections.Generic;");

                    sw.WriteLine();
                    sw.WriteLine(@"namespace {0}.UI.Modules.{1}.Controllers", Project, Table.Comment);
                    sw.WriteLine(@"{");
                    sw.WriteLine();
                    sw.WriteLine(@"   [Area(""{0}"")]", Table.Comment);
                    sw.WriteLine(@"   [Authorize] ");
                    sw.WriteLine(@"   public partial class {0}Controller : BaseController", GCUtil.GetNumber(Table.Name));
                    sw.WriteLine(@"   {");
                    sw.WriteLine();
                    sw.WriteLine(@"      private const string Prefix = ""{0}""; ", GCUtil.GetNumber(Table.Name));
                    sw.WriteLine(@"      private const string TableName = ""{0}""; ", Table.Name);
                    sw.WriteLine();
                    sw.WriteLine(@"      public {0}Controller(IConfiguration config, IHttpContextAccessor httpContextAccessor) : base(config, httpContextAccessor)", GCUtil.GetNumber(Table.Name));
                    sw.WriteLine(@"      {");
                    sw.WriteLine(@"      }");
                    sw.WriteLine();
                    sw.WriteLine(@"      [HttpPost]");
                    sw.WriteLine(@"      public LoadResult Get(DataSourceLoadOptions loadOptions)");
                    sw.WriteLine(@"      {");
                    bool columnIdCia = Table.Columns.ToList().Exists(x => x.Code == "IdCia");
                    if (columnIdCia)
                    {
                        sw.WriteLine(@"        loadOptions = httpContextAccessor.SetLoadOptionsFilter(loadOptions, true);");
                    }
                    sw.WriteLine(@"         return Manager().GeneralLogicaNegocio<{0}>().BuscarTodos(loadOptions).ToLoadResult();", Table.Code);
                    sw.WriteLine(@"      }");
                    sw.WriteLine();
                    sw.WriteLine(@"      public IActionResult List()");
                    sw.WriteLine(@"      {");
                    sw.WriteLine(@"         return View(""{0}List"");", GCUtil.GetNumber(Table.Name));
                    sw.WriteLine(@"      }");
                    sw.WriteLine();
                    sw.WriteLine(@"      public IActionResult ListPartial()");
                    sw.WriteLine(@"      {");
                    sw.WriteLine(@"         return PartialView(""{0}List"");", GCUtil.GetNumber(Table.Name));
                    sw.WriteLine(@"      }");
                    sw.WriteLine();
                    sw.WriteLine(@"      [HttpGet]");
                    sw.WriteLine(@"      public IActionResult New()");
                    sw.WriteLine(@"      {");
                    sw.WriteLine(@"         return PartialView(""{0}Edit"", NewModel());", GCUtil.GetNumber(Table.Name));
                    sw.WriteLine(@"      }");
                    sw.WriteLine();
                    sw.WriteLine(@"      private {0}Model NewModel() ", Table.Code);
                    sw.WriteLine(@"      { ");
                    sw.WriteLine(@"         ViewBag.Prefix = Prefix; ");
                    sw.WriteLine(@"         ViewBag.TableName = TableName; ");
                    sw.WriteLine(@"         {0}Model model = new {0}Model();", Table.Code);
                    sw.WriteLine(@"         model.EsNuevo = true;");
                    if (Table.Columns != null && Table.Columns.Count > 0)
                        for (int i = 0; i < Table.Columns.Count; i++)
                        {
                            var column = Table.Columns[i];
                            if (column.Code.Equals("IdCia"))
                                sw.WriteLine(@"         model.IdCia = httpContextAccessor.GetIdCompaniaActual();");
                            if (column.Code.Contains("UsuarioCreacion"))
                                sw.WriteLine(@"         model.UsuarioCreacion = User.Identity.Name;");
                            if (column.Code.Contains("UsuarioActualizacion"))
                                sw.WriteLine(@"         model.UsuarioActualizacion = User.Identity.Name;");
                            if (column.Code.Contains("FechaCreacion"))
                                sw.WriteLine(@"         model.FechaCreacion = DateTime.Now;");
                            //if (column.Code.Contains("FechaActualizacion"))
                            //    sw.WriteLine(@"         model.FechaActualizacion = DateTime.Now;");
                            if (column.Code.Equals("Notas"))
                                sw.WriteLine(@"         model.Notas = ""."";");
                        }
                    sw.WriteLine(@"         return model; ");
                    sw.WriteLine(@"      } ");
                    sw.WriteLine();
                    sw.WriteLine(@"      [HttpGet]");
                    sw.WriteLine(@"      public IActionResult Edit({0})", GCUtil.GetPkStringColsWhitDataType(Table));
                    sw.WriteLine(@"      {");
                    sw.WriteLine(@"         return PartialView(""{0}Edit"",EditModel({1}));", GCUtil.GetNumber(Table.Name), GCUtil.GetPkStringCols(Table));
                    sw.WriteLine(@"      }");
                    sw.WriteLine();
                    sw.WriteLine(@"      private {0}Model EditModel({1}) ", Table.Code, GCUtil.GetPkStringColsWhitDataType(Table));
                    sw.WriteLine(@"      { ");
                    sw.WriteLine(@"         ViewBag.Prefix = Prefix; ");
                    sw.WriteLine(@"         ViewBag.TableName = TableName; ");
                    sw.WriteLine(@"         {0}Model model = Manager().GeneralLogicaNegocio<{0}>().BuscarPorId({1}).ToModel(); ", Table.Code, GCUtil.GetPkStringColsQuery(Table));
                    sw.WriteLine(@"         model.EsNuevo = false; ");
                    if (Table.Columns != null && Table.Columns.Count > 0)
                        for (int i = 0; i < Table.Columns.Count; i++)
                        {
                            var column = Table.Columns[i];
                            if (column.Code.Contains("UsuarioActualizacion"))
                                sw.WriteLine(@"         model.UsuarioActualizacion = User.Identity.Name;");
                            //if (column.Code.Contains("FechaActualizacion"))
                            //    sw.WriteLine(@"         model.FechaActualizacion = DateTime.Now;");
                            if (column.Code.Equals("Notas"))
                                sw.WriteLine(@"         model.Notas = String.IsNullOrWhiteSpace(model.Notas) ? ""."" : model.Notas;");
                        }
                    sw.WriteLine(@"         return model; ");
                    sw.WriteLine(@"      } ");
                    sw.WriteLine();
                    sw.WriteLine(@"      [HttpPost]");
                    sw.WriteLine(@"      public ActionResult Edit([Bind(Prefix = Prefix)] {0}Model model)", Table.Code);
                    sw.WriteLine(@"      {");
                    sw.WriteLine(@"         return PartialView(""{0}Edit"",EditModel(model));", GCUtil.GetNumber(Table.Name));
                    sw.WriteLine(@"      }");
                    sw.WriteLine();
                    sw.WriteLine(@"      private {0}Model EditModel({0}Model model) ", Table.Code);
                    sw.WriteLine(@"      { ");
                    sw.WriteLine(@"         ViewBag.Prefix = Prefix; ");
                    sw.WriteLine(@"         ViewBag.TableName = TableName; ");
                    sw.WriteLine(@"         var OnState = model.EsNuevo; ");
                    sw.WriteLine(@"         if (ModelState.IsValid) ");
                    sw.WriteLine(@"         { ");
                    sw.WriteLine(@"            try ");
                    sw.WriteLine(@"            { ");
                    sw.WriteLine(@"               if (model.EsNuevo) ");
                    sw.WriteLine(@"               { ");
                    sw.WriteLine(@"                  model = Manager().GeneralLogicaNegocio<{0}>().Adicionar(model.ToEntity()).ToModel(); ", Table.Code);
                    sw.WriteLine(@"                  model.EsNuevo = false;");
                    sw.WriteLine(@"               } ");
                    sw.WriteLine(@"               else ");
                    sw.WriteLine(@"               { ");
                    sw.WriteLine(@"                  model = Manager().GeneralLogicaNegocio<{0}>().Modificar(model.ToEntity()).ToModel(); ", Table.Code);
                    sw.WriteLine(@"               } ");
                    sw.WriteLine(@"               if (model.Errores != null && model.Errores.Count > 0) ");
                    sw.WriteLine(@"               { ");
                    sw.WriteLine(@"                  for (int i = 0; i < model.Errores.Count; i++) ");
                    sw.WriteLine(@"                  { ");
                    sw.WriteLine(@"                     ModelState.AddModelError(""Error "" + i, model.Errores[i]); ");
                    sw.WriteLine(@"                  } ");
                    sw.WriteLine(@"                  if (OnState) model.EsNuevo = true; ");
                    sw.WriteLine(@"               } ");
                    sw.WriteLine(@"            } ");
                    sw.WriteLine(@"            catch (Exception e) ");
                    sw.WriteLine(@"            { ");
                    sw.WriteLine(@"               while (e!=null) ");
                    sw.WriteLine(@"               { ");
                    sw.WriteLine(@"                  ModelState.AddModelError(""Error: "", e.Message); ");
                    sw.WriteLine(@"                  e = e.InnerException; ");
                    sw.WriteLine(@"               } ");
                    sw.WriteLine(@"            } ");
                    sw.WriteLine(@"         } ");
                    sw.WriteLine(@"         return model; ");
                    sw.WriteLine(@"      } ");
                    sw.WriteLine();
                    sw.WriteLine(@"      [HttpGet]");
                    sw.WriteLine(@"      public JsonResult Delete({0})", GCUtil.GetPkStringColsWhitDataType(Table));
                    sw.WriteLine(@"      {");
                    sw.WriteLine(@"         var model = DeleteModel({0});", GCUtil.GetPkStringCols(Table));
                    sw.WriteLine(@"         TieneErrores(model);");
                    sw.WriteLine(@"         Dictionary<String, object> result = new Dictionary<string, object>();");
                    sw.WriteLine(@"         result.Add(""error"", model.Errores);");
                    sw.WriteLine(@"         return Json(result);");
                    sw.WriteLine(@"      }");
                    sw.WriteLine();
                    sw.WriteLine(@"      private {0}Model DeleteModel({1}) ", Table.Code, GCUtil.GetPkStringColsWhitDataType(Table));
                    sw.WriteLine(@"      { ");
                    sw.WriteLine(@"         ViewBag.Prefix = Prefix; ");
                    sw.WriteLine(@"         ViewBag.TableName = TableName; ");
                    sw.WriteLine(@"         {0}Model model = Manager().GeneralLogicaNegocio<{0}>().Eliminar({1}).ToModel(); ", Table.Code, GCUtil.GetPkStringColsQuery(Table));
                    sw.WriteLine(@"         if (model.Errores != null && model.Errores.Count > 0) ");
                    sw.WriteLine(@"            for (int i = 0; i < model.Errores.Count; i++) ");
                    sw.WriteLine(@"            { ");
                    sw.WriteLine(@"               ModelState.AddModelError(""Error "" + i, model.Errores[i]); ");
                    sw.WriteLine(@"            } ");
                    sw.WriteLine(@"         return model; ");
                    sw.WriteLine(@"      } ");
                    sw.WriteLine();
                    sw.WriteLine(@"      [HttpPost]");
                    sw.WriteLine(@"      public JsonResult DeleteList([FromBody]List<{0}> models)", Table.Code);
                    sw.WriteLine(@"      { ");
                    sw.WriteLine(@"         var result = Manager().GeneralLogicaNegocio<{0}>().EliminarLista(models);", Table.Code);
                    sw.WriteLine(@"         Dictionary<String, object> ErrorResult = new Dictionary<string, object>();");
                    sw.WriteLine(@"         List<Object> Errors = new List<Object>();");
                    sw.WriteLine(@"         foreach (var i in result)");
                    sw.WriteLine(@"         {");
                    sw.WriteLine(@"            if (i.TieneErrores)");
                    sw.WriteLine(@"               Errors.Add(i.Errores);");
                    sw.WriteLine(@"         }");
                    sw.WriteLine(@"         ErrorResult.Add(""error"", Errors);");
                    sw.WriteLine(@"         return Json(ErrorResult);");
                    sw.WriteLine(@"      } ");
                    sw.WriteLine();
                    sw.WriteLine(@"      [HttpGet]");
                    sw.WriteLine(@"      public IActionResult Duplicate({0})", GCUtil.GetPkStringColsWhitDataType(Table));
                    sw.WriteLine(@"      {");
                    sw.WriteLine(@"         {0}Model model = EditModel({1});", Table.Code, GCUtil.GetPkStringCols(Table));
                    sw.WriteLine(@"         model.EsNuevo = true;");
                    if (Table.Columns != null && Table.Columns.Count > 0)
                        for (int i = 0; i < Table.Columns.Count; i++)
                        {
                            var column = Table.Columns[i];
                            if (column.IsPrimaryKey)
                            {
                                if (!column.Code.Contains("IdCia"))
                                {
                                    if ((GCUtil.GetNetDataType(column) == "Int" || GCUtil.GetNetDataType(column) == "Int16" || GCUtil.GetNetDataType(column) == "Int32" || GCUtil.GetNetDataType(column) == "Int64" || GCUtil.GetNetDataType(column) == "Short"))
                                        sw.WriteLine(@"         model.{0} = 0;", column.Code);
                                    else if ((GCUtil.GetNetDataType(column) == "Double" || GCUtil.GetNetDataType(column) == "Float" || GCUtil.GetNetDataType(column) == "Decimal"))
                                        sw.WriteLine(@"         model.{0} = 0;", column.Code);
                                    else if (GCUtil.GetNetDataType(column) == "DateTime")
                                        sw.WriteLine(@"         model.{0} = DateTime.Now;", column.Code);
                                    else
                                        sw.WriteLine(@"         model.{0} = null;", column.Code);
                                }
                            }
                            if (column.Code.Contains("UsuarioCreacion"))
                                sw.WriteLine(@"         model.UsuarioCreacion = User.Identity.Name;");
                            if (column.Code.Contains("UsuarioActualizacion"))
                                sw.WriteLine(@"         model.UsuarioActualizacion = User.Identity.Name;");
                            if (column.Code.Contains("FechaCreacion"))
                                sw.WriteLine(@"         model.FechaCreacion = DateTime.Now;");
                            if (column.Code.Contains("RowidMovtoEntidad"))
                                sw.WriteLine(@"         model.RowidMovtoEntidad = null;");
                            if (column.Code.Contains("RowidFoto"))
                                sw.WriteLine(@"         model.RowidFoto = 0;");
                            //if (column.Code.Contains("FechaActualizacion"))
                            //    sw.WriteLine(@"         model.FechaActualizacion = DateTime.Now;");
                        }

                    sw.WriteLine(@"         var entDinamica = model.EntidadDinamica;");
                    sw.WriteLine(@"         if (entDinamica != null) {");
                    sw.WriteLine(@"             entDinamica.RowidMovtoIni = 0;");
                    sw.WriteLine(@"             entDinamica.RowidMovto = null;");
                    sw.WriteLine(@"             var newList = new List<Hefestos.Backend.Core.EntidadDinamica.EntidadDinamicaAdjuntosModel>();");
                    sw.WriteLine();
                    sw.WriteLine(@"             if (entDinamica.Adjuntos != null) {");
                    sw.WriteLine(@"                 entDinamica.Adjuntos.ForEach(x => {");
                    sw.WriteLine(@"                     var newItem = new Hefestos.Backend.Core.EntidadDinamica.EntidadDinamicaAdjuntosModel{");
                    sw.WriteLine(@"                         New = true,");
                    sw.WriteLine(@"                         filename = x.f751NombreArchivo,");
                    sw.WriteLine(@"                         f751NombreArchivoOriginal = x.f751NombreArchivoOriginal");
                    sw.WriteLine(@"                     };");
                    sw.WriteLine(@"                     newList.Add(newItem);");
                    sw.WriteLine(@"                 });");
                    sw.WriteLine(@"                 entDinamica.Adjuntos = newList;");
                    sw.WriteLine(@"             }");
                    sw.WriteLine(@"         }");
                    sw.WriteLine(@"         return PartialView(""{0}Edit"", model);", GCUtil.GetNumber(Table.Name));
                    sw.WriteLine(@"      }");
                    sw.WriteLine();
                    sw.WriteLine(@"   }");
                    sw.WriteLine(@"}");
                }

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
            

        }

        private string GetPkStringCols(List<ColumnModel> pks)
        {
            string cols = "";
            for (int i = 0; i < pks.Count; i++)
            {
                if (i == 0)
                    cols += "t." + pks[i].Code;
                else
                    cols += " ,t." + pks[i].Code;
            }
            return cols;
        }

    }
}
