﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaNetCore
{
    public class EditView
    {

        public string Name { get; set; } = "APP - View Edit (Toolbar)";
        public int Order { get; set; } = 7;
        public string Description { get; set; } = "Generates a Razor page for .NET";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private string Project { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public EditView() { } // Para usarse con reflection

        #endregion

        public EditView(CodeGeneratorModel CodeGeneratorModel)
        {
            OutputFolder = CodeGeneratorModel.PathGenerate + "/APP/" + CodeGeneratorModel.TableGenerated.Comment + "/Views/" + GCUtil.GetNumber(CodeGeneratorModel.TableGenerated.Name) + "/";
            FullNameFile = GCUtil.GetNumber(CodeGeneratorModel.TableGenerated.Name) + "Edit.cshtml";

            #region asignacion
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            Project = CodeGeneratorModel.Domain;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate() 
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                if (Table.Columns.Exists(x=>x.IsPrimaryKey))
                {
                    sw.WriteLine(@"@using {0}.UI.Modules.{1}.Models ", Project, Table.Comment);
                    sw.WriteLine();
                    sw.WriteLine(@"@model {0}Model ", Table.Code);
                    sw.WriteLine(@"@{");
                    sw.WriteLine(@"   var viewData = new Microsoft.AspNetCore.Mvc.ViewFeatures.ViewDataDictionary(ViewData);");
                    sw.WriteLine(@"   viewData.TemplateInfo.HtmlFieldPrefix = ViewBag.Prefix;");
                    sw.WriteLine(@"   ViewBag.AjaxUpdatePanel = ""#mainPanel""; ");
                    sw.WriteLine(@"   ViewBag.AspAction = ""Edit""; ");
                    sw.WriteLine(@"   string Prefix = ViewBag.Prefix; ");
                    sw.WriteLine();
                    sw.WriteLine(@"   string UrlSave = Url.Action(""Edit"", ""{2}"", new {0} area = ""{3}"" {1}); ", "{", "}", GCUtil.GetNumber(Table.Name), Table.Comment);
                    sw.WriteLine(@"   string UrlNew = Url.Action(""New"", ""{2}"", new {0} area = ""{3}"" /*, {4} */ {1}); //Si es maestro detalle descomentar los campos ", "{", "}", GCUtil.GetNumber(Table.Name), Table.Comment, GCUtil.GetPkStringColsAndNameModel3(Table));
                    sw.WriteLine(@"   string UrlDelete = Url.Action(""Delete"", ""{2}"", new {0} area = ""{3}"", {4} {1}); ", "{", "}", GCUtil.GetNumber(Table.Name), Table.Comment, GCUtil.GetPkStringColsAndNameModel3(Table));
                    sw.WriteLine(@"   string UrlDuplicate = Url.Action(""Duplicate"", ""{2}"", new {0} area = ""{3}"", {4} {1}); ", "{", "}", GCUtil.GetNumber(Table.Name), Table.Comment, GCUtil.GetPkStringColsAndNameModel3(Table));
                    sw.WriteLine(@"   string UrlResult = Url.Action(""ListPartial"", ""{2}"", new {0} area = ""{3}"" {1}); ", "{", "}", GCUtil.GetNumber(Table.Name), Table.Comment);
                    sw.WriteLine(@"   //string UrlResultPadre = Url.Action(""Edit"", ""Maestro_Padre_de_{2}"", new {0} area = ""{3}"", {4} {1}); // Se descomenta si es maestro detalle ", "{", "}", GCUtil.GetNumber(Table.Name), Table.Comment, GCUtil.GetPkStringColsAndNameModel3(Table));
                    sw.WriteLine();
                    sw.WriteLine(@"   var ToolbarConfig = new ToolbarConfiguration(Prefix) ");
                    sw.WriteLine(@"        .Title(HCore.Recursos.GetRecurso(""{0}"")) ", Table.Name);
                    sw.WriteLine(@"        .Save(HCore.ActionViewSecurity(Context, UrlSave)) ");
                    sw.WriteLine(@"        .New(UrlNew, HCore.ActionViewSecurity(Context, UrlNew)) ");
                    sw.WriteLine(@"        .Delete(UrlDelete,HCore.ActionViewSecurity(Context, UrlDelete),UrlResult /*Cambiar UrlResult por UrlResultPadre si es un detalle*/, Model.EsNuevo) ");
                    sw.WriteLine(@"        .Duplicate(UrlDuplicate,HCore.ActionViewSecurity(Context, UrlDuplicate), Model.EsNuevo) ");
                    sw.WriteLine(@"        .Back(UrlResult); ");
                    sw.WriteLine(@"        //.LinkNavigation(); // Se descomenta si es maestro detalle y se comenta el .Back ");
                    sw.WriteLine(@"}");
                    sw.WriteLine();
                    sw.WriteLine(@"   @(Html.Siesa().Toolbar(ToolbarConfig)) ");
                    sw.WriteLine();
                    sw.WriteLine(@"    <ol class=""breadcrumbs"" id=""@(ViewBag.Prefix + ""DetailBread"")""></ol>");
                    sw.WriteLine();
                    sw.WriteLine(@" <div id = ""{0}MainPanelForm""> ", GCUtil.GetNumber(Table.Name));
                    sw.WriteLine(@"   <div class=""box box-info""> ");
                    sw.WriteLine(@"      <partial name=""_{0}Edit.cshtml"" model=Model view-data=viewData /> ", GCUtil.GetNumber(Table.Name));
                    sw.WriteLine(@"   </div> ");
                    sw.WriteLine(@" </div> ");
                    sw.WriteLine();
                    sw.WriteLine(@" <script lang = ""javascript""> ");
                    sw.WriteLine();
                    sw.WriteLine(@"    function @(ViewBag.Prefix)FormSuccess(data) ");
                    sw.WriteLine(@"    { ");
                    sw.WriteLine(@"       window.SE.SDI.FormSuccess(data, '@ViewData.ModelState.IsValid', arguments); ");
                    sw.WriteLine(@"    } ");
                    sw.WriteLine();
                    sw.WriteLine(@" </script>");
                }

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }
            

        }

        private string GetPkStringCols(List<ColumnModel> pks)
        {
            string cols = "";
            for (int i = 0; i < pks.Count; i++)
            {
                if (i == 0)
                    cols += "t." + pks[i].Code;
                else
                    cols += " ,t." + pks[i].Code;
            }
            return cols;
        }

    }
}
