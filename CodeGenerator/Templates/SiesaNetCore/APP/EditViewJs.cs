﻿using CodeGenerator.Data;
using CodeGenerator.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace CodeGenerator.Templates.SiesaNetCore
{
    public class EditViewJs
    {

        public string Name { get; set; } = "APP - View _Js (JavaScript)";
        public int Order { get; set; } = 9;
        public string Description { get; set; } = "Generates a Razor page for .NET";

        #region private property

        private string OutputFolder { get; set; }
        private string FullNameFile { get; set; }
        private string Project { get; set; }
        private TableModel Table { get; set; }
        private TemplateModel Template { get; set; }
        public EditViewJs() { } // Para usarse con reflection

        #endregion

        public EditViewJs(CodeGeneratorModel CodeGeneratorModel)
        {
            OutputFolder = CodeGeneratorModel.PathGenerate + "/APP/" + CodeGeneratorModel.TableGenerated.Comment + "/Views/" + GCUtil.GetNumber(CodeGeneratorModel.TableGenerated.Name) + "/";
            FullNameFile = "_" + GCUtil.GetNumber(CodeGeneratorModel.TableGenerated.Name) + "EditJs.cshtml";

            #region asignacion
            Table = CodeGeneratorModel.TableGenerated;
            Template = CodeGeneratorModel.TemplateGenerated;
            Project = CodeGeneratorModel.Domain;
            GenerateCodeTemplate();
            #endregion
        }

        public void GenerateCodeTemplate()
        {
            #region Creation directory
            string fileName = this.OutputFolder;
            if (!Directory.Exists(fileName))
                Directory.CreateDirectory(this.OutputFolder);
            fileName = this.OutputFolder + this.FullNameFile;
            if (File.Exists(fileName))
                File.Delete(fileName);
            #endregion

            StreamWriter sw = File.CreateText(fileName);
            try
            {
                if (Table.Columns.Exists(x => x.IsPrimaryKey))
                {
                    sw.WriteLine(@"@using {0}.UI.Modules.{1}.Models", Project, Table.Comment);
                    sw.WriteLine(@"@model {0}Model ", Table.Code);
                    sw.WriteLine();
                    sw.WriteLine(@"<script>");
                    sw.WriteLine();
                    sw.WriteLine(@"</script>");
                }

                sw.Flush();
                sw.Close();
            }
            catch (Exception e)
            {
                GCUtil.Errors.Add("La plantilla " + Template.Name + " no se genero por el siguiente error: " + e.Message);
                GCUtil.TemplateWithError.Add(Template.NameClass);
                sw.Flush();
                sw.Close();
                File.Delete(fileName);
            }


        }

    }
}
