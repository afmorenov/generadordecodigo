﻿using CodeGenerator.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CodeGenerator.Data
{

    public static class GCUtil
    {
        public static List<DataBaseMapModel> DataBaseInfo { get; set; } = new List<DataBaseMapModel>();
        public static List<string> Errors { get; set; } = new List<string>();
        public static List<string> TemplateWithError { get; set; } = new List<string>();
        public static void Reset()
        {
            Errors = new List<string>();
            TemplateWithError = new List<string>();
        }

        //public static CodeGeneratorModel SetTableInfo(CodeGeneratorModel CodeGeneratorModel)
        //{
        //    CodeGeneratorModel.TableGenerated.Columns = GCUtil.DataBaseInfo.FirstOrDefault(x => x.NumberConnection == CodeGeneratorModel.ConexionActual).Columns;
        //    CodeGeneratorModel.TableGenerated.InReferences = GCUtil.DataBaseInfo.FirstOrDefault(x => x.NumberConnection == CodeGeneratorModel.ConexionActual).InReferences;
        //    CodeGeneratorModel.TableGenerated.OutReferences = GCUtil.DataBaseInfo.FirstOrDefault(x => x.NumberConnection == CodeGeneratorModel.ConexionActual).OutReferences;
        //    CodeGeneratorModel.TableGenerated.Indexes = GCUtil.DataBaseInfo.FirstOrDefault(x => x.NumberConnection == CodeGeneratorModel.ConexionActual).Indexes;

        //    return CodeGeneratorModel;
        //}

        //public static string Project { get; set; }
        //public static string Template { get; set; }
        public static string PathTemp { get; set; } = Path.GetTempPath();
        //public static string PathGenerate { get; set; } = PathTemp;

        public static string CleanName(string name, bool NoPrefix = false)
        {
            Regex cleanRegEx = new Regex(@"\s+|_|-|\.", RegexOptions.Compiled);

            if (name.Contains("_"))
            {
                String[] splitString = name.Split(Char.Parse("_"));
                name = "";

                int index = (NoPrefix ? 1 : 0);

                for (int i = index; i < splitString.Length; i++)
                {
                    name += char.ToUpper(splitString[i][0]) + splitString[i].ToLower().Substring(1);
                }
            }
            return cleanRegEx.Replace(name, "");
        }

        public static string GetTablePrefix(string name)
        {
            Regex cleanRegEx = new Regex(@"\s+|_|-|\.", RegexOptions.Compiled);

            if (name.Contains("_"))
            {
                String[] splitString = name.Split(Char.Parse("_"));
                name = "";

                name += char.ToUpper(splitString[0][0]) + splitString[0].ToLower().Substring(1);
                
            }
            return cleanRegEx.Replace(name, "");
        }

        public static string GetTableOnlyName(string name)
        {
            return PascalCase(name, true);
        }

        public static string GetColumnCodeParentTable(string name)
        {
            Regex cleanRegEx = new Regex(@"\s+|_|-|\.", RegexOptions.Compiled);

            if (name.Contains("_"))
            {
                String[] splitString = name.Split(Char.Parse("_"));
                name = "";

                name += char.ToUpper(splitString[1][0]) + splitString[1].ToLower().Substring(1);

            }
            return cleanRegEx.Replace(name, "");
        }

        public static string CamelCase(string name, bool NoPrefix = false)
        {
            string output = CleanName(name, NoPrefix);
            return char.ToLower(output[0]) + output.Substring(1);
        }

        public static string PascalCase(string name, bool NoPrefix = false)
        {
            string output = CleanName(name, NoPrefix);
            int i = 0;
            bool firstIsNumber = int.TryParse(output[0].ToString(), out i);
            if (firstIsNumber)
                return "F" + output[0] + output.Substring(1);
            else
                return char.ToUpper(output[0]) + output.Substring(1);
        }

        public static string MakePlural(string name)
        {
            Regex plural1 = new Regex("(?<keep>[^aeiou])y$");
            Regex plural2 = new Regex("(?<keep>[aeiou]y)$");
            Regex plural3 = new Regex("(?<keep>[sxzh])$");
            Regex plural4 = new Regex("(?<keep>[^sxzhy])$");

            if (plural1.IsMatch(name))
                return plural1.Replace(name, "${keep}ies");
            else if (plural2.IsMatch(name))
                return plural2.Replace(name, "${keep}s");
            else if (plural3.IsMatch(name))
                return plural3.Replace(name, "${keep}es");
            else if (plural4.IsMatch(name))
                return plural4.Replace(name, "${keep}s");

            return name;
        }

        public static string MakeSingle(string name)
        {
            Regex plural1 = new Regex("(?<keep>[^aeiou])ies$");
            Regex plural2 = new Regex("(?<keep>[aeiou]y)s$");
            Regex plural3 = new Regex("(?<keep>[sxzh])es$");
            Regex plural4 = new Regex("(?<keep>[^sxzhyu])es$");
            Regex plural5 = new Regex("(?<keep>[^sxzhyu])s$");

            if (plural1.IsMatch(name))
                return plural1.Replace(name, "${keep}y");
            else if (plural3.IsMatch(name))
                return plural3.Replace(name, "${keep}");
            else if (plural2.IsMatch(name))
                return plural2.Replace(name, "${keep}");
            else if (plural4.IsMatch(name))
                return plural4.Replace(name, "${keep}");
            else if (plural5.IsMatch(name))
                return plural5.Replace(name, "${keep}");
            else if (name.ToUpper().EndsWith("S"))
                return name.Substring(0, name.Length - 1);
            return name;
        }

        public static string GetNumber(string name)
        {
            String[] splitString = name.Split(Char.Parse("_"));
            bool havePrefixNumber = splitString[0].Any(c => char.IsDigit(c));
            if (havePrefixNumber)
                return char.ToUpper(splitString[0][0]) + splitString[0].Substring(1);
            else
                return null;
        }

        public static string ColumnCodeNoNumber(string ColumnName)
        {
            var codeNoNumber = ColumnName.Substring(ColumnName.IndexOf("_") + 1).Replace("_"," ");
            return new CultureInfo("en-US").TextInfo.ToTitleCase(codeNoNumber);
        }

        public static List<DBSettings> ListDBSettings { get; set; }
        public static void LoadDBData()
        {
            GCUtil.ListDBSettings = new List<DBSettings>();
            string pathDBs = Path.Combine("DBConfig.json");
            if (File.Exists(pathDBs))
            {
                GCUtil.ListDBSettings = JsonConvert.DeserializeObject<List<DBSettings>>(File.ReadAllText(pathDBs));
            }
            else
            {
                GCUtil.ListDBSettings = new List<DBSettings>();
            }
        }

        public static string GetOrderByJavaDAO(List<ColumnModel> Columns)
        {
            string orderby = "";

            if (!Columns.Exists(x=>x.IsPrimaryKey)) {
                return "a." + GCUtil.CamelCase(Columns[0].CodeJava);
            }

            List<ColumnModel> ColumnsPrimaryKeys = Columns.Where(x => x.IsPrimaryKey).ToList();
            for (int i = 0; i < ColumnsPrimaryKeys.Count; i++)
            {
                if (i == 0)
                    orderby += "a." + GCUtil.CamelCase(Columns[i].CodeJava);
                else
                    orderby += ",a." + GCUtil.CamelCase(Columns[i].CodeJava);
            }
            return orderby + " asc";
        }

        public static string GetJavaDataType(ColumnModel ColumnModel)
        {
            if (ColumnModel.Type == "varchar" || ColumnModel.Type == "char" || ColumnModel.Type == "nvarchar" || ColumnModel.Type == "xml")
                return "String";
            if (ColumnModel.Type == "datetime" || ColumnModel.Type == "timestamp")
                return "Calendar";
            if (ColumnModel.Type == "tinyint" || ColumnModel.Type == "smallint" || ColumnModel.Type == "bit")
                return "Short";
            if (ColumnModel.Type == "int" || ColumnModel.Type == "bigint")
                return "Integer";
            if (ColumnModel.Type == "money" || ColumnModel.Type == "decimal" || ColumnModel.Type == "numeric" || ColumnModel.Type == "float" || ColumnModel.Type == "smallmoney")
                return "BigDecimal";
            if (ColumnModel.Type == "image" || ColumnModel.Type == "varbinary")
                return "byte[]";
            return null;
        }

        public static string GetNetDataType(ColumnModel ColumnModel)
        {
            if (ColumnModel.Type == "varchar" || ColumnModel.Type == "char" || ColumnModel.Type == "nvarchar" || ColumnModel.Type == "xml" || ColumnModel.Type == "text")
                return "String";
            if (ColumnModel.Type == "datetime" || ColumnModel.Type == "timestamp")
                return "DateTime";
            if (ColumnModel.Type == "bit")
                return "Boolean";
            if (ColumnModel.Type == "money" || ColumnModel.Type == "decimal" || ColumnModel.Type == "numeric" || ColumnModel.Type == "float" || ColumnModel.Type == "smallmoney")
                return "Decimal";
            if (ColumnModel.Type == "bigint")
                return "Int64";
            if (ColumnModel.Type == "int")
                return "Int32";
            if (ColumnModel.Type == "smallint")
                return "Int16";
            if (ColumnModel.Type == "tinyint")
                return "Byte";
            if (ColumnModel.Type == "image" || ColumnModel.Type == "varbinary")
                return "Byte[]";
            if (ColumnModel.Type == "uniqueidentifier")
                return "Guid";
            return null;
        }

        public static string GetNetDataTypeSimply(ColumnModel ColumnModel)
        {
            if (ColumnModel.Type == "varchar" || ColumnModel.Type == "char" || ColumnModel.Type == "nvarchar" || ColumnModel.Type == "xml")
                return "string";
            if (ColumnModel.Type == "datetime" || ColumnModel.Type == "timestamp")
                return "DateTime";
            if (ColumnModel.Type == "bit")
                return "bool";
            if (ColumnModel.Type == "money" || ColumnModel.Type == "decimal" || ColumnModel.Type == "numeric" || ColumnModel.Type == "float" || ColumnModel.Type == "smallmoney")
                return "decimal";
            if (ColumnModel.Type == "bigint" || ColumnModel.Type == "int")
                return "int";
            if (ColumnModel.Type == "smallint")
                return "short";
            if (ColumnModel.Type == "tinyint")
                return "byte";
            if (ColumnModel.Type == "image" || ColumnModel.Type == "varbinary")
                return "byte[]";
            return null;
        }

        public static string GetBlazorDataType(ColumnModel ColumnModel)
        {
            if (ColumnModel.Type == "varchar" || ColumnModel.Type == "char" || ColumnModel.Type == "nvarchar" || ColumnModel.Type == "xml")
                return (@"<HTextBox @bind-Text=""@Model.{0}"" />", ColumnModel.Code).ToString();

            if (ColumnModel.Type == "datetime" || ColumnModel.Type == "timestamp")
                return (@"<HDateEdit @bind-Date=""@Model.{0}"" />", ColumnModel.Code).ToString();

            if (ColumnModel.Type == "bigint" || ColumnModel.Type == "int" || ColumnModel.Type == "smallint")
                return (@"<HSpinEdit @bind-Value=""@Model.{0}"" />", ColumnModel.Code).ToString();

            if (ColumnModel.Type == "bit")
                return (@"<HCheckBox @bind-Checked=""@Model.{0}"" />", ColumnModel.Code).ToString();

            return null;

        }

            public static string GetPkStringColsWhitDataType(TableModel table)
        {
            var pks = table.Columns.Where(x => x.IsPrimaryKey).ToList();
            string cols = "";
            for (int i = 0; i < pks.Count; i++)
            {
                if (i == 0)
                    cols += GetNetDataType(pks[i])  + " " + GCUtil.CamelCase(pks[i].Code);
                else
                    cols += "," + GetNetDataType(pks[i]) + " " + GCUtil.CamelCase(pks[i].Code);
            }
            return cols;
        }

        public static string GetPkStringColsAndNameModel3(TableModel table)
        {
            var pks = table.Columns.Where(x => x.IsPrimaryKey).ToList();
            string cols = "";
            for (int i = 0; i < pks.Count; i++)
            {
                if (i == 0)
                    cols += GCUtil.CamelCase((pks[i].Code)) + " = " + "Model." + (pks[i].Code) + "";
                else
                    cols += ", " + GCUtil.CamelCase((pks[i].Code)) + " = " + "Model." + (pks[i].Code) + "";
            }
            return cols;
        }

        public static string GetPkStringColsModel(TableModel table)
        {
            var pks = table.Columns.Where(x => x.IsPrimaryKey).ToList();
            string cols = "";
            for (int i = 0; i < pks.Count; i++)
            {
                if (i == 0)
                    cols += "\"" + (pks[i].Code) + "\"";
                else
                    cols += ", \"" + (pks[i].Code) + "\"";
            }
            return cols;
        }

        public static string GetPkStringCols(TableModel table)
        {
            var pks = table.Columns.Where(x => x.IsPrimaryKey).ToList();
            string cols = "";
            for (int i = 0; i < pks.Count; i++)
            {
                if (i == 0)
                    cols += GCUtil.CamelCase(pks[i].Code);
                else
                    cols += "," + GCUtil.CamelCase(pks[i].Code);
            }
            return cols;
        }

        public static string GetPkStringColsQuery(TableModel table)
        {
            var pks = table.Columns.Where(x => x.IsPrimaryKey).ToList();
            string cols = "";
            for (int i = 0; i < pks.Count; i++)
            {
                if (i == 0)
                    cols += " x => x." + pks[i].Code + " == " + GCUtil.CamelCase(pks[i].Code);
                else
                    cols += " && x." + pks[i].Code + " == " + GCUtil.CamelCase(pks[i].Code);
            }
            return cols;
        }

        public static List<string> ModuleSeguridadSiesa()
        {
            List<string> securityModule = new List<string>();
            if (securityModule.Count == 0)
            {
                securityModule.Add("T001_PP_PARAMETROS");
                securityModule.Add("T002_PP_VERSIONES");
                securityModule.Add("T003_PP_MODULOS");
                securityModule.Add("T004_PP_PREF_USUARIO");
                securityModule.Add("T010_MM_COMPANIAS");
                securityModule.Add("T051_MM_PREF_USUARIO_CIA");
                securityModule.Add("T052_MM_PREF_USUARIO_DOCTOS");
                securityModule.Add("T058_MM_USUARIO_ENTIDAD");
                securityModule.Add("T181_MC_PARAMETROS_APROBACION");
                securityModule.Add("T211_MM_FUNCIONARIOS");
                securityModule.Add("T542_SS_SESION_W");
                securityModule.Add("T545_MN_GRUPOS_MENU");
                securityModule.Add("T546_MN_MENU_METODOS");
                securityModule.Add("T547_MN_MENU_OTROS");

                securityModule.Add("T5484_MN_FW_MENU_MAESTRO");
                securityModule.Add("T54851_MN_FW_MET_EXCLUIDOS");
                securityModule.Add("T5485_MN_FW_METODOS");
                securityModule.Add("T5486_MN_FW_TIPO_SEGURIDAD");
                securityModule.Add("T5501_SS_FW_PERFILES_NAV");

                securityModule.Add("T548_MN_MENUS");
                securityModule.Add("T549_MN_MENUS_GRUPOS_MENU");
                securityModule.Add("T5491_MN_MENUS_GRUPOS_MENU_W");
                securityModule.Add("T550_SS_PERFILES");
                securityModule.Add("T551_SS_METODOS_GENERALES");
                securityModule.Add("T552_SS_USUARIOS");
                securityModule.Add("T5521_SS_USUARIOS_CLAVES");
                securityModule.Add("T5522_SS_USUARIOS_PROGRAMA");
                securityModule.Add("T553_SS_PARAMETROS_GENERALES");
                securityModule.Add("T554_SS_POLITICAS");
                securityModule.Add("T561_SS_METODOS");
                securityModule.Add("T5611_SS_METODOS_CU");
                securityModule.Add("T562_SS_PARAMETROS");
                securityModule.Add("T563_SS_METODOS_MODULOS");
                securityModule.Add("T564_SS_METODOS_SP");
                securityModule.Add("T570_SS_METODOS_PERFIL");
                securityModule.Add("T571_SS_USUARIOS_PERFIL");
                securityModule.Add("T572_SS_CONSULTAS_PERFIL");
                securityModule.Add("T5798_SM_UPDATE_SERVICE_PACK");
                securityModule.Add("T6001_IN_INFORMES_PERFILES");
                securityModule.Add("T691_WS_USUARIO_CONSULTA");
                securityModule.Add("T9734_PDV_CAJEROS");
                securityModule.Add("T9760_PDV_BD_POS");
                securityModule.Add("T9766_PDV_ENROLAMIENTO");
                securityModule.Add("W0003_LENGUAJES");
                securityModule.Add("W0004_BARRAS_HERRAMS_CLASES");
                securityModule.Add("W0015_RECURSOS");
                securityModule.Add("W0009_VERSIONES");
                securityModule.Add("W0040_ARCHIVOS");
                securityModule.Add("T997FWJOBS");
                securityModule.Add("T5501SSFWPERFILESNAV");
                securityModule.Add("T5484MNFWMENUMAESTRO");
                securityModule.Add("T5485MNFWMETODOS");
                securityModule.Add("T54851MNFWMETEXCLUIDOS");
                securityModule.Add("T5486MNFWTIPOSEGURIDAD");

            }
            return securityModule;
        }

        public static List<string> ModuleSeguridadLicences()
        {
            List<string> securityModule = new List<string>();
            if (securityModule.Count == 0)
            {
                securityModule.Add("T0000_AUDITORIAS");
                securityModule.Add("T0001_LENGUAJES");
                securityModule.Add("T0002_RECURSOS");
                securityModule.Add("T0003_PAISES");
                securityModule.Add("T0004_DEPARTAMENTOS");
                securityModule.Add("T0005_CIUDADES");
                securityModule.Add("T0006_BARRIOS");
                securityModule.Add("T0007_USUARIOS");
                securityModule.Add("T0008_SESIONES");
                securityModule.Add("T0009_PERFILES");
                securityModule.Add("T0010_CARGOS");
                securityModule.Add("T0011_PERFILES_USUARIOS");
                securityModule.Add("T0012_PERFILES_NAVEGACION");
                securityModule.Add("W0003_LENGUAJES");
                securityModule.Add("W0015_RECURSOS");
            }
            return securityModule;
        }

        public enum RuleType
        {
            CommonRules = 1,
            AddRules = 2,
            MofidyRules = 3,
            RemoveRules = 4,
            //FindByIdRules = 5,

            //CalculationRules = 6,
            //CustomRules = 7,

            //ForeignAdd = 8,
            //ForeignModify = 9,
            //ForeignRemove = 10

        }

    }
}
